package com.connor.bank.interceptor;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;

import com.connor.bank.annotation.Retry;
import com.connor.bank.model.BankConstants;
import com.connor.bank.util.ExceptionUtil;
import com.connor.bank.util.PropertiesUtil;

/**
 * Interceptor for retrying transactions in which rows have been locked.
 * @author Connors
 *
 */
@Retry
@Interceptor
public class RetryTimeoutInterceptor {
	
	/**
	 * Default if nothing is found in properties file.
	 */
	private static final int DEFAULT_NUMBER_RETRIES = 3;
	
	/**
	 * Property indicating how many times to retry.
	 */
	private static final String NUMBER_OF_RETRIES = "lockedAccountRetries";

	/**
	 * Logger
	 */
	private final Logger log;

	/**
	 * Used to read from config file
	 */
	private final PropertiesUtil propertiesUtil;

	/**
	 * 
	 */
	private final ExceptionUtil exceptionUtil;


	/**
	 * Cdi-enabled constructor
	 * @param log
	 * @param propertiesUtil
	 * @param exceptionUtil
	 */
	@Inject
	public RetryTimeoutInterceptor(Logger log, PropertiesUtil propertiesUtil, ExceptionUtil exceptionUtil) {
		this.log = log;
		this.propertiesUtil = propertiesUtil;
		this.exceptionUtil = exceptionUtil;
	}



	/**
	 * This should intercept the call and if we get locked out, retry the number of times specified in properties file.
	 * If it fails all those times, then rethrow (or if it fails for another reason), automatically rethrow.
	 * @param context
	 * @return
	 * @throws Exception
	 */
	@AroundInvoke
	public Object retryTransactionOnTimeout(InvocationContext context) throws Exception {
		//default to amount specified in case file does not contain it
		int numberOfRetries = DEFAULT_NUMBER_RETRIES;		
		
		//read in max retries from properties file
		numberOfRetries = propertiesUtil.getProperty(BankConstants.BANK_PROPS_FILE, NUMBER_OF_RETRIES, numberOfRetries);
		
		//if the transaction fails because someone else has a lock on the row, retry
		int i = 0;
		Exception thrownException = null;
		while(i < numberOfRetries) {
			try {
				//allow the method to return if it goes through successfully
				return context.proceed();
			}catch(Exception e) {
				//if it is not a timeout, rethrow, else let proceed again
				if(!exceptionUtil.isTimeOutException(e)) {
					throw e;
				}
				
				thrownException = e;
			}


			//this means we timed out, so increment the counter
			++i;
		}

		//this means we timed out the max allowable number of times, so rethrow
		throw thrownException;
	}	
}