package com.connor.bank.exception;

public class BankAccountNotExistException extends BankServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7606413784327874822L;
	
	/**
	 * Use with String.format
	 */
	private static final String ERROR_FORMAT_STR = "The account with id %d doesn't exist";
	
	/**
	 * The account id searched on 
	 */
	private final long accountId;
	
	public BankAccountNotExistException(String message, long accountId) {
		super(message);
		this.accountId = accountId;
	}
	
	//TODO override getMessage()

}
