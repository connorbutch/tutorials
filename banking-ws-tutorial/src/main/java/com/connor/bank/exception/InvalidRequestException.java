package com.connor.bank.exception;

import java.util.Collections;
import java.util.List;

import com.connor.bank.model.InvalidRequest;

public class InvalidRequestException extends BankServiceException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -3061587212919350366L;

	/**
	 * 
	 */
	private final List<InvalidRequest> invalids;
	
	/**
	 * 
	 * @param message
	 * @param invalids
	 */
	public InvalidRequestException(String message,List<InvalidRequest> invalids) {
		super(message);
		this.invalids = Collections.unmodifiableList(invalids);
	}

	/**
	 * Getter
	 * @return
	 */
	public List<InvalidRequest> getInvalids() {
		return invalids;
	}
}