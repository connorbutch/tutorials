package com.connor.bank.exception;

public class InsufficientFundsException extends BankServiceException {

	/**
	 * 
	 */
	private final long accountNumber;
	
	/**
	 * 
	 */
	private final double amountAttemptedToWithdraw;
	
	
	public InsufficientFundsException(String message, long accountNumber, double amountAttemptedToWithdraw) {
		super(message);
		this.accountNumber = accountNumber;
		this.amountAttemptedToWithdraw = amountAttemptedToWithdraw;
	}

}
