package com.connor.bank.exception;

/**
 * 
 * @author Connors
 *
 */
public class BankPropertiesException extends BankSystemException {	

	/**
	 * 
	 */
	private static final long serialVersionUID = 7767170834166776895L;
	
	/**
	 * Use with string.format for message
	 */
	private static final String ERROR_FORMAT_STR = "The property file path was %s and the property key was %s";

	/**
	 * 
	 */
	private final String propertyFilePath;
	
	/**
	 * 
	 */
	private final String propertyKey;
	
	/**
	 * 
	 * @param message
	 * @param t
	 * @param propertyFilePath
	 * @param propertyKey
	 */
	public BankPropertiesException(String message, Throwable t, String propertyFilePath, String propertyKey) {
		super(message, t);
		this.propertyFilePath = propertyFilePath;
		this.propertyKey = propertyKey;
	}
	
	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder();
		String message = super.getMessage();
		
		if(message != null) {
			sb.append(message);
		}
		
		sb.append(String.format(ERROR_FORMAT_STR, propertyFilePath, propertyKey));
		return sb.toString();		
	}	
}