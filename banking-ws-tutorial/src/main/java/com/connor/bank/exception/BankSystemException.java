package com.connor.bank.exception;

/**
 * This should get thrown in the case of unexpected system exceptions
 * @author Connors
 *
 */
public class BankSystemException extends Exception {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -8528804414605764372L;

	/**
	 * Constructor
	 * @param message
	 * @param t
	 */
	public BankSystemException(String message, Throwable t) {
		super(message, t);
	}
}