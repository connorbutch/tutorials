package com.connor.bank.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true, inherited=true)
public class BankServiceException extends Exception{

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 8060697424807603590L;

	/**
	 * 
	 * @param message
	 */
	public BankServiceException(String message) {
		super(message);
	}
	
	/**
	 * 
	 * @param message
	 * @param t
	 */
	public BankServiceException(String message, Throwable t) {
		super(message, t);
	}	
}
