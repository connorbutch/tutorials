package com.connor.bank.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.inject.Stereotype;

/**
 * Sterotype annotation used to denote a service.
 * @author Connors
 *
 */
@Stereotype
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
@TransactionManagement(TransactionManagementType.CONTAINER)
@Local
@Documented
@Retention(RUNTIME)
@Target({ TYPE, ANNOTATION_TYPE })
public @interface Service {

}
