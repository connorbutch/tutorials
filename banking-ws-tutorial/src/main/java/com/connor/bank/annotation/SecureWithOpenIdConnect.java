package com.connor.bank.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

/**
 * This allows us to bind a resource to a particular filter
 * @author Connors
 *
 */
@NameBinding
@Documented
@Retention(RUNTIME)
@Target({ TYPE, METHOD })
public @interface SecureWithOpenIdConnect {

}
