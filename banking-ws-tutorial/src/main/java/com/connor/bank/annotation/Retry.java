package com.connor.bank.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

/**
 * This is an interceptor binding used to tell that we should retry a method in case of timeout
 * @author Connors
 *
 */
@InterceptorBinding
@Documented
@Retention(RUNTIME)
@Target({ TYPE, METHOD })
public @interface Retry {
	//marker interface -- intentionally blank
}
