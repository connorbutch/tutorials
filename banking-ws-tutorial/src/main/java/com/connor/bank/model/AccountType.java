package com.connor.bank.model;

/**
 * Represents different types of accounts
 * @author Connors
 *
 */
public enum AccountType {

	/**
	 * checking account
	 */
	CHECKING,
	
	/**
	 * savings account
	 */
	SAVINGS;
	
}
