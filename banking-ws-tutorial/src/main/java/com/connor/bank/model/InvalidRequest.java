package com.connor.bank.model;

/**
 * This holds the error codes for an invalid request
 * @author Connors
 *
 */
public enum InvalidRequest {
	
	UNKNOWN(999, 999, "Unknown");

	
	private final int code;
	
	private final int subcode;
	
	private final String message;

	/**
	 * Private, hidden constructor.
	 * @param code
	 * @param subcode
	 * @param message
	 */
	private InvalidRequest(int code, int subcode, String message) {
		this.code = code;
		this.subcode = subcode;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public int getSubcode() {
		return subcode;
	}

	public String getMessage() {
		return message;
	}
	
	
}
