package com.connor.bank.model;

import java.io.Serializable;

public class BankAccount implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6469260300261590180L;
	
	/**
	 * 
	 */
	private long routingNumber;
	
	/**
	 * 
	 */
	private double balance;
	
	/**
	 * 
	 */
	private AccountType accountType;

	public long getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(long routingNumber) {
		this.routingNumber = routingNumber;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}	
}