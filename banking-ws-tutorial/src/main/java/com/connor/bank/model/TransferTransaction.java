package com.connor.bank.model;

import java.io.Serializable;
import java.time.Instant;

/**
 * Represents a trasfer of money between accounts.
 * @author Connors
 *
 */
public class TransferTransaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2712703360375821618L;

	private long transactionId;
	
	private long fromAccountId;
	
	private long toAccountId;
	
	private double transferAmount;
	
	private Instant timestamp;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getFromAccountId() {
		return fromAccountId;
	}

	public void setFromAccountId(long fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	public long getToAccountId() {
		return toAccountId;
	}

	public void setToAccountId(long toAccountId) {
		this.toAccountId = toAccountId;
	}

	public double getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(double transferAmount) {
		this.transferAmount = transferAmount;
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}
	
	
	
}
