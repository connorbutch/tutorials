package com.connor.bank.model;

public interface BankConstants {

	/**
	 * The jndi name of the datasource
	 */
	public static final String BANK_DB_JNDI_NAME = ""; //TODO
	
	/**
	 * Points to the properties file loaded by bank
	 */
	public static final String BANK_PROPS_FILE = "bank.properties"; 
	
}
