package com.connor.bank.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;

@Provider
@Priority(1) //make occur first, even before authentication filter (lowest number filter is processed first)
public class RequestHealthCheckFilter implements ContainerRequestFilter {

	/**
	 * 
	 */
	private static final String HEALTH_CHECK_HEADER = "health-check";

	/**
	 * 
	 */
	private final Logger log;

	/**
	 * 
	 */
	private final ResourceInfo resourceInfo;

	/**
	 * 
	 * @param log
	 * @param resourceInfo
	 */
	@Inject
	public RequestHealthCheckFilter(Logger log, ResourceInfo resourceInfo) {
		this.log = log;
		this.resourceInfo = resourceInfo;
	}



	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		/*
		 * NOTE: this should only return a 200 response when it is going to a valid endpoint and has health check header.  
		 * In all other cases, let the request continue
		 */

		//handle if we have health check header
		if(requestContext.getHeaderString(HEALTH_CHECK_HEADER) != null) { //can add various degrees
			//check we are going to valid endpoint
			if(resourceInfo.getResourceClass() != null) {
				//as we know this is going to a valid endpoint, abort the request and return the expected response
				Response healthCheckResponse = buildHealthCheckResponse();
				requestContext.abortWith(healthCheckResponse);
			}
		}	
	}

	/**
	 * For now, returns a 200 response with an empty body.
	 * @return
	 */
	protected Response buildHealthCheckResponse() {
		return Response.ok().build();
	}
}