package com.connor.bank.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;

import com.connor.bank.annotation.SecureWithOpenIdConnect;
import com.connor.bank.bean.OpenIdConnectBean;

/**
 * This class exists to validate the open id connect token is passed in the header, and that they have access
 * @author Connors
 *
 */
@Provider
@SecureWithOpenIdConnect
@Priority(Priorities.AUTHORIZATION)
public class RequestAuthorizationFilter implements ContainerRequestFilter {

	/**
	 * The header for the open id connect token
	 */
	private static final String OPEN_ID_HEADER = ""; //TODO
	
	/**
	 * 
	 */
	private final Logger log;	
	
	/**
	 * 
	 */
	private final OpenIdConnectBean openIdConnectBean;
	
	/**
	 * Cdi-enabled constructor
	 * @param log
	 * @param openIdConnectBean
	 */
	@Inject
	public RequestAuthorizationFilter(Logger log, OpenIdConnectBean openIdConnectBean) {
		this.log = log;
		this.openIdConnectBean = openIdConnectBean;
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		//check header is present
		String openIdConnectToken = requestContext.getHeaderString(OPEN_ID_HEADER);
		if(openIdConnectToken == null) {
			Response response = null; //TODO build invalid request response
			requestContext.abortWith(response);
		}
		
		
		//TODO validate header for resource -- if invalid, then return response
		
		
		//if we got here, we recieved a valid request
		
		
	}

}
