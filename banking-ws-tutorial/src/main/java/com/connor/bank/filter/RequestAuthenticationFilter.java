package com.connor.bank.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;

import com.connor.bank.annotation.SecureWithOpenIdConnect;
import com.connor.bank.bean.OpenIdConnectBean;

@Provider //register with jaxrs implementation as a provider
@SecureWithOpenIdConnect //only intercept methods registered with this annotation
@Priority(Priorities.AUTHENTICATION) //set the priority
public class RequestAuthenticationFilter implements ContainerRequestFilter {

	private final Logger log;
	
	private final OpenIdConnectBean openIdConnectBean;
	
	/**
	 * Cdi-enabled constructor
	 * @param log
	 * @param openIdConnectBean
	 */
	@Inject
	public RequestAuthenticationFilter(Logger log, OpenIdConnectBean openIdConnectBean) {
		this.log = log;
		this.openIdConnectBean = openIdConnectBean;
	}



	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
