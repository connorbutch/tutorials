package com.connor.bank.controller;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;

import com.connor.bank.exception.BankServiceException;
import com.connor.bank.model.TransferTransaction;
import com.connor.bank.service.AccountService;
import com.connor.bank.util.ErrorBuilder;
import com.connor.bank.util.Validator;

/**
 * 
 * @author Connors
 *
 */
public class BankControllerImpl implements BankController {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -6048665187854833290L;
	
	/**
	 * Logger instance
	 */
	private final Logger log;
	
	/**
	 * 
	 */
	private final AccountService accountService;
	
	/**
	 * 
	 */
	private final Validator validator;
	
	/**
	 * 
	 */
	private final ErrorBuilder errorBuilder;

	
	/**
	 * Cdi-enabled constructor
	 * @param log
	 * @param accountService
	 * @param validator
	 * @param errorBuilder
	 */
	@Inject
	public BankControllerImpl(Logger log, AccountService accountService, Validator validator,
			ErrorBuilder errorBuilder) {
		this.log = log;
		this.accountService = accountService;
		this.validator = validator;
		this.errorBuilder = errorBuilder;
	}



	@Override
	public Response createTransaction(TransferTransaction transaction) {
		//TODO switch service to take transaction itself
		
		Response response = null;
		TransferTransaction transactionResponse = null;
		try {
			transactionResponse = accountService.transfer(transaction.getFromAccountId(), transaction.getToAccountId(), transaction.getTransferAmount());
			response = Response.ok().entity(transactionResponse).build();
		} catch (BankServiceException e) {
			log.error("There was an error when creating a transaction", e);
			response = errorBuilder.toResponse(e);
		}
		
		return response;
	}
}