package com.connor.bank.controller;

import java.io.Serializable;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.BeanParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.connor.bank.model.TransferTransaction;

/**
 * 
 * @author Connors
 *
 */
@ApplicationPath("/banks/v1")
public interface BankController extends Serializable {

	/**
	 * Transfers money as specified by the transaction.
	 * @param transaction
	 * @return
	 */
	@POST
	@Path("/transactions")
	public Response createTransaction(@BeanParam TransferTransaction transaction);	
}
