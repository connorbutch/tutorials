package com.connor.bank.util;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;

/**
 * 
 * @author Connors
 *
 */
public class DefaultExceptionHandler implements ExceptionMapper<RuntimeException> {

	/**
	 * 
	 */
	private final Logger log;
	
	/**
	 * 
	 */
	private final ErrorBuilder errorBuilder;
	
	/**
	 * Cdi-enabled constructor
	 * @param log
	 * @param errorBuilder
	 */
	@Inject
	public DefaultExceptionHandler(Logger log, ErrorBuilder errorBuilder) {
		this.log = log;
		this.errorBuilder = errorBuilder;
	}



	@Override
	public Response toResponse(RuntimeException exception) {
		// TODO Auto-generated method stub
		return null;
	}

}
