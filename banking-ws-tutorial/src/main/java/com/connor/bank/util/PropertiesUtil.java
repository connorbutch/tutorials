package com.connor.bank.util;

import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.connor.bank.annotation.Cache;

/**
 * Used for reading from properties files
 * @author Connors
 *
 */
public class PropertiesUtil {
	
	/**
	 * Logger instance
	 */
	private final Logger log;

	/**
	 * 
	 */
	private final Map<String, Map<String, String>> cache;

	/**
	 * Cdi-enabeld constructor
	 * @param log
	 * @param cache
	 */
	@Inject
	public PropertiesUtil(Logger log, @Cache Map<String, Map<String, String>> cache) {
		this.log = log;
		this.cache = cache;
	}
	
	/**
	 * 
	 * @param propertyFilePath
	 * @param propertyKey
	 * @return
	 */
	public String getProperty(String propertyFilePath, String propertyKey) {
		return (String) read(propertyFilePath, propertyKey);
	}	
	
	/**
	 * 
	 * @param propertyFilePath
	 * @param propertyKey
	 * @param defaultValue
	 * @param returnDefaultIfKeyNotFound
	 * @param returnDefaultIfPropertyBlank
	 * @return
	 */
	public String getProperty(String propertyFilePath, String propertyKey, String defaultValue, boolean returnDefaultIfKeyNotFound, boolean returnDefaultIfPropertyBlank) {
		return null; //TODO
	}

	
	/**
	 * 
	 * @param propertyFilePath
	 * @param propertyKey
	 * @param type
	 * @return
	 */
	public <T> T getProperty(String propertyFilePath, String propertyKey, T type) {
		return (T) read(propertyFilePath, propertyKey); //TODO may want to import google guava to do type checks here
		//and avoid generic type erasure at runtime . .  .
	}
	
	//TODO add method for read in with default and generics (similar to overloaded string method)
	
	/**
	 * 
	 * @param propertyFilePath
	 * @param propertyKey
	 * @return
	 */
	private Object read(String propertyFilePath, String propertyKey) {
		//TODO
		return null;
	}
}