package com.connor.bank.bean;

import javax.enterprise.context.RequestScoped;

/**
 * This is a request scoped bean used to hold information about the open id connect info passed.
 * This allows information to be easily shared between authentication and authorization filters. 
 * @author Connors
 *
 */
@RequestScoped
public class OpenIdConnectBean {

}
