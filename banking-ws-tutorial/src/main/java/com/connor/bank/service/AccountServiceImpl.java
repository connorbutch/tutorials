package com.connor.bank.service;

import java.time.Instant;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.connor.bank.annotation.Service;
import com.connor.bank.dao.AccountDao;
import com.connor.bank.exception.BankAccountNotExistException;
import com.connor.bank.exception.BankServiceException;
import com.connor.bank.exception.BankSystemException;
import com.connor.bank.exception.InsufficientFundsException;
import com.connor.bank.model.BankAccount;
import com.connor.bank.model.TransferTransaction;
import com.connor.bank.util.Validator;

/**
 * 
 * @author Connors
 *
 */
@Service
public class AccountServiceImpl implements AccountService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1618681680951788589L;

	/**
	 * Logger
	 */
	private final Logger log;

	/**
	 * Dao
	 */
	private final AccountDao accountDao;

	/**
	 * 
	 */
	private final Validator validator;


	/**
	 * Cdi-enabled constructor.
	 * @param log
	 * @param accountDao
	 * @param validator
	 */
	@Inject
	public AccountServiceImpl(Logger log, AccountDao accountDao, Validator validator) {
		this.log = log;
		this.accountDao = accountDao;
		this.validator = validator;
	}

	@Override
	public TransferTransaction transfer(long fromAccountId, long toAccountId, double amountToTransfer) throws BankServiceException {
		//validate the request, if invalid, let exception be rethrown
		validator.validateTransfer(fromAccountId, toAccountId, amountToTransfer);

		//retrieve the to account by id
		BankAccount toAccount = null;
		try {
			toAccount = accountDao.getAccountById(toAccountId); //NOTE: we choose not to lock here because we are transfering to this account.  We assume our business processes do not allow deleting/closing of accounts during this time . .  
		} catch (BankSystemException e) {
			//wrap and rethrow with context, causing transaction rollback
			String errorMessage = String.format("There was an issue retrieving account with id %d", toAccountId);
			throw new BankServiceException(errorMessage, e);
		}

		//if the to account doesn't exist, then throw exception
		if(toAccount == null) {
			throw new BankAccountNotExistException("Couldn't find account to transfer funds to", toAccountId);
		}

		//retrieve the to account by id and get a lock on row so no one else can update it
		BankAccount fromAccount = null;
		try {
			fromAccount = accountDao.getAccountByIdAndMantainLock(fromAccountId);
		} catch (BankSystemException e) {
			//wrap and rethrow with context, causing transaction rollback
			String errorMessage = String.format("There was an issue getting account with id %d and maintaining the atomic lock", fromAccountId);
			throw new BankServiceException(errorMessage, e);
		}

		//if the from account doesn't exist, throw not found exception
		if(fromAccount == null) {
			throw new BankAccountNotExistException("Couldn't find account to withdraw funds from", fromAccountId);
		}

		//check from account has valid balance, if not, throw exception
		if(fromAccount.getBalance() < amountToTransfer) {
			throw new InsufficientFundsException("The account has insufficient funds to withdraw", fromAccountId, amountToTransfer);
		}		

		//attempt to withdraw from one account
		try {
			accountDao.withdrawFromAccount(fromAccountId, amountToTransfer);
		} catch (BankSystemException e) {
			//wrap and throw exceptions, which will cause rollback
			String errorMessage = String.format("There was an error when attempting to withdraw from account with id %d", fromAccountId);
			throw new BankServiceException(errorMessage, e);
		}

		//deposit from other
		try {
			accountDao.depositToAccount(toAccountId, amountToTransfer);
		} catch (BankSystemException e) {
			//wrap and throw exceptions, which will cause rollback
			String errorMessage = String.format("There was an error when attempting to deposit to account with id %d", toAccountId);
			throw new BankServiceException(errorMessage, e);
		}

		//create the transaction in db
		TransferTransaction transaction = null;
		try {
			transaction = accountDao.createTransaction(fromAccountId, toAccountId, amountToTransfer, Instant.now());
		} catch (BankSystemException e) {
			//wrap and throw exceptions, which will cause rollback
			throw new BankServiceException("There was an issue creating a record of the transaction", e);
		}
		
		//log the transaction for extra assurance
		log.info("Successfully created transaction {}", transaction);
		
		//return it
		return transaction;
	}
}