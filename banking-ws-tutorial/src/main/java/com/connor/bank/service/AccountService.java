package com.connor.bank.service;

import java.io.Serializable;

import com.connor.bank.exception.BankServiceException;
import com.connor.bank.model.TransferTransaction;

/**
 * This class defines the contract for the account service
 * @author Connors
 *
 */
public interface AccountService extends Serializable {

	/**
	 * 
	 * @param fromAccountId
	 * @param toAccountId
	 * @param amountToTransfer
	 * @return
	 * @throws BankServiceException
	 */
	TransferTransaction transfer(long fromAccountId, long toAccountId, double amountToTransfer) throws BankServiceException; //TODO add more specific exception here
	
}
