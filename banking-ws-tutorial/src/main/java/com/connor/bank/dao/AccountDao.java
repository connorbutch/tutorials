package com.connor.bank.dao;

import java.io.Serializable;
import java.time.Instant;

import com.connor.bank.exception.BankSystemException;
import com.connor.bank.model.BankAccount;
import com.connor.bank.model.TransferTransaction;

/**
 * Interface for account interactions
 * @author Connors
 *
 */
public interface AccountDao extends Serializable{

	/**
	 * Gets a bank account by its id.  If none exists with the specified id, then it will return null.
	 * @param routingNumber
	 * @return
	 * @throws BankSystemException
	 */
	BankAccount getAccountById(long routingNumber) throws BankSystemException;

	/**
	 * Same as above, except this maintains a lock on the row returned until transaction
	 * is committed.
	 * @param routingNumber
	 * @return
	 * @throws BankSystemException
	 */
	BankAccount getAccountByIdAndMantainLock(long routingNumber) throws BankSystemException;

	/**
	 * 
	 * @param routingNumber
	 * @param amountToDeposit
	 * @throws BankSystemException
	 */
	void depositToAccount(long routingNumber, double amountToDeposit) throws BankSystemException;

	/**
	 * 
	 * @param routingNumber
	 * @param amountToWithdraw
	 * @throws BankSystemException
	 */
	void withdrawFromAccount(long routingNumber, double amountToWithdraw) throws BankSystemException;

	/**
	 * Creates a record of the transfer in our transaction table
	 * @param fromAccountId
	 * @param toAccountId
	 * @param amount
	 * @param timestamp
	 * @return
	 * @throws BankSystemException
	 */
	TransferTransaction createTransaction(long fromAccountId, long toAccountId, double amount, Instant timestamp) throws BankSystemException;
}
