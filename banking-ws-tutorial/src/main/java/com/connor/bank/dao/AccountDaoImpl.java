package com.connor.bank.dao;

import java.time.Instant;
import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.connor.bank.annotation.Retry;
import com.connor.bank.exception.BankSystemException;
import com.connor.bank.model.BankAccount;
import com.connor.bank.model.BankConstants;
import com.connor.bank.model.TransferTransaction;
import com.connor.bank.util.ExceptionUtil;
import com.connor.bank.util.PropertiesUtil;

/**
 * Concrete implementation of account dao.
 * @author Connors
 *
 */
public class AccountDaoImpl implements AccountDao {	

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -7518843308923904112L;

	//---------------------------------------------------------------------------
	//The following are the property names of the sql to execute

	/**
	 * 
	 */
	private static final String GET_ACCOUNT_NO_LOCK_SQL_PROP = "getAccountByIdSql";

	/**
	 * 
	 */
	private static final String GET_ACCOUNT_WITH_LOCK_SQL_PROP = "getAccountByIdAndLockSql";

	/**
	 * 
	 */
	private static final String DEPOSIT_SQL_PROP = "depositToAccountSql";

	/**
	 * 
	 */
	private static final String WITHDRAW_SQL_PROP = "withdrawFromAccountSql";

	/**
	 * 
	 */
	private static final String CREATE_TRANSFER_TRANSACTION_PROP = "createTransferTransactionSql";

	/**
	 * 
	 */
	private static final String LOCK_SQL = "FOR UPDATE";

	//---------------------------------------------------------------------------
	//begin fields

	/**
	 * 
	 */
	private final Logger log;	

	/**
	 * 
	 */
	private final NamedParameterJdbcTemplate namedTemplate;

	/**
	 * 
	 */
	private final JdbcTemplate jdbcTemplate;

	/**
	 * 
	 */
	private final ResultSetExtractor<BankAccount> bankAccountMapper;

	/**
	 * 
	 */
	private final PropertiesUtil propertiesUtil;

	/**
	 * 
	 */
	private final ExceptionUtil exceptionUtil; //TODO likely don't need this . . . 


	/**
	 * Cdi-enabled constructor.
	 * @param log
	 * @param namedTemplate
	 * @param jdbcTemplate
	 * @param bankAccountMapper
	 * @param propertiesUtil
	 * @param exceptionUtil
	 */
	@Inject
	public AccountDaoImpl(Logger log, NamedParameterJdbcTemplate namedTemplate, JdbcTemplate jdbcTemplate,
			ResultSetExtractor<BankAccount> bankAccountMapper, PropertiesUtil propertiesUtil, ExceptionUtil exceptionUtil) {
		this.log = log;
		this.namedTemplate = namedTemplate;
		this.jdbcTemplate = jdbcTemplate;
		this.bankAccountMapper = bankAccountMapper;
		this.propertiesUtil = propertiesUtil;
		this.exceptionUtil = exceptionUtil;
	}

	@Retry //make sure we retry in the rare chance we have someone else locking our row
	@Override
	public BankAccount getAccountById(long routingNumber) throws BankSystemException {
		//read sql from file -- let blank sql throw data access exception
		String sql = propertiesUtil.getProperty(BankConstants.BANK_PROPS_FILE, GET_ACCOUNT_NO_LOCK_SQL_PROP);

		//warn if sql is blank, but allow to continue
		if(StringUtils.isBlank(sql)) {
			log.warn("The sql retrieved for getting an account was blank.  This will likely lead to errors");
		}

		//build args
		Object[] args = { routingNumber };

		//execute proc
		try {
			return jdbcTemplate.query(sql, args, bankAccountMapper);
		} catch(DataAccessException e) {
			//handle exception by wrapping and throwing
			throw new BankSystemException("", e); //TODO
		}
	}

	@Retry //make sure we retry in the rare chance we have someone else locking our row
	@Override
	public BankAccount getAccountByIdAndMantainLock(long routingNumber) throws BankSystemException {
		//read sql from file
		String sql = propertiesUtil.getProperty(BankConstants.BANK_PROPS_FILE, GET_ACCOUNT_WITH_LOCK_SQL_PROP);

		//warn if sql is blank, but allow to continue
		if(StringUtils.isBlank(sql)) {
			log.warn("The sql for getting account by id and maintaing a lock is blank.  This will likely cause errors");
		}

		//warn if is not a locking query, but allow to proceed
		if(!isLockQuery(sql)) {
			log.warn("We are attempting to execute a locking query, but it is not locking.  Sql to execute is {}", sql);
		}

		//build args
		Object[] args = { routingNumber };

		//execute proc
		try {
			return jdbcTemplate.query(sql, args, bankAccountMapper);
		}catch(DataAccessException e) {
			//handle exception by wrapping and throwing
			throw new BankSystemException("", e); //TODO
		}
	}

	@Retry //make sure we retry in the rare chance we have someone else locking our row
	@Override
	public void depositToAccount(long routingNumber, double amountToDeposit) throws BankSystemException {
		//read sql from file
		String sql = propertiesUtil.getProperty(BankConstants.BANK_PROPS_FILE, DEPOSIT_SQL_PROP);

		//warn if sql is blank, but allow to continue
		if(StringUtils.isBlank(sql)) {
			log.warn("The sql for getting account by id and maintaing a lock is blank.  This will likely cause errors");
		}

		//build args
		Map<String, ?> args = Stream.of(
				new AbstractMap.SimpleEntry<>("routingNumber", routingNumber),
				new AbstractMap.SimpleEntry<>("amountToDeposit", amountToDeposit))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		//execute proc
		int numberOfRowsAffected;
		try {
			numberOfRowsAffected = namedTemplate.update(sql, args);
		}catch(DataAccessException e) {
			//handle exception by wrapping and throwing (interceptor will retry)
			throw new BankSystemException("", e); //TODO
		}

		//warn if we didn't update the expected number of rows -- NOTE: in future, may want to check to be greater than 0, this may affect other rows too if foreign keys/more complex sql
		if(numberOfRowsAffected < 1) {
			log.warn("We updated no rows when making a deposit.  This likely means an error");
		}
	}

	@Retry //make sure we retry in the rare chance we have someone else locking our row
	@Override
	public void withdrawFromAccount(long routingNumber, double amountToWithdraw) throws BankSystemException {
		//read sql from file
		String sql = propertiesUtil.getProperty(BankConstants.BANK_PROPS_FILE, WITHDRAW_SQL_PROP);

		//warn if sql is blank, but allow to continue
		if(StringUtils.isBlank(sql)) {
			log.warn("The sql for withdrawing money from an account is blank.  This will likely cause errors");
		}

		//build args
		Map<String, ?> args = Stream.of(
				new AbstractMap.SimpleEntry<>("routingNumber", routingNumber),
				new AbstractMap.SimpleEntry<>("amountToWithdraw", amountToWithdraw))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		//execute proc
		int numberOfRowsAffected;
		try {
			numberOfRowsAffected = namedTemplate.update(sql, args);
		}catch(DataAccessException e) {
			//handle exception by wrapping and throwing (interceptor will retry)
			throw new BankSystemException("", e); //TODO
		}

		//warn if we didn't update the expected number of rows -- NOTE: in future, may want to check to be greater than 0, this may affect other rows too if foreign keys/more complex sql
		if(numberOfRowsAffected < 1) {
			log.warn("We updated no rows when making a withdraw.  This likely means an error");
		}
	}

	@Override
	public TransferTransaction createTransaction(long fromAccountId, long toAccountId, double amount, Instant timestamp)
			throws BankSystemException {
		//read sql from file
		String sql = propertiesUtil.getProperty(BankConstants.BANK_PROPS_FILE, CREATE_TRANSFER_TRANSACTION_PROP);

		//warn if sql is blank, but allow to continue
		if(StringUtils.isBlank(sql)) {
			log.warn("The sql for creating a transaction is blank.  This will likely cause errors");
		}

		//build args		
		SqlParameterSource paramSource = new MapSqlParameterSource()
				.addValue("", fromAccountId) //TODO
				.addValue("", toAccountId) //TODO
				.addValue("", timestamp); //TODO
		
		//give us something to store the generated key into
		KeyHolder generatedKeyHolder = new GeneratedKeyHolder();

		//execute proc
		int numberOfRowsAffected;
		try {
			numberOfRowsAffected = namedTemplate.update(sql, paramSource, generatedKeyHolder);
		}catch(DataAccessException e) {
			//handle exception by wrapping and throwing (interceptor will retry)
			throw new BankSystemException("", e); //TODO
		}

		//warn if we didn't update the expected number of rows -- NOTE: in future, may want to check to be greater than 0, this may affect other rows too if foreign keys/more complex sql
		if(numberOfRowsAffected < 1) {
			log.warn("We updated no rows when making a new insert to the transaction table.  This likely means an error");
		}
		
		//build the transaction object to return
		TransferTransaction transaction = new TransferTransaction();
		transaction.setTransactionId((long) generatedKeyHolder.getKey());
		transaction.setFromAccountId(fromAccountId);
		transaction.setToAccountId(toAccountId);
		transaction.setTimestamp(timestamp);
		
		//return the built transaction
		return transaction;
	}

	/**
	 * Returns true if is lock a query
	 * @param sql
	 * @return
	 */
	private boolean isLockQuery(String sql) {		
		//ensure it contains the string (after upper-casing and making sure only has one space between words)
		return sql != null 
				&& sql
				.toUpperCase()
				.replaceAll("\\s+", " ")
				.contains(LOCK_SQL);		
	}
}