package com.connor.bank.factory;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.connor.bank.annotation.Datasource;

/**
 * 
 * @author Connors
 *
 */
public class JdbcTemplateFactory {

	@Datasource(name="") //non-binding, so we don't care
	@Produces
	public JdbcTemplate getJdbcTemplate(InjectionPoint ip) {
		//TODO
		return null;
	}
	
	@Datasource(name="") //non-binding, so we don't care
	@Produces
	public NamedParameterJdbcTemplate getNamedJdbcTemplate(InjectionPoint ip) {
		//TODO
		return null;
	}	
}