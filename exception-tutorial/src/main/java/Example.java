import com.connor.exception.checked.CheckedException;
import com.connor.exception.unchecked.UncheckedException;

public class Example {
	
	
	public static void main(String[] args) {
		//NOTE: have to put try catch here or compiler will yell at us and code won't work
		try {
			haveToHandleCheckedException();
		} catch (CheckedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//NOTE: don't have to try catch this, but we can.  This leaves freedom to developer, which I prefer
		dontHaveToHandleUncheckedException();
	}
	
	/**
	 * Note: don't have to declare unchecked exception, but we should in java doc
     * @throws CheckedException
	 */
	private static void dontHaveToHandleUncheckedException() {
		throw new UncheckedException();
	}

	/**
	 * Note: have to declare checked exception in method signature (should also put in java doc)
	 * @throws CheckedException
	 */
	private static void haveToHandleCheckedException() throws CheckedException{
		throw new CheckedException();
	}
	
	
}
