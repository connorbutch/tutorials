package com.connor.exception.unchecked;

/**
 * An unchecked exception.  I believe almost all exceptions should be unchecked, but written in java doc.
 * This let's the caller decide which exceptions to handle, and avoids a bunch of catch blocks where an 
 * exception is rethrown.
 * 
 * NOTE: any exception extending RuntimeException is an unchecked exception.
 * @author Connors
 *
 */
public class UncheckedException extends RuntimeException{

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 8119665753136247138L;

}
