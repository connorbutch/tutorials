package com.connor.exception.checked;

/**
 * This is a checked exception that must be either declared in method signature (throws CheckedException)
 * or handled with try/catch.  In my opinion, these should almost never be used, and instead favor runtime exceptions.
 * This is a pattern also followed by popular frameworks, such as spring framework.
 * 
 * NOTE: any exception extending Exception is considered a checked exception.
 * @author Connors
 *
 */
public class CheckedException extends Exception {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -3721623878842360429L;

}
