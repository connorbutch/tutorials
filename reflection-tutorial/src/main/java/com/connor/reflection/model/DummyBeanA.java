package com.connor.reflection.model;

import javax.inject.Inject;

public class DummyBeanA {

	
	@Inject
	private DummyBeanB dummyBeanB;

	public DummyBeanB getDummyBeanB() {
		return dummyBeanB;
	}
	
	
}
