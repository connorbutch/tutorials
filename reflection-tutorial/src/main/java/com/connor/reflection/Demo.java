package com.connor.reflection;

import com.connor.reflection.model.DummyBeanA;
import com.connor.reflection.model.DummyBeanB;

public class Demo {

	public static void main(String[] args) {
	ReflectionUtil reflectionUtil = new ReflectionUtil();
		
		//set the dummy bean b into the field on dummy bean a
		DummyBeanA dummyObjectToInjectInto = new DummyBeanA();
		DummyBeanB dummyValueToInject = new DummyBeanB(); 
		boolean wasSuccessful = reflectionUtil.inject(dummyObjectToInjectInto, dummyValueToInject);
		
		System.out.println("Successfully set field? " + wasSuccessful + " Field is null? " + dummyObjectToInjectInto.getDummyBeanB() == null);
	

	}

}
