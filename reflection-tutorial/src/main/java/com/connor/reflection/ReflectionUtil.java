package com.connor.reflection;

import java.lang.reflect.Field;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

public class ReflectionUtil {


	/**
	 * Attempts to inject to the first field assignable from the value
	 * @param toSetValueOn
	 * @param value
	 * @return
	 */
	public boolean inject(Object toSetValueOn, Object value) {
		boolean wasSetSuccessfully = false;		

		//loop through the fields, find the first assignable one
		if(toSetValueOn != null) {
			for(Field field: toSetValueOn.getClass().getDeclaredFields()) {
				if(canFieldBeAssignedFromValue(field, value)) {
					//attempt to set the value -- if we do, save as true and break
					try {
						field.set(toSetValueOn, value);
						wasSetSuccessfully = true;
						break;
					} catch (IllegalArgumentException | IllegalAccessException e) {
						//silence is already false
					}

				}
			}
		}		

		return wasSetSuccessfully;
	}

	/**
	 * Used to mock field injection.  Will only match on exact field name.  If injects, will return true,
	 * else false.
	 * @param toSetValueOn
	 * @param fieldName
	 * @param value
	 * @return
	 */
	public boolean inject(Object toSetValueOn, String fieldName, Object value) {
		boolean wasSetSuccessfully = false;

		//validate that it is a valid field
		Field field = getFieldWithName(toSetValueOn, fieldName);
		if(toSetValueOn != null && field != null && canFieldBeAssignedFromValue(field, value)  && doesHaveInjectAnnotation(field)) {			
			try {
				field.set(toSetValueOn, value);
				wasSetSuccessfully = true;
			} catch(Exception e) {
				//silence -- condensed to one block for simplicity -- we don't really care about the type of exception
			}
		}		

		//if we set it, this will be true -- else false
		return wasSetSuccessfully;
	}	

	/**
	 * Returns the field with the specified name.  If no field with the name exists, it returns false.
	 * @param toSetValueOn
	 * @param fieldName
	 * @return
	 */
	private Field getFieldWithName(Object toSetValueOn, String fieldName) {
		Field field = null;

		//only run if we get a valid input 
		if(toSetValueOn != null && StringUtils.isNotBlank(fieldName)) {
			for(Field classField: toSetValueOn.getClass().getDeclaredFields()) {
				//if we find a match, then save it and break out of loop
				if(classField != null && classField.getName().equals(fieldName)) {
					field = classField;
					break;
				}
			}
		}		

		//return the field -- will be null if no match on name
		return field;
	}

	/**
	 * Returns true if the field can be assigned from the given value
	 * @param field
	 * @param value
	 * @return
	 */
	private boolean canFieldBeAssignedFromValue(Field field, Object value) {
		return field.getType().isAssignableFrom(value.getClass()) && field.trySetAccessible();
	}

	/**
	 * 
	 * @param field
	 * @return
	 */
	private boolean doesHaveInjectAnnotation(Field field) {
		return field.isAnnotationPresent(Inject.class);
	}	
}
