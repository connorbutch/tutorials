package com.connor.generic.exception;

/**
 * Exception that is thrown when a class cannot convert a string to the given type.
 * @author Connors
 *
 */
public class InvalidFormatException extends RuntimeException{

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 3826025288605247564L;
	
	/**
	 * 
	 */
	private final String value;
	
	/**
	 * 
	 */
	private final Class<?> conversionClass;
	
	/**
	 * 
	 * @param value
	 * @param conversionClass
	 */
	public InvalidFormatException(String value, Class<?> conversionClass) {
		this.value = value;
		this.conversionClass = conversionClass;
	}

	/**
	 * Getter
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Getter
	 * @return
	 */
	public Class<?> getConversionClass() {
		return conversionClass;
	}

	
}
