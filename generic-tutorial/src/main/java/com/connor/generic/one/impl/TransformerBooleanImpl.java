package com.connor.generic.one.impl;

import com.connor.generic.one.api.AbstractBaseTransformer;

public class TransformerBooleanImpl extends AbstractBaseTransformer<Boolean>{

	@Override
	protected Boolean transformNull() {
		return false;
	}

	@Override
	protected Boolean transformEmpty() {
		return false;
	}

	@Override
	protected Boolean transformBlank(String input) {
		return false;
	}

	@Override
	protected Boolean transformNotBlank(String input) {
		//trim it
		input = input.trim();
		
		//will return true for equals ignore case on true, else false
		return Boolean.parseBoolean(input);
	}

}
