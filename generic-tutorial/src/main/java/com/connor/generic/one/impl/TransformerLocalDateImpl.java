package com.connor.generic.one.impl;

import java.text.DateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.connor.generic.exception.InvalidFormatException;
import com.connor.generic.one.api.AbstractBaseTransformer;

public class TransformerLocalDateImpl extends AbstractBaseTransformer<LocalDate> {

	/**
	 * Used to format dates
	 */
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("MM-dd-yyyy");

	@Override
	protected LocalDate transformNull() {
		return null;
	}

	@Override
	protected LocalDate transformEmpty() {
		return null;
	}

	@Override
	protected LocalDate transformBlank(String input) {
		return null;
	}

	@Override
	protected LocalDate transformNotBlank(String input) {
		//already know it is not null, so trim
		input = input.trim();

		try {
			return LocalDate.parse(input, FORMATTER);
		}catch(DateTimeException e) {
			throw new InvalidFormatException(input, LocalDate.class);
		}
	}
}