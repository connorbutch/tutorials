package com.connor.generic.one.impl;

import com.connor.generic.exception.InvalidFormatException;
import com.connor.generic.one.api.AbstractBaseTransformer;

/**
 * This class transforms a string to an integer
 * @author Connors
 *
 */
public class TransformerIntegerImpl extends AbstractBaseTransformer<Integer>{

	@Override
	protected Integer transformNull() {
		return null;
	}

	@Override
	protected Integer transformEmpty() {
		return 0;
	}

	@Override
	protected Integer transformBlank(String input) {
		return 0;
	}

	@Override
	protected Integer transformNotBlank(String input) {
		//remove leading/trailing whitespace (as we know it is not null)
		input = input.trim();
		
		try {
			return Integer.parseInt(input);
		}catch(NumberFormatException e) {
			throw new InvalidFormatException(input, Integer.class);
		}
	}
}