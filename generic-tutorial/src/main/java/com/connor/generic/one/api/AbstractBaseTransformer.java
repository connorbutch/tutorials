package com.connor.generic.one.api;

import java.lang.reflect.ParameterizedType;

import org.apache.commons.lang3.StringUtils;

/**
 * This is a base class for transformer of one value.  All implementations should extend this (not the interface)
 * @author Connors
 *
 * @param <T>
 */
public abstract class AbstractBaseTransformer<T> implements Transformer<T> {

	/**
	 * Used to keep reference to class type
	 */
	private final Class<T> clazz;
	
	/**
	 * Constructor to be called from child classes
	 */
	@SuppressWarnings("unchecked")
	protected AbstractBaseTransformer() {
		clazz = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	@Override
	public T transform(String input) {
		//what we return
		T value = null;
		
		if(input == null) {
			value =  transformNull();
		}else if(StringUtils.isEmpty(input)) {
			value = transformEmpty();
		}else if(StringUtils.isBlank(input)) {
			value = transformNotBlank(input);
		}else {
			value = transformNotBlank(input);
		}
		
		return value;
	}
	
	@Override
	public Class<T> getMappedFieldType() {
		return clazz;
	}
	
	/**
	 * Handle null
	 * @return
	 */
	protected abstract T transformNull();
	
	/**
	 * Handle empty
	 * @return
	 */
	protected abstract T transformEmpty();
	
	/**
	 * Handle blank
	 * @param input
	 * @return
	 */
	protected abstract T transformBlank(String input);
	
	/**
	 * Handle general case
	 * @param input
	 * @return
	 */
	protected abstract T transformNotBlank(String input);

	
}
