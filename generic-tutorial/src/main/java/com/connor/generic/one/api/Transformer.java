package com.connor.generic.one.api;

import com.connor.generic.exception.InvalidFormatException;

/**
 * Transforms a string to a given type.
 * @author Connors
 *
 * @param <T>
 */
public interface Transformer<T> {

	/**
	 * Transforms the input to a generic type. 
	 * @param input the string to convert
	 * @return the converted value
	 * @throws InvalidFormatException if value cannot be converted
	 */
	T transform(String input);
	
	/**
	 * Get the generic class associated with this transformer
	 * @return
	 */
	Class<T> getMappedFieldType();

}
