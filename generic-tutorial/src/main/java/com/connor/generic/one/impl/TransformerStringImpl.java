package com.connor.generic.one.impl;

import org.apache.commons.lang3.NotImplementedException;

import com.connor.generic.one.api.AbstractBaseTransformer;

public class TransformerStringImpl extends AbstractBaseTransformer<String>{

	
	@Override
	public String transform(String input) {
		return input;
	}

	@Override
	protected String transformNull() {
		throw new NotImplementedException("This should not be called");
	}

	@Override
	protected String transformEmpty() {
		throw new NotImplementedException("This should not be called");
	}

	@Override
	protected String transformBlank(String input) {
		throw new NotImplementedException("This should not be called");
	}

	@Override
	protected String transformNotBlank(String input) {
		throw new NotImplementedException("This should not be called");
	}	
}