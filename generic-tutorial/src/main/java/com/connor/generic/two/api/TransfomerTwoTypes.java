package com.connor.generic.two.api;

import java.io.Serializable;

/**
 * Transforms from source class (s) to destination class (d)
 * @author Connors
 *
 * @param <S>
 * @param <D>
 */
public interface TransfomerTwoTypes<S, D> extends Serializable{

	/**
	 * Transforms input to output
	 * @param input
	 * @return
	 */
	D transform(S input);
	
}
