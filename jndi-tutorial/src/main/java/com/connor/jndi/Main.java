package com.connor.jndi;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.NamingManager;

/**
 * Driver class for an example of using simple jndi implemented here.
 * @author Connors
 *
 */
public class Main {

	public static void main(String[] args) {
		try {
			//this can be done only once, and has to be done before using context
			NamingManager.setInitialContextFactoryBuilder(new ContextFactoryAndBuilderDemoImpl());
			InitialContext context = new InitialContext();
			System.out.println(context.lookup("test"));
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
