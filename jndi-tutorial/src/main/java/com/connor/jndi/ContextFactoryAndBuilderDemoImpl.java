package com.connor.jndi;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import javax.naming.spi.InitialContextFactoryBuilder;

public class ContextFactoryAndBuilderDemoImpl implements InitialContextFactory, InitialContextFactoryBuilder {

	/**
	 * Comes from initial context factory
	 */
	@Override
	public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
		return new ContextDemoImpl();
	}

	/**
	 * Comes from the builder
	 * @param environment
	 * @return
	 * @throws NamingException
	 */
	@Override
	public InitialContextFactory createInitialContextFactory(Hashtable<?, ?> environment) throws NamingException {
		return this;
	}

}
