package com.connor.spring.exception;

public class PetNotFoundException extends Exception {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 8419354812671708182L;

	/**
	 * 
	 */
	private final long id;

	public PetNotFoundException(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}



}
