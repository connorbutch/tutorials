package com.connor.spring.exception;

import java.util.List;

import com.connor.spring.model.ErrorEnum;

/**
 * This is a checked exception that gets thrown when we validate a request.
 * @author Connors
 *
 */
public class InvalidRequestException extends Exception {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 9216788403608288892L;
	
	/**
	 * The errors
	 */
	private final List<ErrorEnum> errors;

	/**
	 * 
	 * @param errors
	 */
	public InvalidRequestException(List<ErrorEnum> errors) {
		this.errors = errors;
	}

	/**
	 * 
	 * @return
	 */
	public List<ErrorEnum> getErrors() {
		return errors;
	}	
}