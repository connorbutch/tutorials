package com.connor.spring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.connor.spring.exception.InvalidRequestException;
import com.connor.spring.model.ErrorResponse;
import com.connor.spring.model.Pet;
import com.connor.spring.service.PetService;
import com.connor.spring.util.ErrorResponseBuilder;
import com.connor.spring.util.PetValidator;

/**
 * Controller for a spring boot restful webservice.
 * @author Connors
 *
 */
@RestController
public class PetController {

	private final ErrorResponseBuilder builder;

	private final PetValidator validator;

	private final PetService petService;
	
	/**
	 * Cdi-enabled constructor
	 * @param builder
	 * @param validator
	 * @param petService
	 */
	@Inject //NOTE: we can use jsr330 here, but still uses spring di, so must annotate beans with stereotype of component or stereotype containing it
	public PetController(ErrorResponseBuilder builder, PetValidator validator, PetService petService) {
		this.builder = builder;
		this.validator = validator;
		this.petService = petService;
	}

	@GetMapping("/pets")
	@ResponseBody
	public ResponseEntity<?> getAllPets(@RequestParam Integer limit, @RequestParam Integer offset){
		//what we return
		ResponseEntity<?> responseBody = null;

		try {
			validator.validateGetAllPets(limit, offset);
			//TODO query dao
		} 
		//if we reached here, we had invalid input
		catch (InvalidRequestException e) {
			ErrorResponse body = builder.getResponseFromException(e);
			responseBody = new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
		}

		return responseBody;
	}

	@PostMapping("/pets")
	public ResponseEntity<?> createPet(@RequestBody Pet pet) {
		//what we return
		ResponseEntity<?> responseBody = null;

		try {
			validator.validateCreatePet(pet);
		} catch (InvalidRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseBody;
	}

	@PutMapping("/pets/{id}")
	public ResponseEntity<?> replacePet(@PathVariable Long id, @RequestBody Pet pet) {
		//what we return
		ResponseEntity<?> responseBody = null;

		try {
			validator.validateUpdatePet(id, pet);
		} catch (InvalidRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseBody;
	}

	@DeleteMapping("/pets/{id}")
	public ResponseEntity<?> deletePet(@PathVariable Long id){
		//what we return
		ResponseEntity<?> responseBody = null;

		try {
			validator.validateDeletePet(id);
		} catch (InvalidRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseBody;

	}
}
