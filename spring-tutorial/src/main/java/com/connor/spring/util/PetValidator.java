package com.connor.spring.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.connor.spring.exception.InvalidRequestException;
import com.connor.spring.model.ErrorEnum;
import com.connor.spring.model.Pet;

/**
 * This class validates pet inputs, and throws exception specifying invalid input if 
 * anything is incorrect.
 * @author Connors
 *
 */
@Component
public class PetValidator {


	public void validateGetAllPets(Integer limit, Integer offset) throws InvalidRequestException {
		List<ErrorEnum> errors = new ArrayList<>();
		if(limit != null && limit < 0) {
			errors.add(ErrorEnum.LIMIT_IS_INVALID);
		}

		if(offset != null && offset < 0) {
			errors.add(ErrorEnum.OFFSET_IS_INVALID);
		}

		if(!errors.isEmpty()) {
			throw new InvalidRequestException(errors);
		}
	}

	public void validateGetPet(Long id) throws InvalidRequestException {		
		if(id != null && id < 0) {
			throw new InvalidRequestException(Arrays.asList(ErrorEnum.INVALID_PET_ID));
		}
	}


	public void validateCreatePet(Pet pet) throws InvalidRequestException {
		List<ErrorEnum> errors = new ArrayList<>();
		if(pet == null) {
			errors.add(ErrorEnum.PET_IS_REQUIRED_IN_BODY);
		}else if(pet.getName() == null) {
			errors.add(ErrorEnum.NAME_IS_REQUIRED);
		}

		if(!errors.isEmpty()) {
			throw new InvalidRequestException(errors);
		}
	}	

	public void validateUpdatePet(Long id, Pet pet) throws InvalidRequestException{
		List<ErrorEnum> errors = new ArrayList<>();
		//TODO
		if(id == null) {
			errors.add(ErrorEnum.INVALID_PET_ID);
		}

		if(pet == null) {
			errors.add(ErrorEnum.PET_IS_REQUIRED_IN_BODY);
		}

		if(pet != null && id != pet.getId()) {
			errors.add(ErrorEnum.ID_IN_PATH_MUST_MATCH_THAT_IN_BODY);
		}

		if(pet != null && pet.getName() == null) {
			errors.add(ErrorEnum.NAME_IS_REQUIRED);
		}

		if(!errors.isEmpty()) {
			throw new InvalidRequestException(errors);
		}
	}

	public void validateDeletePet(Long id) throws InvalidRequestException{
		if(id == null) {
			throw new InvalidRequestException(Arrays.asList(ErrorEnum.INVALID_PET_ID));
		}
	}
}