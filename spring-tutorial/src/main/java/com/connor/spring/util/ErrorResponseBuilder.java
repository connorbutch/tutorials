package com.connor.spring.util;

import org.springframework.stereotype.Component;

import com.connor.spring.exception.InvalidRequestException;
import com.connor.spring.model.ErrorResponse;

/**
 * This class builds error responses (response body) from various inputs
 * @author Connors
 *
 */
@Component
public class ErrorResponseBuilder {

	/**
	 * Get an error response from an exception
	 * @param ex
	 * @return
	 */
	public ErrorResponse getResponseFromException(InvalidRequestException ex) {
		//TODO
		return null;
	}
	
}
