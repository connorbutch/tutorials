package com.connor.spring.model;

/**
 * This is the interface containing constants for the pet classes.
 * @author Connors
 *
 */
public interface IPetConstants {

	public static final int DEFAULT_OFFSET = 0;
	
	public static final int DEFAULT_LIMIT = 50;
	
}
