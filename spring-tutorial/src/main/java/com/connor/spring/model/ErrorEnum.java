package com.connor.spring.model;

public enum ErrorEnum {

	PET_NOT_FOUND("Pet not found with specified id", 1),
	
	INVALID_PET_ID("Pet id cannot be negative", 2),
	
	NAME_IS_REQUIRED("Pet name is required", 3),
	
	OFFSET_IS_INVALID("Offset cannot be negative", 4),
	
	LIMIT_IS_INVALID("Limit cannot be negative", 5),
	
	PET_IS_REQUIRED_IN_BODY("Pet is required in body", 6),
	
	ID_IN_PATH_MUST_MATCH_THAT_IN_BODY("Pet id used as path param must match pet id in body", 7);
	
	private final String message;
	
	private final int code;
	
	/**
	 * Constructor
	 * @param message
	 * @param code
	 */
	private ErrorEnum(String message, int code) {
		this.message = message;
		this.code = code;
	}
	
}
