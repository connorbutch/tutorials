package com.connor.spring.model;

import java.util.List;

public class ErrorResponse {

	private final List<ErrorEnum> errors;

	public ErrorResponse(List<ErrorEnum> errors) {
		this.errors = errors;
	}	
}