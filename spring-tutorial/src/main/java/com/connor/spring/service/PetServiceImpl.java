package com.connor.spring.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.connor.spring.dao.PetDao;
import com.connor.spring.model.Pet;

@Service
public class PetServiceImpl implements PetService{

	private final PetDao petDao;
	
	/**
	 * Cdi-enabled constructor
	 * @param petDao
	 */
	@Inject
	public PetServiceImpl(PetDao petDao) {
		this.petDao = petDao;
	}

	public Pet getPet(Long id) {
		return null; //TODO
	}
	
	public Pet deletePet(Long id) {
		return null; //TODO
	}
	
	public Pet updatePet(Long id, Pet pet) {
		return null; //TODO
	}
	
	public List<Pet> getPets(Integer limit, Integer offset){
		return new ArrayList<>(); //TODO
	}
	
}
