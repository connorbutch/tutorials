package com.connor.spring.service;

import java.util.List;


import com.connor.spring.model.Pet;

/**
 * Interface for a pet service
 * @author Connors
 *
 */
public interface PetService {

	public Pet getPet(Long id);
	
	public Pet deletePet(Long id);
	
	public Pet updatePet(Long id, Pet pet);
	
	public List<Pet> getPets(Integer limit, Integer offset);
}