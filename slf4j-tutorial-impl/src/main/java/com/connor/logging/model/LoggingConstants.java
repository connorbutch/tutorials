package com.connor.logging.model;

/**
 * Contains constants for logging.
 * @author Connors
 *
 */
public interface LoggingConstants {

	/**
	 * 
	 */
	public static final String ERROR_PREFACE = "";
	
	/**
	 * 
	 */
	public static final String WARN_PREFACE = "";
	
	/**
	 * 
	 */
	public static final String DEBUG_PREFACE = "";
	
	/**
	 * 
	 */
	public static final String INFO_PREFACE = "";
	
}
