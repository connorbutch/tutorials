/* File: initializeH2Database.sql
 * Author: Connor Butch
 * Version: 1.0.0
 * Purpose: Initialize the h2 database with sample data
 * NOTE: this is not production ready sql (as H2 is an 
 * in memory database designed for testing)
 */

--begin ddl statements
--create the schema used by all tables
CREATE SCHEMA HOTEL;

--room table
CREATE TABLE HOTEL.ROOM(
	ROOM_NUMBER INT PRIMARY KEY NOT NULL,
	IS_HANDICAP_ACCESSIBLE BOOLEAN NOT NULL DEFAULT FALSE,
	NUMBER_OF_BEDS INT NOT NULL
);

--reservation table
CREATE TABLE HOTEL.RESERVATION(
	RESERVATION_SID INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	CHECKIN DATE NOT NULL,
	CHECKOUT DATE NOT NULL,
	--enforce check in is before checkout
	CHECK(CHECKIN < CHECKOUT),
	--enforce check in is in future
	CHECK(CHECKIN > TODAY)
);

/* room-to-reservation join table (denoted by the _rel suffix)
 * NOTE: this is needed because this is a m:n relationship -- 
 * meaning a room can have one or more reservations and a reservation
 * must have one or more rooms associated with it
 */
CREATE TABLE ROOM_RESERVATION_REL(
	ROOM_NUMBER INT NOT NULL,
	RESERVATION_SID INT NOT NULL,
	--add our foreign key checks first
	FOREIGN_KEY(ROOM_NUMBER) REFERENCES HOTEL.ROOM(ROOM_NUMBER),
	FOREIGN_KEY(RESERVATION_SID) REFERENCES HOTEL.RESERVATION(RESERVATION_SID),
	--make our primary key (NOTE: h2 does not support unique index, which is implicit on pk)
	PRIMARY KEY(ROOM_NUMBER, RESERVATION_SID)
);

--begin dml statements (to populate tables)
--TODO