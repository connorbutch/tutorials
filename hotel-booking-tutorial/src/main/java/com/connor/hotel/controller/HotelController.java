package com.connor.hotel.controller;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;

import com.connor.hotel.exception.HotelServiceException;
import com.connor.hotel.exception.LoadPropertyException;
import com.connor.hotel.handler.ErrorResponseBuilder;
import com.connor.hotel.model.HotelConstants;
import com.connor.hotel.model.Reservation;
import com.connor.hotel.model.Room;
import com.connor.hotel.service.HotelService;
import com.connor.hotel.util.PropertyUtil;

/**
 * The controller class for handling requests.
 * @author Connors
 *
 */
@ApplicationPath("/v1/") //the base path for our application
@Consumes({MediaType.APPLICATION_JSON}) //consume json (much less verboose than xml and still structured)
@Produces({MediaType.APPLICATION_JSON}) //also produces light-weight json
public class HotelController extends Application {

	/**
	 * Use this as the key for whether or not we deleted a reservation.
	 * This is required because RESTful webservice response codes dictate that
	 * delete must be idempotent (meaning it must return a 200 at the end if 
	 * the entity doesn't exist, regardless of if we actually deleted it or if
	 * it didn't exist before)
	 */
	private static final String DID_DELETE_RESPONSE_HEADER = "didDelete";

	/**
	 * The property for the base url
	 */
	private static final String BASE_URL_PROP = "baseUrl";

	/**
	 * Logger.
	 */
	private final Logger log;

	/**
	 * Use to keep translation of exceptions to responses in a single place.
	 */
	private final ErrorResponseBuilder errorResponseBuilder;

	/**
	 * The business logic layer for the application.
	 */
	private final HotelService hotelService;

	/**
	 * Used for reading in properties
	 */
	private final PropertyUtil propertyUtil;

	/**
	 * Cdi-enabled constructor.
	 * @param log
	 * @param errorResponseBuilder
	 * @param hotelService
	 * @param propertyUtil
	 */
	@Inject
	public HotelController(Logger log, ErrorResponseBuilder errorResponseBuilder, HotelService hotelService,  PropertyUtil propertyUtil) {
		this.log = log;
		this.errorResponseBuilder = errorResponseBuilder;
		this.hotelService = hotelService;
		this.propertyUtil = propertyUtil;
	}

	@GET
	@Path("rooms/")
	public Response getAvailableRooms(@QueryParam(value="checkin") LocalDate checkin, @QueryParam(value="checkout") LocalDate checkout) {
		//what we return
		Response response = null;

		//call our service, which contains business logic
		try {
			List<Room> availableRooms =  hotelService.getAvailableRooms(checkin, checkout);

			//if we got here, we are successful, return a 200 to client with the body being the available rooms
			response = Response.ok().entity(availableRooms).build(); //TODO maybe in future add header for total number of rooms and implement paging
		} catch (HotelServiceException e) {
			//since we are at the "top" layer, we need to log here where we have most info
			String errorMessage = String.format("An error was encountered when searching for rooms with check in date %s and checkout date %s", checkin, checkout);
			log.error(errorMessage, e);

			//make call to error builder, which provides common point of maintenance for responses from exceptions
			response = errorResponseBuilder.getResponse(e);
		}

		//either way -- return it
		return response;
	}

	@GET
	@Path("rooms/{id}")
	public Response getRoom(@PathParam(value = "id") int roomNumber) {
		//what we return
		Response response = null;

		try {
			Room room = hotelService.getRoomById(roomNumber);
			response = Response.ok().entity(room).build();
		} catch (HotelServiceException e) {
			//log
			String errorMessage = String.format("An error was encountered when searching for room based on id %d", roomNumber);
			log.error(errorMessage, e);

			//build the response for this exception
			response = errorResponseBuilder.getResponse(e);
		}

		//either way -- return it
		return response;
	}

	@POST
	@Path("reservations/")
	public Response placeReservation(@BeanParam Reservation reservation) {
		//what we return
		Response response = null;

		try {
			//call the business logic
			Reservation responseBody = hotelService.placeReservation(reservation);

			//build the response (hopefully with the location header for 201 response)
			response = getResponseForCreatedReservation(responseBody);
		} catch (HotelServiceException e) {
			//log
			String errorMessage = String.format("An error was enountered when placing reservation %s", reservation.toString());
			log.error(errorMessage, e);

			//call shared logic to build the response
			response = errorResponseBuilder.getResponse(e);
		}

		//either way -- return it
		return response;
	}

	@PUT
	@Path("reservations/{id}")
	public Response updateReservation(@PathParam(value="id")long reservationId, @BeanParam Reservation reservation) {
		//what we return
		Response response = null;

		try {
			//call the business logic layer
			Reservation responseBody = hotelService.updateReservation(reservationId, reservation);

			//on success, return a 200 with the updated reservation as the response body
			return Response.ok().entity(responseBody).build();
		} catch (HotelServiceException e) {
			//log
			String errorMessage = String.format("An error occurred when updating reservation with id %d with value %s", reservationId, reservation);
			log.error(errorMessage, e);

			//call shared logic to build response
			response = errorResponseBuilder.getResponse(e);
		}

		//either way -- return it
		return response;
	}

	@DELETE
	@Path("reservations/{id}")
	public Response cancelReservation(@PathParam(value="id") long reservationId) {
		//what we return
		Response response = null;

		try {
			//call business logic, and get whether or not we deleted a reservation
			boolean didDelete = hotelService.cancelReservation(reservationId);

			//build the string for the diddelete header, and log if didn't delete
			String didDeleteValue;
			if(didDelete) {
				didDeleteValue = Boolean.TRUE.toString();
			}else {
				//if not, log to warn that we received request in addition to setting value to false
				log.warn(String.format("Recieved a request to cancel a reservation that does not exist (id %d)", reservationId));
				didDeleteValue = Boolean.FALSE.toString();
			}

			//build the response with the ok status code and the "did delete" header
			response = Response.ok().header(DID_DELETE_RESPONSE_HEADER, didDeleteValue).build();			
		} catch (HotelServiceException e) {
			//log
			String errorMessage = String.format("An error was encountered canceling reservation with id %d", reservationId);
			log.error(errorMessage, e);

			//call shared logic to build response
			response = errorResponseBuilder.getResponse(e);
		}

		//either way -- return it
		return response;
	}

	/**
	 * Gets the base url for this web service
	 * @return
	 * @throws LoadPropertyException 
	 */
	private String getBaseUrl() throws LoadPropertyException {
		return propertyUtil.getProperty(HotelConstants.HOTEL_SQL_PROPS_FILE_PATH, BASE_URL_PROP);
	}

	/**
	 * Returns a response for the created reservation.  It defaults to one with the location header (http standard for created post request responses)
	 * but if it can't create one, return a 201 without the header
	 * @param responseBody
	 * @return
	 */
	private Response getResponseForCreatedReservation(Reservation responseBody) {
		Response response = null;

		//build the location header
		StringBuilder sb = new StringBuilder();
		try {
			sb = new StringBuilder(getBaseUrl());
			sb.append("/");
			sb.append(responseBody.getReservationId());
			URI location = null;			
			location = new URI(sb.toString());
			response = Response.created(location).entity(responseBody).build();
		}catch(Exception e) {
			//intentionally silence -- one these shouldn't happen, and two, we don't care, we will default to the response without the created header
			log.warn("Couldn't create location header for response, will still be returned, just without header", e);

			//fall back on the default without a header
			response = Response.status(Status.CREATED).entity(responseBody).build();
		}

		return response;
	}
}