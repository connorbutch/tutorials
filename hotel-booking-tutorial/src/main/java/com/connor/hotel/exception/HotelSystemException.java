package com.connor.hotel.exception;

public class HotelSystemException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7658435252114523238L;

	/**
	 * Constructor
	 * @param message
	 * @param t
	 */
	public HotelSystemException(String message, Throwable t) {
		super(message, t);
	}

	//NOTE: no need to override toString here, we are not adding info, so parent is fine.

}
