package com.connor.hotel.exception;

public class HotelServiceException extends Exception {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -7107064356302593982L;

	public HotelServiceException(String message) {
		super(message);
	}

	public HotelServiceException(String message, Throwable t) {
		super(message, t);
	}

	//NOTE: no need to override toString here, we are not adding info, so parent is fine.

}
