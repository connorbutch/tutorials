package com.connor.hotel.exception;

public class HotelDbException extends HotelSystemException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7094208874410726457L;
	
	/**
	 * 
	 */
	private static final String ERROR_FORMAT_STR = "The sql executed was %s";
	
	/**
	 * The sql ran
	 */
	private final String sql;
	
	public HotelDbException(String message, Throwable t, String sql) {
		super(message, t);
		this.sql = sql;
	}
	
	@Override
	public String getMessage() {
		/**
		 * NOTE: overriding get message is VERY VERY important for custom exceptions.  
		 * This is what gets called implicitly by your logger, so what gets returned
		 * here should be as specific as possible to make debugging as painless as can be.
		 */
		
		//use string builder over string -- more efficient
		StringBuilder sb = new StringBuilder();
		
		//get the message from parent exception
		String message = super.getMessage();
		if(message != null) {
			sb.append(message);
		}
		
		//add specific fields from this class
		sb.append(String.format(ERROR_FORMAT_STR, sql));
		
		//return the built message
		return sb.toString();
	}

}
