package com.connor.hotel.exception;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 * 
 * @author Connors
 *
 */
public class HotelDbParamSourceException extends HotelDbException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -8510122116773505117L;
	
	/**
	 * 
	 */
	private static final String ERROR_FORMAT_STR = "The parameters used to call the proc were %s";


	/**
	 * 
	 */
	private final SqlParameterSource paramSource;
	
	
	/**
	 * Constructor
	 * @param message
	 * @param t
	 * @param sql
	 * @param paramSource
	 */
	public HotelDbParamSourceException(String message, Throwable t, String sql, SqlParameterSource paramSource) {
		super(message, t, sql);
		this.paramSource = paramSource;
	}
	
	@Override
	public String getMessage() {
		/**
		 * NOTE: overriding get message is VERY VERY important for custom exceptions.  
		 * This is what gets called implicitly by your logger, so what gets returned
		 * here should be as specific as possible to make debugging as painless as can be.
		 */
		
		//use string builder over string -- more efficient
		StringBuilder sb = new StringBuilder();
		
		//get the message from parent exception
		String message = super.getMessage();
		if(message != null) {
			sb.append(message);
		}
		
		//add specific fields from this class
		sb.append(String.format(ERROR_FORMAT_STR, paramSource.toString()));
		
		//return the built message
		return sb.toString();
	}

}
