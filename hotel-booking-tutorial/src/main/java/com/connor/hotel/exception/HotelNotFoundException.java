package com.connor.hotel.exception;

public class HotelNotFoundException extends HotelServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5807867261636765775L;
	
	/**
	 * 
	 */
	private final long id;
	
	/**
	 * 
	 */
	private static final String ERROR_FORMAT_STR = "The id not found was %d";
	
	/**
	 * 
	 * @param message
	 * @param id
	 */
	public HotelNotFoundException(String message, long id) {
		super(message);
		this.id = id;
	}
	
	@Override
	public String getMessage() {
		/**
		 * NOTE: overriding get message is VERY VERY important for custom exceptions.  
		 * This is what gets called implicitly by your logger, so what gets returned
		 * here should be as specific as possible to make debugging as painless as can be.
		 */
		
		//use string builder over string -- more efficient
		StringBuilder sb = new StringBuilder();
		
		//get the message from parent exception
		String message = super.getMessage();
		if(message != null) {
			sb.append(message);
		}
		
		//add specific fields from this class
		sb.append(String.format(ERROR_FORMAT_STR, id));
		
		//return the built message
		return sb.toString();
	}
}