package com.connor.hotel.exception;

import java.util.Map;

public class HotelDbMultipleParamsException extends HotelDbException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1569636652786797499L;
	
	/**
	 * 
	 */
	private static final String ERROR_FORMAT_STR = "The parameters used to call the proc were %s";

	/**
	 * 
	 */
	private final Map<String, ?> parameters;

	/**
	 * 
	 * @param message
	 * @param t
	 * @param sql
	 * @param parameters
	 */
	public HotelDbMultipleParamsException(String message, Throwable t, String sql, Map<String, ?> parameters) {
		super(message, t, sql);
		this.parameters = parameters;
	}

	@Override
	public String getMessage() {
		/**
		 * NOTE: overriding get message is VERY VERY important for custom exceptions.  
		 * This is what gets called implicitly by your logger, so what gets returned
		 * here should be as specific as possible to make debugging as painless as can be.
		 */
		
		//use string builder over string -- more efficient
		StringBuilder sb = new StringBuilder();
		
		//get the message from parent exception
		String message = super.getMessage();
		if(message != null) {
			sb.append(message);
		}
		
		//add specific fields from this class
		sb.append(String.format(ERROR_FORMAT_STR, parameters.toString()));
		
		//return the built message
		return sb.toString();
	}

	
}