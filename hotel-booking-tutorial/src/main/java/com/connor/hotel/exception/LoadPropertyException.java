package com.connor.hotel.exception;

public class LoadPropertyException extends HotelSystemException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 940381774499835165L;
	
	/**
	 * 
	 */
	private static final String ERROR_FORMAT_STR = "The file path was %s and the property was %s";

	/**
	 * 
	 */
	private final String propertyFilePath;
	
	/**
	 * 
	 */
	private final String property;
	
	/**
	 * Constructor
	 * @param message
	 * @param t
	 * @param propertyFilePath
	 * @param property
	 */
	public LoadPropertyException(String message, Throwable t, String propertyFilePath, String property) {
		super(message, t);
		this.propertyFilePath = propertyFilePath;
		this.property = property;
	}

	@Override
	public String getMessage() {
		/**
		 * NOTE: overriding get message is VERY VERY important for custom exceptions.  
		 * This is what gets called implicitly by your logger, so what gets returned
		 * here should be as specific as possible to make debugging as painless as can be.
		 */
		
		//use string builder over string -- more efficient
		StringBuilder sb = new StringBuilder();
		
		//get the message from parent exception
		String message = super.getMessage();
		if(message != null) {
			sb.append(message);
		}
		
		//add specific fields from this class
		sb.append(String.format(ERROR_FORMAT_STR, propertyFilePath, property));
		
		//return the built message
		return sb.toString();
	}
}