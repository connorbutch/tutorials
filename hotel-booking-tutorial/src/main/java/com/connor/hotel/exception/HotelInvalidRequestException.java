package com.connor.hotel.exception;

import java.util.Collections;
import java.util.List;

import com.connor.hotel.model.HotelError;

public class HotelInvalidRequestException extends HotelServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3120798192336895216L;
	
	/**
	 * 
	 */
	private static final String ERROR_FORMAT_STR = "The invalid messages associated with this request are %s";

	
	/**
	 * 
	 */
	private final List<HotelError> errors;
	
	/**
	 * 
	 * @param message
	 * @param errors
	 */
	public HotelInvalidRequestException(String message, List<HotelError> errors) {
		super(message);
		this.errors = Collections.unmodifiableList(errors);
	}
	
	@Override
	public String getMessage() {
		/**
		 * NOTE: overriding get message is VERY VERY important for custom exceptions.  
		 * This is what gets called implicitly by your logger, so what gets returned
		 * here should be as specific as possible to make debugging as painless as can be.
		 */
		
		//use string builder over string -- more efficient
		StringBuilder sb = new StringBuilder();
		
		//get the message from parent exception
		String message = super.getMessage();
		if(message != null) {
			sb.append(message);
		}
		
		//add specific fields from this class
		StringBuilder errorsStrBuilder = getListOfErrors();
		if(errorsStrBuilder.length() > 0) {
			sb.append(String.format(ERROR_FORMAT_STR, errorsStrBuilder.toString()));
		}
		
		//return the built message
		return sb.toString();
	}
	
	/**
	 * Gets a stringbuilder containing the list of errors
	 * @return
	 */
	private StringBuilder getListOfErrors() {
		StringBuilder sb = new StringBuilder();
		
		//TODO bring in collection utils
		if(errors != null && !errors.isEmpty()) {
			for(HotelError error : errors) {
				sb.append(error.toString() + "\n");
			}
		}
		
		//return it
		return sb;
	}
}