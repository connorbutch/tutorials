package com.connor.hotel.exception;

public class HotelDbArrayParamsException extends HotelDbException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -8409115180568030425L;

	private static final String ERROR_FORMAT_STR = "The args used were %s";
	
	/**
	 * The args
	 */
	private final Object[] args;

	/**
	 * Constructor
	 * @param message
	 * @param t
	 * @param sql
	 * @param args
	 */
	public HotelDbArrayParamsException(String message, Throwable t, String sql, Object[] args) {
		super(message, t, sql);
		this.args = args;
	}

	@Override
	public String getMessage() {
		/**
		 * NOTE: overriding get message is VERY VERY important for custom exceptions.  
		 * This is what gets called implicitly by your logger, so what gets returned
		 * here should be as specific as possible to make debugging as painless as can be.
		 */

		//use string builder over string -- more efficient
		StringBuilder sb = new StringBuilder();

		//get the message from parent exception
		String message = super.getMessage();
		if(message != null) {
			sb.append(message);
		}

		//add specific fields from this class
		sb.append(String.format(ERROR_FORMAT_STR, args.toString()));
		
		//return the built message
		return sb.toString();
	}
}