package com.connor.hotel.service;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;

import com.connor.hotel.dao.HotelDao;
import com.connor.hotel.exception.HotelNotFoundException;
import com.connor.hotel.exception.HotelServiceException;
import com.connor.hotel.exception.HotelSystemException;
import com.connor.hotel.model.Reservation;
import com.connor.hotel.model.Room;

/**
 * Service for hotel webservice.
 * @author Connors
 *
 */
public class HotelServiceImpl implements HotelService {	

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -741648665612918802L;

	/**
	 * Used to validate requests
	 */
	private final HotelValidator validator;

	/**
	 * The dao instance
	 */
	private final HotelDao dao;


	/**
	 * Cdi-enabled constructor.
	 * @param validator
	 * @param dao
	 */
	@Inject
	public HotelServiceImpl(HotelValidator validator, HotelDao dao) {
		this.validator = validator;
		this.dao = dao;
	}

	@Override
	public Room getRoomById(int roomNumber) throws HotelServiceException {
		//validate the request -- if invalid request, then let exception be handled up call stack 
		validator.validateGetRoomById(roomNumber);		

		//what we return
		Room room = null;

		//find the room from the db
		try {
			room = dao.getRoomById(roomNumber);
		} catch (HotelSystemException e) {
			//system exceptions are things we have no control over, so wrap/rethrow with more context
			throw new HotelServiceException("An error occurred when looking up room", e); 
		}

		//if we don't find room by id, throw exception, will get turned to 404
		if(room == null) {
			throw new HotelNotFoundException("Couldn't find room with room number", roomNumber);
		}

		//if we got here, we found a room with that id, so return it
		return room;
	}

	@Override
	public Room updateRoom(int roomNumber, Room room) throws HotelServiceException {
		//validate the request -- if invalid request, then let exception be handled up call stack 
		validator.validateUpdateRoom(roomNumber, room);

		//keep track of number of rooms updated
		boolean updatedRoom = false;

		//try to update the room
		try {
			updatedRoom = dao.updateRoom(roomNumber, room);
		} catch (HotelSystemException e) {
			String errorMessage = String.format("An error occurred when updating room %d", roomNumber);
			throw new HotelServiceException(errorMessage, e);
		}

		//if we didn't update one room, then it means that the room did not exist, so throw a 404
		if(!updatedRoom) {
			throw new HotelNotFoundException("No room was found with room number", roomNumber);
		}

		//get the new room
		try {
			return dao.getRoomById(roomNumber);
		} catch (HotelSystemException e) {
			//catch anything unusual happening and log for more info
			throw new HotelServiceException("There was an issue searching for room on id", e); //TODO
		}
	}

	@Override
	public List<Room> getAvailableRooms(LocalDate checkin, LocalDate checkout) throws HotelServiceException {
		//validate the request -- if invalid request, then let exception be handled up call stack 
		validator.validateGetAvailableRooms(checkin, checkout);

		//make the call and return it
		try {
			return dao.getAvailableRooms(checkin, checkout);
		} catch (HotelSystemException e) {
			//add more context and rethrow
			String errorMessage = String.format("An error occurred when looking up rooms with checkin date %s and checkout date %s", checkin, checkout);
			throw new HotelServiceException(errorMessage, e); 
		}
	}

	@Override
	public Reservation placeReservation(Reservation reservation) throws HotelServiceException {
		//validate the request -- if invalid request, then let exception be handled up call stack 
		validator.validatePlaceReservation(reservation);

		try {
			return dao.placeReservation(reservation);
		} catch (HotelSystemException e) {
			//wrap with more context
			String errorMessage = String.format("An exception occurred when trying to place reservation %s", reservation.toString());
			throw new HotelServiceException(errorMessage, e);
		}		
	}

	@Override
	public Reservation updateReservation(long reservationId, Reservation reservation)
			throws HotelServiceException {
		//validate the request -- if invalid request, then let exception be handled up call stack 
		validator.validateUpdateReservation(reservationId, reservation);

		//keep track of number of whether or not we updated reservation
		boolean udpatedReservation = false;

		try {
			udpatedReservation = dao.updateReservation(reservationId, reservation);
		} catch (HotelSystemException e) {
			String errorMessage = String.format("An error occurred when updating reservation with id %d and reservation %s", reservationId, reservation);
			throw new HotelServiceException(errorMessage, e);
		}

		//if we didn't update a reservation, that means none with the id exists, so throw 404
		if(!udpatedReservation) {
			throw new HotelNotFoundException("No reservation found with id", reservationId);
		}

		//get the id -- will be original id unless they changed it
		long idToSearchOn = (reservation.getReservationId() == null)? reservationId: reservation.getReservationId();

		//try to get with id
		try {
			return dao.getReservationById(idToSearchOn);
		} catch (HotelSystemException e) {
			//if we get an unexpected exception, and info, wrap, and rethrow
			throw new HotelServiceException("Couldn't find reservation with id", e);
		}
	}

	@Override
	public boolean cancelReservation(long reservationId) throws HotelServiceException {
		//validate the request -- if invalid request, then let exception be handled up call stack 
		validator.validateCancelReservation(reservationId);

		try {
			return dao.cancelReservation(reservationId);
		} catch (HotelSystemException e) {
			String errorMessage = String.format("Error encountered when cancelling reservation with id %d", reservationId);
			throw new HotelServiceException(errorMessage, e);
		}
	}	
}