package com.connor.hotel.service;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.connor.hotel.exception.HotelInvalidRequestException;
import com.connor.hotel.exception.HotelNotFoundException;
import com.connor.hotel.exception.HotelServiceException;
import com.connor.hotel.exception.HotelSystemException;
import com.connor.hotel.model.Reservation;
import com.connor.hotel.model.Room;

public interface HotelService extends Serializable{

	/**
	 * 
	 * @param roomNumber
	 * @return
	 * @throws HotelServiceException
	 */
	Room getRoomById(int roomNumber) throws HotelServiceException;

	/**
	 * 
	 * @param roomNumber
	 * @param room
	 * @return
	 * @throws HotelServiceException
	 */
	Room updateRoom(int roomNumber, Room room)  throws HotelServiceException;

	/**
	 * 
	 * @param checkin
	 * @param checkout
	 * @return
	 * @throws HotelServiceException
	 */
	List<Room> getAvailableRooms(LocalDate checkin, LocalDate checkout) throws HotelServiceException;

	/**
	 * 
	 * @param reservation
	 * @return
	 * @throws HotelServiceException
	 */
	Reservation placeReservation(Reservation reservation) throws HotelServiceException;

	/**
	 * 
	 * @param reservationId
	 * @param reservation
	 * @return
	 * @throws HotelServiceException
	 */
	Reservation updateReservation(long reservationId, Reservation reservation) throws HotelServiceException;

	/**
	 * 
	 * NOTE: in this case, delete is often idempotent when called from a webservice (meaning you should be able to call delete
	 * multiple times with the same result).  In this case, I return a boolean from the service to let know if 
	 * we deleted one, and use that value in a header in webservice response (as deleting an resource and trying to delete a resource
	 * that doesn't exist are both required to return a 200 per idempotency rest standards)
	 * @param reservationId
	 * @return
	 * @throws HotelServiceException
	 */
	boolean cancelReservation(long reservationId) throws HotelServiceException;
}
