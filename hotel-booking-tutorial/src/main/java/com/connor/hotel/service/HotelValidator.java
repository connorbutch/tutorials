package com.connor.hotel.service;

import java.time.LocalDate;

import com.connor.hotel.exception.HotelInvalidRequestException;
import com.connor.hotel.model.Reservation;
import com.connor.hotel.model.Room;

/**
 * This class takes care of validating all input requests
 * @author Connors
 *
 */
public class HotelValidator {

	public void validateGetRoomById(int roomNumber) throws HotelInvalidRequestException{
		//TODO
	}
	
	public void validateUpdateRoom(int roomNumber, Room room) throws HotelInvalidRequestException{
		//TODO
	}
	
	
	public void validateGetAvailableRooms(LocalDate checkin, LocalDate checkout) throws HotelInvalidRequestException{
		//TODO
	}
	
	public void validatePlaceReservation(Reservation reservation) throws HotelInvalidRequestException{
		//TODO
	}
	
	public void  validateUpdateReservation(long reservationId, Reservation reservation) throws HotelInvalidRequestException{
		//TODO
	}
	
	public void validateCancelReservation(long reservationId) throws HotelInvalidRequestException {
		
	}

}
