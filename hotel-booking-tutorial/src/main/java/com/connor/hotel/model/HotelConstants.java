package com.connor.hotel.model;

/**
 * Holds constants related to hotel
 * @author Connors
 *
 */
public interface HotelConstants {

	/**
	 * The jndi name of the hotel datasource
	 */
	public static final String HOTEL_DB_JNDI_NAME = "";
	
	/**
	 * The path to the properties file that holds the sql to be executed.
	 */
	public static final String HOTEL_SQL_PROPS_FILE_PATH = ""; 
}
