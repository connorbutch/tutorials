package com.connor.hotel.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class Reservation implements Serializable {
	
	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -3274375285506781518L;

	private Long reservationId;

	private List<Room> rooms;
	
	private LocalDate checkin;
	
	private LocalDate checkout;

	public Long getReservationId() {
		return reservationId;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public LocalDate getCheckin() {
		return checkin;
	}

	public LocalDate getCheckout() {
		return checkout;
	}

	public void setReservationId(Long reservationId) {
		this.reservationId = reservationId;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public void setCheckin(LocalDate checkin) {
		this.checkin = checkin;
	}

	public void setCheckout(LocalDate checkout) {
		this.checkout = checkout;
	}
	
	
}
