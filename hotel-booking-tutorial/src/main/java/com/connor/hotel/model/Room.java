package com.connor.hotel.model;

import java.io.Serializable;

public class Room implements Serializable {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 1206755892439595035L;

	/**
	 * 
	 */
	private Integer roomNumber;
	
	/**
	 * 
	 */
	private Integer numberOfBeds;
	
	/**
	 * 
	 */
	private Boolean isHandicapAccessible;

	public void setRoomNumber(Integer roomNumber) {
		this.roomNumber = roomNumber;
	}

	public void setNumberOfBeds(Integer numberOfBeds) {
		this.numberOfBeds = numberOfBeds;
	}

	public void setHandicapAccessible(Boolean isHandicapAccessible) {
		this.isHandicapAccessible = isHandicapAccessible;
	}

	public Integer getRoomNumber() {
		return roomNumber;
	}

	public Integer getNumberOfBeds() {
		return numberOfBeds;
	}

	public Boolean isHandicapAccessible() {
		return isHandicapAccessible;
	}	
}