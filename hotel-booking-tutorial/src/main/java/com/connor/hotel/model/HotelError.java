package com.connor.hotel.model;

public enum HotelError {

	CHECK_IN_IS_IN_PAST("Check in cannot be in the past", 1);
	
	/**
	 * 
	 */
	private final String message;
	
	/**
	 * 
	 */
	private final int code;
	
	/**
	 * 
	 * @param message
	 * @param code
	 */
	private HotelError(String message, int code) {
		this.message = message;
		this.code = code;
	}	
}