package com.connor.hotel.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;
import javax.inject.Singleton;


import com.connor.hotel.annotation.Cache;
import com.connor.hotel.exception.HotelSystemException;
import com.connor.hotel.exception.LoadPropertyException;

/**
 * Used for reading from property files.
 * @author Connors
 *
 */
@Singleton
public class PropertyUtil {

	/**
	 * As a matter of practice, I always try to avoid maintaining implicit locks
	 * and use objects instead (for visibility)
	 */
	private final Object LOCK = new Object();

	/**
	 * First key is property file, second key is property name, inner value is property value
	 */
	private final Map<String, Map<String, String>> cache;


	/**
	 * Cdi-enabled constructor
	 * @param cache
	 */
	@Inject
	public PropertyUtil(@Cache Map<String, Map<String, String>> cache) {
		this.cache = cache;
	}



	public String getProperty(String filePath, String propertyName) throws LoadPropertyException {		
		//if the file hasn't already been loaded into memory, then load it
		if(!cache.containsKey(filePath)) {
			try {
				loadFile(filePath);
			} catch (IOException e) {
				//if we get an exception, wrap and rethrow for more context
				throw new LoadPropertyException("Error loading property", e, filePath, propertyName);
			}
		}

		//if we get here, the file has been successfully read into memory
		String property =  cache.get(filePath).get(propertyName);

		//this means property is not present -- warn
		if(property == null) {
			//TODO
		}
		//this means is blank -- warn
		else if(property.trim().isEmpty()) {
			//TODO
		}

		//either way, return the property 
		return property;
	}



	private void loadFile(String filePath) throws IOException {
		//just in case, here we choose thread safety over performance (but I mean, come on, how many properties files are going to get loaded)
		synchronized (LOCK) {			
			Properties properties = new Properties();
			Map<String, String> innerMap = new ConcurrentHashMap<>(); //TODO move to factory to get 100% test coverage (if ever required)

			//try with resources to avoid memory leaks :)
			try(InputStream inputStream = new FileInputStream(filePath)){ //TODO move to factory to get 100% test coverage (if ever required)
				properties.load(inputStream);
			} 			

			//loop over properties and add to map, which is more synchronized and slightly more performant
			properties
			.forEach(
					(key, value) -> innerMap.put((String)key, (String)value)
					);

			//save to our nested map
			cache.put(filePath, innerMap);
		}
	}
}