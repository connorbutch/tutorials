package com.connor.hotel.util;
/**
 * Only exists so we can get higher unit test coverage
 * @author Connors
 *
 */

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.inject.Produces;

import com.connor.hotel.annotation.Cache;

public class CacheFactory {

	@Produces
	@Cache
	public Map<String, Map<String, String>> getCache(){
		return new ConcurrentHashMap<>();
	}
	
}
