package com.connor.hotel.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.connor.hotel.model.Room;

/**
 * This class is needed for queries that return a list of rooms (as opposed to result 
 * set mapper, which returns only a single result)
 * @author Connors
 *
 */
public class RowMapperRoomImpl implements RowMapper<Room> {

	@Override
	public Room mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
