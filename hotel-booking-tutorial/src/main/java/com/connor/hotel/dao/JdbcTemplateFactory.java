package com.connor.hotel.dao;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.connor.hotel.annotation.Datasource;

/**
 * This class is a factory class for getting jdbc templates and named jdbc templates.
 * This class also allows getting the templates in a cdi-enabled way (in coordination 
 * with the use of the datasource annotation)
 * @author Connors
 *
 */
public class JdbcTemplateFactory {
	
	/**
	 * Extracted here for unit testing
	 */
	private final Context context;
	
	/**
	 * Cdi-enabled constructor
	 * @param context
	 */
	@Inject
	public JdbcTemplateFactory(Context context) {
		this.context = context;
	}

	/**
	 * Cdi enabled method for getting a jdbc template
	 * @param ip
	 * @return
	 */
	@Produces
	@Datasource(name="")
	public JdbcTemplate getJdbcTemplate(InjectionPoint ip) {
		//datasource is qualifier, so we know it is present
		String jndiName = ip.getAnnotated().getAnnotation(Datasource.class).name();
		return getJdbcTemplateByName(jndiName);
	}
	
	/**
	 * Get a jdbc template from its jndi name
	 * @param jndiName
	 * @return
	 */
	public JdbcTemplate getJdbcTemplateByName(String jndiName) {
		//TODO validation
		
		Object fromJndi = null;
		try {
			fromJndi = context.lookup(jndiName);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(fromJndi != null && fromJndi instanceof DataSource) {
			DataSource ds = (DataSource) fromJndi;
			return new JdbcTemplate(ds);
		}
		
		//TODO
		throw new RuntimeException();
	}
	
	/**
	 * Cdi producer method for named jdbc template.  This is the same as a jdbc template, except parameters are mapped
	 * based on name, which I always use for queries involving multiple variables (so you odn't have to keep track of ordering)
	 * @param ip
	 * @return
	 */
	@Produces
	@Datasource(name="")
	public NamedParameterJdbcTemplate getNamedJdbcTemplate(InjectionPoint ip) {
		//get the name from annotation (is qualifier, so must be present)
		String name = ip.getAnnotated().getAnnotation(Datasource.class).name();
		
		//get datasource from this and use it to create named template.
		return new NamedParameterJdbcTemplate(getJdbcTemplateByName(name));
	}
}
