package com.connor.hotel.dao;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.connor.hotel.exception.HotelSystemException;
import com.connor.hotel.model.Reservation;
import com.connor.hotel.model.Room;

public interface HotelDao extends Serializable{

	Room getRoomById(int roomNumber) throws HotelSystemException;

	List<Room> getAvailableRooms(LocalDate checkin, LocalDate checkout)  throws HotelSystemException;

	/**
	 * 
	 * NOTE: this is a boolean as it lets our service layer knowing whether or not we succesfully updated the reservation
	 * (which can be used to throw 404) while keeping it decoupled from service by not exposing underlying implementation 
	 * (such as the number of rows affected)
	 * @param roomNumber
	 * @param room
	 * @return
	 * @throws HotelSystemException
	 */
	boolean updateRoom(int roomNumber, Room room)  throws HotelSystemException;

	Reservation getReservationById(long reservationId)  throws HotelSystemException;

	/**
	 * NOTE: this is a boolean as it lets our service layer knowing whether or not we succesfully updated the reservation
	 * (which can be used to throw 404) while keeping it decoupled from service by not exposing underlying implementation 
	 * (such as the number of rows affected)
	 * @param reservationId
	 * @param reserveration
	 * @return
	 * @throws HotelSystemException
	 */
	boolean updateReservation(long reservationId, Reservation reserveration)  throws HotelSystemException;

	/**
	 * NOTE: this is a boolean as it lets our service layer knowing whether or not we succesfully updated the reservation
	 * (which can be used to throw 404) while keeping it decoupled from service by not exposing underlying implementation 
	 * (such as the number of rows affected)
	 * @param reservationId
	 * @return
	 * @throws HotelSystemException
	 */
	boolean cancelReservation(long reservationId) throws HotelSystemException;

	Reservation placeReservation(Reservation reservation) throws HotelSystemException;
}
