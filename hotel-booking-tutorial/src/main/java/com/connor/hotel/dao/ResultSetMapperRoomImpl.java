package com.connor.hotel.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.connor.hotel.model.Room;

/**
 * Used for getting a room from a result set
 * @author Connors
 *
 */
public class ResultSetMapperRoomImpl implements ResultSetExtractor<Room> {

	@Override
	public Room extractData(ResultSet rs) throws SQLException, DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}
}