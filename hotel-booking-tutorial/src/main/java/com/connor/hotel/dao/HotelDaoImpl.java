package com.connor.hotel.dao;

import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.connor.hotel.annotation.Datasource;
import com.connor.hotel.exception.HotelDbArrayParamsException;
import com.connor.hotel.exception.HotelDbMultipleParamsException;
import com.connor.hotel.exception.HotelDbParamSourceException;
import com.connor.hotel.exception.HotelSystemException;
import com.connor.hotel.model.HotelConstants;
import com.connor.hotel.model.Reservation;
import com.connor.hotel.model.Room;
import com.connor.hotel.util.PropertyUtil;

/**
 * Concrete implementation of the hotel dao interface
 * @author Connors
 *
 */
public class HotelDaoImpl implements HotelDao {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -1765344276721321675L;

	/**
	 * 
	 */
	private static final String GET_ROOM_BY_NUMBER_PROP = "getRoomByNumberSql";

	/**
	 * 
	 */
	private static final String GET_AVAILABLE_ROOMS_PROP = "getAvailableRoomsSql";

	/**
	 * 
	 */
	private static final String UPDATE_ROOM_PROP = "updateRoomSql";

	/**
	 * 
	 */
	private static final String GET_RESERVATION_BY_ID_PROP = "getReservationByIdSql";

	/**
	 * 
	 */
	private static final String UPDATE_RESERVATION_PROP = "updateReservationSql";

	/**
	 * 
	 */
	private static final String CANCEL_RESERVATION_PROP = "cancelReservationSql";

	/**
	 * 
	 */
	private static final String PLACE_RESERVATION_PROP = "placeReservationSql";

	/**
	 * Use this for most queries.
	 */
	private final JdbcTemplate jdbcTemplate;

	/**
	 * Use this if we have more than one parameters, as this associates by name, rather than order
	 */
	private final NamedParameterJdbcTemplate namedJdbcTemplate;

	/**
	 * Use this to read sql strings from properties file (so we can externalize from our source code)
	 */
	private final PropertyUtil propertyUtil;

	/**
	 * Use this with jdbc template to get a LIST OF rooms from a query
	 */
	private final ResultSetExtractor<Room> roomExtractor;

	/**
	 * Use this with jdbc template to get a single room from a query
	 */
	private final RowMapper<Room> roomMapper;

	/**
	 * Use this with jdbc template to get a LIST OF reservations from a query
	 */
	private final ResultSetExtractor<Reservation> reservationExtractor;
	
	/**
	 * Logger instance
	 */
	private final Logger log;

	/**
	 * Cdi-enabled constructor
	 * @param jdbcTemplate
	 * @param namedJdbcTemplate
	 * @param propertyUtil
	 * @param roomExtractor
	 * @param roomMapper
	 */
	@Inject
	public HotelDaoImpl(@Datasource(name=HotelConstants.HOTEL_DB_JNDI_NAME) JdbcTemplate jdbcTemplate, @Datasource(name=HotelConstants.HOTEL_DB_JNDI_NAME) NamedParameterJdbcTemplate namedJdbcTemplate, PropertyUtil propertyUtil, ResultSetExtractor<Room> roomExtractor,  RowMapper<Room> roomMapper, ResultSetExtractor<Reservation> reservationExtractor, Logger log) {
		this.jdbcTemplate = jdbcTemplate;
		this.namedJdbcTemplate = namedJdbcTemplate;
		this.propertyUtil = propertyUtil;
		this.roomExtractor = roomExtractor;
		this.roomMapper = roomMapper;
		this.reservationExtractor = reservationExtractor;
		this.log = log;
	}

	@Override
	public Room getRoomById(int roomNumber) throws HotelSystemException {
		//get the sql to execute from properties file
		String sql = propertyUtil.getProperty(HotelConstants.HOTEL_SQL_PROPS_FILE_PATH, GET_ROOM_BY_NUMBER_PROP);

		//build the arguments
		Object[] args = { roomNumber };

		//execute the query and return the results
		try {
			return jdbcTemplate.query(sql, args, roomExtractor);
		}catch(DataAccessException e) {
			//wrap and rethrow exception with as much info as possible (to help with debugging)
			throw new HotelDbArrayParamsException("There was an error when querying for a room by its id", e, sql, args);
		}
	}

	@Override
	public List<Room> getAvailableRooms(LocalDate checkin, LocalDate checkout) throws HotelSystemException {
		//get the sql from the properties file
		String sql = propertyUtil.getProperty(HotelConstants.HOTEL_SQL_PROPS_FILE_PATH, GET_AVAILABLE_ROOMS_PROP);

		//build the args using parameters passed
		Map<String, ?> parameters = Stream.of(
				new AbstractMap.SimpleEntry<>("checkin", checkin),
				new AbstractMap.SimpleEntry<>("checkout", checkout))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		//execute the query and return the results
		try {
			return namedJdbcTemplate.query(sql, parameters, roomMapper);
		}catch(DataAccessException e) {
			//if we got an exception, wrap it with context
			throw new HotelDbMultipleParamsException("Exception when querying for available rooms", e, sql, parameters);
		}
	}

	@Override
	public boolean updateRoom(int roomNumber, Room room) throws HotelSystemException {
		//get the sql to execute from the file
		String sql = propertyUtil.getProperty(HotelConstants.HOTEL_SQL_PROPS_FILE_PATH, UPDATE_ROOM_PROP);

		//build the args		
		Map<String, ?> parameters = Stream.of(
				new AbstractMap.SimpleEntry<>("numberOfBeds", room.getNumberOfBeds()), //TODO
				new AbstractMap.SimpleEntry<>("handicapAccessible", room.isHandicapAccessible())) //TODO
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));	

		//store the number of rows affected here
		int numRowsAffected = -1;

		//execute the sql
		try {
			numRowsAffected = namedJdbcTemplate.update(sql, parameters);
		}catch(DataAccessException e) {
			//wrap and rethrow
			throw new HotelDbMultipleParamsException("Error when updating room", e, sql, parameters);
		}

		//return the number of rows affected
		return numRowsAffected == 1;
	}

	@Override
	public Reservation getReservationById(long reservationId) throws HotelSystemException {
		//get the sql from properties file
		String sql = propertyUtil.getProperty(HotelConstants.HOTEL_SQL_PROPS_FILE_PATH, GET_RESERVATION_BY_ID_PROP);

		//build the args
		Object[] args = { reservationId };

		//execute query and return results
		try {
			return jdbcTemplate.query(sql, args, reservationExtractor);
		}catch(DataAccessException e) {
			//wrap and rethrow for context
			throw new HotelDbArrayParamsException("Error when querying for reservation by id", e, sql, args);
		}
	}

	@Override
	public boolean updateReservation(long reservationId, Reservation reserveration) throws HotelSystemException {
		//get the sql from properties file
		String sql = propertyUtil.getProperty(HotelConstants.HOTEL_SQL_PROPS_FILE_PATH, UPDATE_RESERVATION_PROP);

		//build the args
		Map<String, ?> parameters = null; //TODO

		//store the number of rows affected for checking later here
		int numRowsAffected = -1;

		//execute the sql
		try {
			numRowsAffected = namedJdbcTemplate.update(sql, parameters);
		}catch(DataAccessException e) {
			//catch and rethrow exception
			throw new HotelDbMultipleParamsException("Error when updating reservation", e, sql, parameters);
		}
		
		//can be used by service to throw 404 if someone updated in mean time
		return numRowsAffected == 1;
	}

	@Override
	public boolean cancelReservation(long reservationId) throws HotelSystemException {
		//get the sql from properties file
		String sql = propertyUtil.getProperty(HotelConstants.HOTEL_SQL_PROPS_FILE_PATH, CANCEL_RESERVATION_PROP);

		//build the args
		Object[] args = { reservationId };

		//store the number of rows affected into this variable for checks if we deleted one
		int numRowsAffected = -1;

		//execute delete query, getting a handle of how many rows affected
		try {
			numRowsAffected = jdbcTemplate.update(sql, args);
		}catch(DataAccessException e) {
			//if we get an exception, wrap and rethrow for more context
			throw new HotelDbArrayParamsException("Error cancelling reservation", e, sql, args);
		}
		
		//NOTE: in future, may want to return void, or int for number of rows deleted . . . 
		return numRowsAffected == 1;
	}

	@Override
	public Reservation placeReservation(Reservation reservation) throws HotelSystemException {
		//get the sql to execute -- NOTE: this is a m:n join table
		String sql = propertyUtil.getProperty(HotelConstants.HOTEL_SQL_PROPS_FILE_PATH, PLACE_RESERVATION_PROP);
		
		//build the args
		SqlParameterSource paramSource = new MapSqlParameterSource()
				.addValue("", "") //TODO
				.addValue("", ""); //TODO

		//as we follow best practice and let database autogenerate keys (as it is most performant and avoids any change of collions on pk), we need to hold the key here
		KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		
		//keep track of number of rows updated
		int numRowsAffected = -1;
		
		//execute the sql
		try {
			numRowsAffected = namedJdbcTemplate.update(sql, paramSource, generatedKeyHolder);
		}catch(DataAccessException e) {
			//wrap and rethrow with more context
			throw new HotelDbParamSourceException("Error when placing reservation" , e, sql, paramSource);
		}
		
		/**
		 * Check number of rows affected, if it is not what we expect, then log to warn level
		 * NOTE: we log here, as opposed to others, because we return that to service to get logged
		 * at that layer
		 */
		if(numRowsAffected != 1) {
			//TODO
		}
		
		//update the reservation with the key from the holder
		reservation.setReservationId((Long) generatedKeyHolder.getKey());
		
		//return the new reservation
		return reservation;
	}
}