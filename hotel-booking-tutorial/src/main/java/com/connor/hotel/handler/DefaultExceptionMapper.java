package com.connor.hotel.handler;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;

/**
 * This class provides default exception handling for all uncaught runtime exceptions.   Please try to avoid
 * letting expected exceptions pass through to this layer.
 * 
 * NOTE: I choose to only catch runtime exceptions here, as there is no need to catch checked exceptions or errors
 * here
 * @author Connors
 *
 */
@Provider
public class DefaultExceptionMapper implements ExceptionMapper<RuntimeException>{

	@Inject
	private ErrorResponseBuilder errorResponseBuilder;
	
	@Inject
	private Logger log;
	
	@Override
	public Response toResponse(RuntimeException e) {
		log.error("Recieved an unhandled error in the default exception mapper", e);		
		
		//call the response builder, which gives us a common point of maintenance
		return errorResponseBuilder.getResponse(e);
	}

}
