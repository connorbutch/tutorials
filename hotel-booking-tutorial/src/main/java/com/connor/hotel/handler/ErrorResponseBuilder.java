package com.connor.hotel.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.connor.hotel.exception.HotelInvalidRequestException;
import com.connor.hotel.exception.HotelNotFoundException;

/**
 * This class is responsible for being the central point of maintenance for exceptions
 * (caught, or caught by exception mapper)
 * @author Connors
 *
 */
public class ErrorResponseBuilder {

	/**
	 * Gets response for a certain exception.  This provides a centralized point of maintenance.
	 * @param e
	 * @return
	 */
	public Response getResponse(Exception e) {
		//the jaxrs response itself
		Response response = null;
		
		//the default body of the response
		Object body = getDefaultResponseBody();

		
		if(e == null) {
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(body).build();
		}else {
			//should get a 400
			if(e instanceof HotelInvalidRequestException) {
				response = Response.status(Status.BAD_REQUEST).entity(body).build(); //TODO udpate body
			}
			//should get a 404
			else if(e instanceof HotelNotFoundException) {
				response = Response.status(Status.NOT_FOUND).entity(body).build(); //TODO udpate body
			}
			//500
			else {
				response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(body).build();
			}
		}
		
		//return the response
		return response;
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	private Object getDefaultResponseBody() {
		return null; //TODO
	}
	
}
