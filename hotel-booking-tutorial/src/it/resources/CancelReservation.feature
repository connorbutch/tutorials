#Author: Connor Butch 
Feature: This feature file tests functionality relating to cancelling a reservation.

  
  Scenario Outline: Cancelling an existing reservation by its id should lead to a cancellation of the reservation.
   	Given a request is made to cancel reservation with id <reservationId>
	When the request is made
	Then the http status code will be 200
	And the was deleted header will be true

    Examples: 
      |  reservationId   |
      |       12         |       

   Scenario Outline: Cancelling a reservation with a valid id that does not exist
	Given a request is made to cancel reservation with id <reservationId>
	When the request is made
	Then the http status code will be 200
	And the was deleted header will be false

    Examples: 
      |  reservationId   |
      |     23222        |       

	

Scenario Outline: Attempting to cancel a reservation with an invalid id should return an error response.
	Given a request is made to cancel reservation with id <reservationId>
	When the request is made
	Then we will get an error response
	And the http status code will be 400

    Examples: 
      |  reservationId   |
      |     -3222        |       
