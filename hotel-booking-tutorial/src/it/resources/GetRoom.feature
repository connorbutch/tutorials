#Author: Connor Butch 
Feature: This tests the ability to get a single reservation.

  #NOTE: please use "true" for if they support handicap, else "false" 
  Scenario Outline: Checking for a room with a valid room number should return information about the room.
    Given A request is made to get information on room <roomNumber>    
    When the request is made to get retrieve room information
    Then the request will recieve a success response
    And the response will have the same room number as above
    And the number of beds will be <numberOfBeds>
    And the room does support individuals with handicaps '<isHandicapFriendly>'

    Examples: 
      | roomNumber | numberOfBeds | isHandicapFriendly |
      |  201       |  2           |       false        |
      |  126       |  1           |       true         |

   Scenario Outline: Checking for a room with invalid information should return an error response.
         Given A request is made to get information on room <roomNumber>    
    	When the request is made to get retrieve room information
	Then the request will recieve an error response
	And the status code will be <statusCode>

	 Examples: 
 	|roomNumber | statusCode |
        | -1        |    400     |
        |  580      |    404     |