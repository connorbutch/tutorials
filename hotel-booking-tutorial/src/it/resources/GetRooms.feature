#Author: Connor Butch 
Feature: This feature file tests all functionalities related to the rooms* enpoint.  Reservations are tested in the other file (Reservations.feature)

  
  Scenario Outline: Checking for available rooms with valid information should return rooms.
    Given A request is made to search for rooms with check in date <checkIn>
    And the request wants to checkout on date <checkout>
    When the request is made to the api
    Then the request will return <numRooms> rooms
    And the room <roomNumber> room will be one of the rooms listed as available

    Examples: 
      | checkin | checkout | numRooms | roomNumber |
      |         |          |          |            |

   Scenario Outline: Checking for available rooms with valid information when there are no rooms available should return an empty list
     Given 