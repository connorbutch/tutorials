package com.connor.hotel.cancelReservation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient43Engine;
import org.jboss.resteasy.client.jaxrs.internal.ResteasyClientBuilderImpl;

import com.connor.hotel.common.CommonTestUtil;
import com.connor.hotel.controller.HotelController;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * This class holds the "given when then" step definitions for the cancel reservation feature file.
 * @author Connors
 *
 */
@SuppressWarnings("deprecation")
public class CancelReservationGWT {

	/**
	 * 
	 */
	private Scenario scenario;

	/**
	 * 
	 */
	private Integer reservationId;

	/**
	 * 
	 */
	private Response response;

	/**
	 * Runs between test cases to cleanup data
	 * @param scenario
	 */
	@Before
	public void init(Scenario scenario) {
		//save off scenario
		this.scenario = scenario;

		//reset variables between runs
		reservationId = null;
		response = null;
	}


	@Given("a request is made to cancel reservation with id {int}")
	public void a_request_is_made_to_cancel_reservation_with_id(Integer int1) {
		reservationId = int1;
	}

	@When("the request is made")
	public void the_request_is_made() {		
		//setup a resteasy client using our common configuration
		ResteasyClient client = CommonTestUtil.getDefaultPooledClient();
		ResteasyWebTarget target = client.target(UriBuilder.fromPath("")); //TODO get path here
		HotelController proxy = target.proxy(HotelController.class);

		//make the actual request with our pre-built client
		response = proxy.cancelReservation(reservationId);
	}

	@Then("the http status code will be {int}")
	public void the_http_status_code_will_be(Integer int1) {
		assertEquals("Status code should match", int1.intValue(), response.getStatus());
		scenario.write("Status code matched");
	}

	@Then("the was deleted header will be true")
	public void the_was_deleted_header_will_be_true() {
		String wasDeletedHeaderValue = response.getHeaderString(""); //TODO move this to a shared constant
		assertTrue("Was deleted header value should match", Boolean.TRUE.toString().equalsIgnoreCase(wasDeletedHeaderValue));
		scenario.write("Found matching was deleted value (both expected and actual were true)");
	}

	@Then("the was deleted header will be false")
	public void the_was_deleted_header_will_be_false() {
		String wasDeletedHeaderValue = response.getHeaderString(""); //TODO move this to a shared constant
		assertTrue("Was deleted header value should match", Boolean.FALSE.toString().equalsIgnoreCase(wasDeletedHeaderValue));
		scenario.write("Found matching was deleted header value (expected and actual were false)");
	}

	@Then("we will get an error response")
	public void we_will_get_an_error_response() {
		assertTrue("Response should be an error", response.getStatus() >= Status.BAD_REQUEST.getStatusCode());
		scenario.write("Recieved expected response, which was an error");
	}

}
