package com.connor.hotel.cancelReservation;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * This class runs the cancel reservation feature file.
 * @author Connors
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(glue="com.connor.hotel.cancelReservation", features="src/it/resources/CancelReservation.feature")
public class RunCancelReservationIT {

	/**
	 * Hide constructor
	 */
	private RunCancelReservationIT() {
		//utility class
	}
	
	/**
	 * Preclass setup
	 */
	@BeforeClass
	public static void init() {
		//fill in as needed
	}
	
	@AfterClass
	public static void teardown() {
		//fill in as needed
	}
	
	
}
