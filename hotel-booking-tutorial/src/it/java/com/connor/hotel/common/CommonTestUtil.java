package com.connor.hotel.common;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient43Engine;
import org.jboss.resteasy.client.jaxrs.internal.ResteasyClientBuilderImpl;

/**
 * This contains utility methods for use across all the tests.
 * NOTE: The ONLY time you should write static classes is in test code
 * @author Connors
 *
 */
public class CommonTestUtil {


	/**
	 * Gets resteasy client with pooling
	 * @return
	 */
	public static ResteasyClient getDefaultPooledClient() {
		//setup a thread pool
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
		cm.setMaxTotal(getMaxTotal()); // Increase max total connection to 200
		cm.setDefaultMaxPerRoute(getDefaultMaxPerRoute()); // Increase default max connection per route to 20
		ApacheHttpClient43Engine engine = new ApacheHttpClient43Engine(httpClient);		 

		//setup a resteasy client using our thread pool
		return new ResteasyClientBuilderImpl().httpEngine(engine).build();
	}

	private static int getMaxTotal() {
		return 200; //TODO get from property file/sys property
	}

	private static int getDefaultMaxPerRoute() {
		return 20;  //TODO get from property file/sys property
	}

}
