package com.connor.hotel.updateReservation;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * This class runs the update reservation feature file.
 * @author Connors
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(glue="com.connor.hotel.updateReservation", features="src/it/resources/UpdateReservation.feature")
public class RunUpdateReservationIT {

	
	/**
	 * Preclass setup
	 */
	@BeforeClass
	public static void init() {
		//fill in as needed
	}
	
	@AfterClass
	public static void teardown() {
		//fill in as needed
	}
}
