package com.connor.hotel.placeReservation;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * This class runs the place reservation feature file.
 * @author Connors
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(glue="com.connor.hotel.placeReservation", features="src/it/resources/PlaceReservation.feature")
public class RunPlaceReservationIT {

	
	/**
	 * Preclass setup
	 */
	@BeforeClass
	public static void init() {
		//fill in as needed
	}
	
	@AfterClass
	public static void teardown() {
		//fill in as needed
	}
}
