package com.connor.hotel.getRooms;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * This class runs the get rooms feature file.
 * @author Connors
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(glue="com.connor.hotel.getRooms", features="src/it/resources/GetRooms.feature")
public class RunGetRoomsIT {

	/**
	 * Preclass setup
	 */
	@BeforeClass
	public static void init() {
		//fill in as needed
	}
	
	@AfterClass
	public static void teardown() {
		//fill in as needed
	}
	
}
