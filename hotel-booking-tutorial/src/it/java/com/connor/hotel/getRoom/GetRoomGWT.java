package com.connor.hotel.getRoom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient43Engine;
import org.jboss.resteasy.client.jaxrs.internal.ResteasyClientBuilderImpl;

import com.connor.hotel.common.CommonTestUtil;
import com.connor.hotel.controller.HotelController;
import com.connor.hotel.model.Room;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * This class holds the "given when then" step definitions for the get room feature file.
 * @author Connors
 *
 */
@SuppressWarnings("deprecation") //cucumber is always (alegedly) "coming out with new versions", so ignore these underlines
public class GetRoomGWT {

	/**
	 * Maintain a reference to the scenario at the class level
	 */
	private Scenario scenario;

	/**
	 * The room number for the request
	 */
	private Integer roomNumber;
	
	/**
	 * The response recieved from api for this data row
	 */
	private Response response;

	/**
	 * Runs before each rown in example table
	 * @param scenario
	 */
	@Before
	public void init(Scenario scenario) {
		this.scenario = scenario;

		//reset variables between runs for a clean slate:)
		roomNumber = null;
		response = null;
	}

	@Given("A request is made to get information on room {int}")
	public void a_request_is_made_to_get_information_on_room(Integer int1) {
		roomNumber = int1;
	}

	@When("the request is made to get retrieve room information")
	public void the_request_is_made_to_get_retrieve_room_information() {		
		//setup a resteasy client using our thread pool
		ResteasyClient client = CommonTestUtil.getDefaultPooledClient();
		ResteasyWebTarget target = client.target(UriBuilder.fromPath("")); //TODO get path here
		HotelController proxy = target.proxy(HotelController.class);

		//make the actual request with our pre-built client
		response = proxy.getRoom(roomNumber);
	}

	@Then("the request will recieve a success response")
	public void the_request_will_recieve_a_success_response() {
		assertTrue("Should get a room for a success response", response.getEntity() instanceof Room);
		scenario.write("Got the expected response body");
	}

	@Then("the response will have the same room number as above")
	public void the_response_will_have_the_same_room_number_as_above() {
		Room room = (Room) response.getEntity();
		assertEquals("Room number in response should match room number in request", roomNumber,  room.getRoomNumber());
		scenario.write("Room number in response matched room number used in request");
	}

	@Then("the number of beds will be {int}")
	public void the_number_of_beds_will_be(Integer int1) {
		Room room = (Room) response.getEntity();
		assertEquals("Number of beds should match the expected (input) number of beds", int1, room.getNumberOfBeds());
		scenario.write("The number of beds matched");
	}

	//NOTE: in future, this would be prime place for @Transform annotation
	@Then("the room does support individuals with handicaps {string}")
	public void the_room_does_support_individuals_with_handicaps(String string) {
	   	//default to false -- in case of exact match, then set to true
		boolean doesSupportPeopleWithHandicaps = false;
		if(Boolean.TRUE.toString().equalsIgnoreCase(string)) {
			doesSupportPeopleWithHandicaps = true;		   
	   }
		
		//check the response contains the expected value
		Room room = (Room) response.getEntity();
		assertEquals("Support of people with handicaps should match", doesSupportPeopleWithHandicaps, room.isHandicapAccessible());
		scenario.write("The handicap status matched");
	}

	@Then("the request will recieve an error response")
	public void the_request_will_recieve_an_error_response() {
		//TODO flesh out instanceof check once format of error is decided
	}

	@Then("the status code will be {int}")
	public void the_status_code_will_be(Integer int1) {
		assertEquals("Actual http status code should match input http status code", response.getStatus(), int1.intValue());
		scenario.write("Http status codes matched");
	}
	
	/**
	 * Used to get the base url
	 * @return
	 */
	private String getBaseUrlFormatStr() {
		//NOTE: update this once deploy to cloud
		return "localhost:8080/rooms/%d";
	}
}