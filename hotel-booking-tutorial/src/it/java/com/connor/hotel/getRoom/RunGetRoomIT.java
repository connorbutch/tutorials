package com.connor.hotel.getRoom;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * This class runs the get room feature file.
 * @author Connors
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(glue="com.connor.hotel.getRoom", features="src/it/resources/GetRoom.feature")
public class RunGetRoomIT {

}
