package com.connor.resource.injection;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.NamingManager;

import org.glassfish.ejb.config.EjbContainer;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import com.connor.jndi.ContextFactoryAndBuilderDemoImpl;

public class Main {

	/**
	 * 
	 * @param args
	 * @throws NamingException 
	 */
	public static void main(String[] args) throws NamingException {
		//NOTE: embedded glassfish already sets it's own jndi implementation
		
		/*
		try {
			NamingManager.setInitialContextFactoryBuilder(new ContextFactoryAndBuilderDemoImpl());
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/

		/**
		 * NOTE: you can use multiple resources in try with resources block, but make sure to separate with semi-colon
		 * also realize that resources will be instantiated/opened from left to right
		 * Also note that any inner resources should be declared separately
		 */
		
		//try with resources so we can avoid finally block 
		try(EJBContainer ejbContainer = EJBContainer.createEJBContainer(); WeldContainer cdiContainer = new Weld().initialize()){
			ResourceInjectionDemo demo = cdiContainer.select(ResourceInjectionDemo.class).get();
			
			Context context = cdiContainer.select(Context.class).get();
			
			System.out.printf("Context class: %s\n", context.getClass().getName());
			
			//NOTE: in real life, you have to do instanceof checks on jndi lookups, then explicitly cast
			Object ds = context.lookup("datasource");
			
			/*
			 * NOTE: the below line will throw a null pointer (as resource injection is done via ejb, cdi container will not do this).
			 * This means that although the modern java standard is using cdi (@Inject) on ejbs (instead of the deprecated @EJB)
			 * the injection of ejbs is done through the cdi container, which delegates the work to the ejb container
			 */
			demo.doStuffWithResources();
		}
	}
}