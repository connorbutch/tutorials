package com.connor.resource.injection;

import javax.annotation.Resource;
import javax.sql.DataSource;

public class ResourceInjectionDemo {

	/**
	 * Note: this will attempt to get this from the jndi using the name "datasource"
	 * i.e. new InitialContext().lookup("datasource");
	 */
	@Resource 
	private DataSource datasource;
	
	/**
	 * This will attempt to get this from the jndi using the name "secondDatasource"
	 * i.e. new InitialContext().lookup("secondDatasource");
	 */
	@Resource(name="secondDatasource")
	private DataSource datasource2;
	
	/**
	 * Just here to show we can use the resources.
	 */
	public void doStuffWithResources() {
		//right now they are both null, figure this out  . . . 
		try {
			boolean areSame = datasource.equals(datasource2);			
			String.format("The equality value is %b", areSame);
		}catch(NullPointerException e) {
			e.printStackTrace();
		}		
	}	
}