package com.connor.netflix.service.validate.impl;

import com.connor.netflix.model.Account;
import com.connor.netflix.model.Subscription;
import com.connor.netflix.service.validate.api.NetflixValidator;

/**
 * This implements a validator
 * @author Connors
 *
 */
public class NetflixValidatorImpl implements NetflixValidator {

	@Override
	public void validateCreateAccount(Account accountToAdd) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateUpdateAccount(long accountId, Account updatedAccount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateDeleteAccount(long accountId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateGetAccount(long accountId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validateGetAccountsBySubscription(Subscription subscription, long offset, long limit) {
		// TODO Auto-generated method stub
		
	}

}
