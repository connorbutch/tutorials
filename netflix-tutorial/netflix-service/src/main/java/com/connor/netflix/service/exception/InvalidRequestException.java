package com.connor.netflix.service.exception;

import java.util.Collections;
import java.util.List;

import javax.ejb.ApplicationException;

/**
 * This exception should be thrown when a user provides an invalid request
 * @author Connors
 *
 */
@ApplicationException(rollback = true, inherited = true) //marked here so returned to client as is, without wrapping in ejb exception
public class InvalidRequestException extends RuntimeException{

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -1678068915559755416L;
	
	/**
	 * The list of all error messages.  In the case the user makes more than one exception, this will contain more than one message.
	 */
	private final List<String> errorMessages;

	/**
	 * Constructor
	 * @param errorMessages
	 */
	public InvalidRequestException(List<String> errorMessages) {
		this.errorMessages = Collections.unmodifiableList(errorMessages);
	}
	
	/**
	 * Getter
	 * @return
	 */
	public List<String> getErrorMessages() {
		return errorMessages;
	}	
}