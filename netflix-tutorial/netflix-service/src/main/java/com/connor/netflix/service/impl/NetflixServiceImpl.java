package com.connor.netflix.service.impl;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;

import com.connor.netflix.dao.api.NetflixDao;
import com.connor.netflix.model.Account;
import com.connor.netflix.model.Subscription;
import com.connor.netflix.service.api.NetflixService;
import com.connor.netflix.service.exception.CreditCardExpiredException;
import com.connor.netflix.service.exception.NetflixAccountNotFoundException;
import com.connor.netflix.service.stereotype.Service;
import com.connor.netflix.service.validate.api.NetflixValidator;

/**
 * Concrete implementation of the netflix service interface
 * @author Connors
 *
 */
@Service
public class NetflixServiceImpl implements NetflixService{

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 1575652800926748961L;
	
	/**
	 * Used with String.format
	 */
	private static final String NOT_FOUND_FORMAT_STR = "Account with id %d not found";
	
	/**
	 * Dao instance
	 */
	private final NetflixDao netflixDao;
	
	/**
	 * Validate requests
	 */
	private final NetflixValidator validator;
	
	/**
	 * Cdi-enabled constructor.
	 * @param netflixDao
	 * @param validator
	 */
	@Inject
	public NetflixServiceImpl(NetflixDao netflixDao, NetflixValidator validator) {
		this.netflixDao = netflixDao;
		this.validator = validator;
	}

	@Override
	public Account createAccount(Account accountToAdd) {
		//validate the request -- throw exceptions if invalid
		validator.validateCreateAccount(accountToAdd);		
		
		//create the account and return it
		return netflixDao.addAccount(accountToAdd);
	}

	@Override
	public Account updateAccount(long accountId, Account updatedAccount) {
		validator.validateUpdateAccount(accountId, updatedAccount);
		
		//get the account by the id, if it not found, this will throw exception that will turn into 404
		Account account = getAccount(accountId);
		
		//check they account has a valid credit card on file, if they don't, throw exception
		validateCreditCardOnFile(account);
		
		//update the account
		return netflixDao.updateAccountById(accountId, updatedAccount);
	}

	@Override
	public Account deleteAccount(long accountId) {
		validator.validateDeleteAccount(accountId);
		return netflixDao.deleteAccountById(accountId);
	}

	@Override
	public Account getAccount(long accountId) {
		validator.validateGetAccount(accountId);
		
		Account account = netflixDao.getAccountById(accountId);		
		
		//if account is null, throw an exception that will lead to 404
		if(account == null) {
			String message = String.format(NOT_FOUND_FORMAT_STR, accountId);
			throw new NetflixAccountNotFoundException(message, accountId);
		}
		
		return account;		
	}

	@Override
	public List<Account> getAccountsBySubscription(Subscription subscription, long offset, long limit) {
		validator.validateGetAccountsBySubscription(subscription, offset, limit);
		return netflixDao.getAccountsBySubscriptionType(subscription, limit, offset);
	}	
	
	/**
	 * Validates a credit card on file for an account. 
	 * @throws CreditCardExpiredException if card is expired
	 * @param accountToValidate
	 */
	private void validateCreditCardOnFile(Account accountToValidate) {
		//NOTE: called from internal, so no need to null check
		if(accountToValidate.getCreditCard() == null || accountToValidate.getCreditCard().getExpirationDate() == null || accountToValidate.getCreditCard().getExpirationDate().isBefore(LocalDate.now())) {
			throw new CreditCardExpiredException("The credit card is expired", accountToValidate.getCreditCard());
		}
	}
}