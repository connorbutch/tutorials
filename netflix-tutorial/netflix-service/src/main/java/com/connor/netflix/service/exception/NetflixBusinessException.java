package com.connor.netflix.service.exception;

import javax.ejb.ApplicationException;

/**
 * This class is the base class for all business exceptions.  This should rollback transactions,
 * and should only be thrown from within an "if" block
 * @author Connors
 *
 */
@ApplicationException(rollback=true, inherited=true) //make explicit that this rolls back transaction and is applied to child exceptions
public abstract class NetflixBusinessException extends RuntimeException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -7416855938793962091L;
	
	/**
	 * Constructor -- force to provide a message and no exception
	 * @param message
	 */
	protected NetflixBusinessException(String message) {
		super(message);
	}
}