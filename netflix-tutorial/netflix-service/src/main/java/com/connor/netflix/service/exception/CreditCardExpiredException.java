package com.connor.netflix.service.exception;

import com.connor.netflix.model.CreditCard;

/**
 * This should be thrown when a credit card used on an account has expired
 * @author Connors
 *
 */
public class CreditCardExpiredException extends NetflixBusinessException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -3742184151602911472L;

	/**
	 * The card that is expired
	 */
	private final CreditCard expiredCard;

	/**
	 * Constructor
	 * @param message
	 * @param expiredCard
	 */
	public CreditCardExpiredException(String message, CreditCard expiredCard) {
		super(message);
		this.expiredCard = expiredCard;
	}

	/**
	 * Getter
	 * @return
	 */
	public CreditCard getExpiredCard() {
		return expiredCard;
	}
}