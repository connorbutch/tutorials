package com.connor.netflix.service.exception;

/**
 * Throw if no account found with id
 * @author Connors
 *
 */
public class NetflixAccountNotFoundException extends NetflixBusinessException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -2653117669038520328L;

	/**
	 * 
	 */
	private final long accountId;
	
	/**
	 * Constructor
	 * @param message
	 * @param accountId
	 */
	public NetflixAccountNotFoundException(String message, long accountId) {
		super(message);
		this.accountId = accountId;
	}

	/**
	 * Getter
	 * @return
	 */
	public long getAccountId() {
		return accountId;
	}	
}