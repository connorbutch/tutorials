package com.connor.netflix.service.validate.api;

import java.util.List;

import com.connor.netflix.model.Account;
import com.connor.netflix.model.Subscription;
import com.connor.netflix.service.exception.InvalidRequestException;

/**
 * This class is responsible for validating requests.  If anything is wrong, it should
 * throw an InvalidRequestException with a list of strings for ALL error messages (i.e.
 * don't stop processing after the first error)
 * @author Connors
 *
 */
public interface NetflixValidator {

	/**
	 * @throws InvalidRequestException
	 * @param accountToAdd
	 */
	void validateCreateAccount(Account accountToAdd);
	
	/**
	 * @throws InvalidRequestException
	 * @param accountId
	 * @param updatedAccount
	 */
	void validateUpdateAccount(long accountId, Account updatedAccount);

	/**
	 * @throws InvalidRequestException
	 * @param accountId
	 */
	void validateDeleteAccount(long accountId);

	/**
	 * @throws InvalidRequestException
	 * @param accountId
	 */
	void validateGetAccount(long accountId);

	/**
	 * @throws InvalidRequestException
	 * @param subscription
	 * @param offset
	 * @param limit
	 */
	void validateGetAccountsBySubscription(Subscription subscription, long offset, long limit);
}
