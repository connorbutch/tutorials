package com.connor.netflix.service.api;

import java.io.Serializable;
import java.util.List;

import com.connor.netflix.model.Account;
import com.connor.netflix.model.Subscription;

/**
 * Contains contract for netflix service
 * @author Connors
 *
 */
public interface NetflixService extends Serializable {

	/**
	 * Creates an account
	 * @param accountToAdd
	 * @return
	 */
	Account createAccount(Account accountToAdd); //TODO add throws
	
	/**
	 * Update an account
	 * @param accountId
	 * @param updatedAccount
	 * @return
	 */
	Account updateAccount(long accountId, Account updatedAccount); //TODO add at throws
	
	/**
	 * 
	 * @param accountId
	 * @return
	 */
	Account deleteAccount(long accountId);
	
	/**
	 * 
	 * @param accountId
	 * @return
	 */
	Account getAccount(long accountId);
	
	/**
	 * 
	 * @param subscription
	 * @param offset
	 * @param limit
	 * @return
	 */
	List<Account> getAccountsBySubscription(Subscription subscription, long offset, long limit);
	
}
