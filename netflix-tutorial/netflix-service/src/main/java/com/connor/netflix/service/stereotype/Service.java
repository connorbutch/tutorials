package com.connor.netflix.service.stereotype;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.inject.Stereotype;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

/**
 * This is a stereotype to be used on all concrete service implementations.
 * @author Connors
 *
 */
@Stereotype
@Local //make explicit
@TransactionManagement(TransactionManagementType.CONTAINER) //make explicit
@Transactional(value=TxType.REQUIRED)
@Stateless
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface Service {
	//intentionally blank -- marker interface
}
