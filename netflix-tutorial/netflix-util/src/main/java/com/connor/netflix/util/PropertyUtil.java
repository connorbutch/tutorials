package com.connor.netflix.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.connor.netflix.util.exception.PropertyFileNotFoundException;

/**
 * Use to read from properties file.
 * @author Connors
 *
 */
public class PropertyUtil {

	/**
	 * Reads property from file specified.  If property doesn't exist, will return null. If no value is provided for property
	 * will return empty string.
	 * @throws PropertyFileNotFoundException if file doesn't exist
	 * @param filePath
	 * @param property
	 * @return
	 */
	public String getProperty(String filePath, String property) {
		Properties props = new Properties();
 
		
		try {
			props.load(new FileInputStream(filePath));			
		 } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (String)props.get(property);
	}
	
}
