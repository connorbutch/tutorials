package com.connor.netflix.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Represents an error response.
 * @author Connors
 *
 */
public class NetflixError implements Serializable {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -7811139126156284919L;

	/**
	 * The error messages
	 */
	private final List<String> errors;

	/**
	 * 
	 * @param errors
	 */
	public NetflixError(List<String> errors) {
		this.errors = Collections.unmodifiableList(errors);
	}

	/**
	 * Returns an unmodifiable view of the error messages
	 * @return
	 */
	public List<String> getErrors() {
		return errors;
	}
	
	
}
