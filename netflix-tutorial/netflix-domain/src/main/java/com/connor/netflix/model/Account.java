package com.connor.netflix.model;

import java.io.Serializable;

/**
 * Represents an account
 * @author Connors
 *
 */
public class Account implements Serializable{

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -9160497215980502103L;

	private long accountId;

	private Subscription subscriptionLevel;

	private CreditCard creditCard;

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public void setSubscriptionLevel(Subscription subscriptionLevel) {
		this.subscriptionLevel = subscriptionLevel;
	}	

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}	

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public long getAccountId() {
		return accountId;
	}

	public Subscription getSubscriptionLevel() {
		return subscriptionLevel;
	}
}
