package com.connor.netflix.model;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * This class represents a credit card on file.
 * @author Connors
 *
 */
public class CreditCard implements Serializable {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -4258478979663552559L;

	private String cardNumber;
	
	private LocalDate expirationDate;
	
	private String csv;

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCsv() {
		return csv;
	}

	public void setCsv(String csv) {
		this.csv = csv;
	}
	
	
}
