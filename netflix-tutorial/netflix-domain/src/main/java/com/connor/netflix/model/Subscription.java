package com.connor.netflix.model;

/**
 * This represents the "level" of subscription
 * @author Connors
 *
 */
public enum Subscription {

	/**
	 * 1000 hours of streaming per month
	 */
	GOLD,
	
	/**
	 * 500 hours of streaming per month
	 */
	SILVER,
	
	/**
	 * 250 hours of streaming per month
	 */
	BRONZE;
	
	
}
