package com.connor.netflix.dao.exception;

/**
 * This should be thrown when we attempt to execute a single sql value.  It should be used to wrap
 * DataAccessException.
 * @author Connors
 *
 */
public class NetflixDatabaseSingleValueException extends NetflixDatabaseException {
	
	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -2320165991480748043L;

	/**
	 * The sql attempted to be ran
	 */
	private final String sqlRan;
	
	/**
	 * The parameter passed
	 */
	private final Object parameter;
	
	/**
	 * Constructor
	 * @param message
	 * @param t
	 * @param sqlRan
	 * @param parameter
	 */
	public NetflixDatabaseSingleValueException(String message, Throwable t, String sqlRan, Object parameter) {
		super(message, t);
		this.sqlRan = sqlRan;
		this.parameter = parameter;	
	}

	/**
	 * Getter
	 * @return
	 */
	public String getSqlRan() {
		return sqlRan;
	}

	/**
	 * Getter
	 * @return
	 */
	public Object getParameter() {
		return parameter;
	}	
}