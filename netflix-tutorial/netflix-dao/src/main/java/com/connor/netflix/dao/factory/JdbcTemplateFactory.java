package com.connor.netflix.dao.factory;

import javax.enterprise.inject.spi.InjectionPoint;

import org.springframework.jdbc.core.JdbcTemplate;

import com.connor.netflix.dao.annotation.Datasource;

/**
 * Factory class (cdi-enabled) for a jdbc template based on the datasource annotation
 * @author Connors
 *
 */
public class JdbcTemplateFactory {

	/**
	 * Producer method for jdbc template
	 * @param ip
	 * @return
	 */
	@Datasource(name="") //non-binding, so we don't care
	public JdbcTemplate getJdbcTemplate(InjectionPoint ip) {
		//TODO
		return null;
	}	
}