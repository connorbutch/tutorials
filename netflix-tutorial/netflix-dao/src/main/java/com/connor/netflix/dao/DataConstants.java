package com.connor.netflix.dao;

/**
 * Holds constants relating to db.
 * @author Connors
 *
 */
public interface DataConstants {

	/**
	 * The jndi name of the datasource
	 */
	public static final String DATASOURCE_NAME = ""; //TODO
	
	/**
	 * 
	 */
	public static final String QUERY_BY_ID_SQL_PROPERTY = ""; //TODO
	
	/**
	 * 
	 */
	public static final String QUERY_FOR_SUBSCRIPTIONS_SQL_PROPERTY = ""; //TODO
	
	/**
	 * 
	 */
	public static final String UPDATE_ACCOUNT_SQL_PROPERTY = ""; //TODO
	
	/**
	 * 
	 */
	public static final String DELETE_ACCOUNT_BY_ID_SQL_PROPERTY = ""; //TODO
	
	
	
	
}
