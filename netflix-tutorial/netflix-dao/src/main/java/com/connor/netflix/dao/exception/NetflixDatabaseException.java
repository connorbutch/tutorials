package com.connor.netflix.dao.exception;

public abstract class NetflixDatabaseException extends RuntimeException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -4951297771339136086L;
	
	/**
	 * Database exception constructor, only to be invoked by subclasses
	 * @param message
	 * @param t
	 */
	protected NetflixDatabaseException(String message, Throwable t) {
		super(message, t);
	}

}
