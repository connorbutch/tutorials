package com.connor.netflix.dao.exception;

/**
 * Throw this if a statement using list of parameters fails
 * @author Connors
 *
 */
public class NetflixDatabaseMultiValueException extends NetflixDatabaseException {

	//TODO add fields
	
	public NetflixDatabaseMultiValueException(String message, Throwable t) {
		super(message, t);
	}

}
