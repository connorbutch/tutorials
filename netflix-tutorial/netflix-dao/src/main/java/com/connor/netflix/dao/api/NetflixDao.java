package com.connor.netflix.dao.api;

import java.io.Serializable;
import java.util.List;

import com.connor.netflix.dao.exception.NetflixDatabaseSingleValueException;
import com.connor.netflix.model.Account;
import com.connor.netflix.model.Subscription;

/**
 * Interface for account dao.
 * @author Connors
 *
 */
public interface NetflixDao extends Serializable{

	/**
	 * Gets an account by its id.  Will return null if not found.
	 * @param accountId
	 * @throws NetflixDatabaseSingleValueException
	 * @return the account with the specified id, or null if not found
	 */
	Account getAccountById(long accountId);
	
	/**
	 * 
	 * @param accountId
	 * @return
	 */
	Account deleteAccountById(long accountId); //TODO
	
	/**
	 * 
	 * @param accountId
	 * @param updatedAccount
	 * @return
	 */
	Account updateAccountById(long accountId, Account updatedAccount); //TODO add at throws
	
	/**
	 * 
	 * @param accountToAdd
	 * @return
	 */
	Account addAccount(Account accountToAdd);
	
	/**
	 * 
	 * @param subscription
	 * @param limit
	 * @param offset
	 * @return
	 */
	List<Account> getAccountsBySubscriptionType(Subscription subscription, long limit, long offset); //TODO at throws
	
}
