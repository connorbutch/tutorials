package com.connor.netflix.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.connor.netflix.dao.DataConstants;
import com.connor.netflix.dao.annotation.Datasource;
import com.connor.netflix.dao.api.NetflixDao;
import com.connor.netflix.dao.exception.NetflixDatabaseSingleValueException;
import com.connor.netflix.dao.mapper.AccountExtractor;
import com.connor.netflix.model.Account;
import com.connor.netflix.model.Subscription;
import com.connor.netflix.util.PropertyUtil;

/**
 * Concrete implementation of netflix dao interface
 * @author Connors
 *
 */
public class NetflixDaoImpl implements NetflixDao {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 1061544525797359781L;

	/**
	 * Jdbc template for db operations
	 */
	private final JdbcTemplate jdbcTemplate;

	/**
	 * Used to map accounts
	 */
	private final ResultSetExtractor<Account> accountExtractor;

	/**
	 * Property util, used to read in sql
	 */
	private final PropertyUtil propertyUtil;

	/**
	 * Cdi-enabled constructor
	 * @param jdbcTemplate
	 * @param accountMapper
	 * @param propertyUtil
	 */
	@Inject
	public NetflixDaoImpl(@Datasource(name = DataConstants.DATASOURCE_NAME) JdbcTemplate jdbcTemplate,  ResultSetExtractor<Account> accountMapper, PropertyUtil propertyUtil) {
		this.jdbcTemplate = jdbcTemplate;
		this.accountExtractor = accountMapper;
		this.propertyUtil = propertyUtil;
	}

	@Override
	public Account getAccountById(long accountId) {
		//get the sql to execute
		String sqlToExecute = propertyUtil.getProperty("", DataConstants.QUERY_BY_ID_SQL_PROPERTY);

		//make the db call
		try {
			return jdbcTemplate.query(sqlToExecute, accountExtractor);
		}catch(DataAccessException e) {
			//if we fail, wrap and rethrow with more information for debugging
			String errorMessage = "We encountered an error when getting an account by its id";
			throw new NetflixDatabaseSingleValueException(errorMessage, e, sqlToExecute, null); //TODO
		}
	}

	@Override
	public Account deleteAccountById(long accountId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account updateAccountById(long accountId, Account updatedAccount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account addAccount(Account accountToAdd) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Account> getAccountsBySubscriptionType(Subscription subscription, long limit, long offset) {
		// TODO Auto-generated method stub
		return null;
	}
}