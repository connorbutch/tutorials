package com.connor.netflix.ws.model;

/**
 * Hold constants for netflix webservice for use across classes.
 * @author Connors
 *
 */
public interface NetflixWSConstants {

	public static final String DEFAULT_ERROR_MESSAGE = "We are sorry, an unknown exception occured";
	
	public static final String NOT_FOUND_ERROR_MESSAGE = "No account was found with the passed id";
	
}
