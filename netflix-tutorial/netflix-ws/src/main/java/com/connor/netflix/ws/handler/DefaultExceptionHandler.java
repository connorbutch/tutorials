package com.connor.netflix.ws.handler;

import java.util.Collections;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.connor.netflix.model.NetflixError;
import com.connor.netflix.ws.model.NetflixWSConstants;

/**
 * This is the default exception handler for uncaught exceptions not handled by other exception handlers
 * NOTE: the jsr specifications ensure that the exception handlers are ordered (i.e. will be caught by most specific
 * first)
 * @author Connors
 *
 */
@Provider
public class DefaultExceptionHandler implements ExceptionMapper<Exception>  {
	
	@Override
	public Response toResponse(Exception exception) {
		exception.printStackTrace();
		
		NetflixError body = new NetflixError(Collections.singletonList(NetflixWSConstants.DEFAULT_ERROR_MESSAGE));
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(body).build();
	}

}
