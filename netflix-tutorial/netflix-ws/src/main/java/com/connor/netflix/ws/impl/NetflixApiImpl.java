package com.connor.netflix.ws.impl;

import javax.ws.rs.ext.Provider;

import com.connor.netflix.ws.api.NetflixApi;

/**
 * Concrete implementation of netflix api (webservice)
 * @author Connors
 *
 */
@Provider
public class NetflixApiImpl implements NetflixApi {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 2609295728770649229L;

}
