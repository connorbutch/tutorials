package com.connor.netflix.ws.handler;

import java.util.Collections;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.connor.netflix.dao.exception.NetflixDatabaseException;
import com.connor.netflix.dao.exception.NetflixDatabaseMultiValueException;
import com.connor.netflix.dao.exception.NetflixDatabaseSingleValueException;
import com.connor.netflix.model.NetflixError;
import com.connor.netflix.ws.model.NetflixWSConstants;

/**
 * Handles db exceptions that get rethrown.  Should use instanceof to log as much info as possible.
 * @author Connors
 *
 */
@Provider
public class NetflixDatabaseExceptionHandler implements ExceptionMapper<NetflixDatabaseException>{

	@Override
	public Response toResponse(NetflixDatabaseException exception) {
		//do instance of checks for further processing (logging), but either way, return 500
		if(exception instanceof NetflixDatabaseSingleValueException) {
			logSingleValueException((NetflixDatabaseSingleValueException) exception);
		}else if(exception instanceof NetflixDatabaseMultiValueException) {
			logMultiValueException((NetflixDatabaseMultiValueException) exception);
		}else {
			logUnknownDatabaseException(exception);
		}
		
		//no matter what type, return 500
		NetflixError body = new NetflixError(Collections.singletonList(NetflixWSConstants.DEFAULT_ERROR_MESSAGE));
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(body).build();
	}
	
	/**
	 * 
	 * @param e
	 */
	private void logSingleValueException(NetflixDatabaseSingleValueException e) {
		//TODO
	}
	
	/**
	 * 
	 * @param e
	 */
	private void logMultiValueException(NetflixDatabaseMultiValueException e) {
		//TODO
	}
	
	/**
	 * 
	 * @param e
	 */
	private void logUnknownDatabaseException(NetflixDatabaseException e) {
		//TODO
	}
}