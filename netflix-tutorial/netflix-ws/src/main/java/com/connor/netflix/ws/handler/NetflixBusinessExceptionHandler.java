package com.connor.netflix.ws.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.connor.netflix.service.exception.NetflixBusinessException;

/**
 * Handles business exceptions.  Can use instanceof checks here to log specific info
 * @author Connors
 *
 */
@Provider
public class NetflixBusinessExceptionHandler implements ExceptionMapper<NetflixBusinessException>{

	@Override
	public Response toResponse(NetflixBusinessException exception) {
		// TODO Auto-generated method stub
		return null;
	}



}