package com.connor.netflix.ws.api;

import java.io.Serializable;

/**
 * Interface for webservice.  Note that implementing class must be annotated with provider annotatation
 * to be registered by jaxrs implementation at runtime.
 * @author Connors
 *
 */
public interface NetflixApi extends Serializable {

}
