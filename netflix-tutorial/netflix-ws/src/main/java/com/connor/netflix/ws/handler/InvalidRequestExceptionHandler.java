package com.connor.netflix.ws.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.connor.netflix.model.NetflixError;
import com.connor.netflix.service.exception.InvalidRequestException;

/**
 * Exception handler for invalid request exception
 * @author Connors
 *
 */
@Provider
public class InvalidRequestExceptionHandler implements ExceptionMapper<InvalidRequestException>{

	@Override
	public Response toResponse(InvalidRequestException exception) {
		System.out.println("We recieved an invalid response.  The list of messages was "); //TODO log
	
		NetflixError body = new NetflixError(exception.getErrorMessages());
		return Response.status(Status.BAD_REQUEST).entity(body).build();
	}

}
