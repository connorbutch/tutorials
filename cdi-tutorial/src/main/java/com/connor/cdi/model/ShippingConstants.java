package com.connor.cdi.model;

/**
 * Holds constants related to shipping.
 * Using java 8 features where interfaces can declare public static final variables.
 * @author Connors
 *
 */
public interface ShippingConstants {

	/**
	 * The priority for insurance observer
	 */
	public static final int INSURANCE_PRIORITY = 1;
	
	/**
	 * Priority for email observer.
	 */
	public static final int NOTIFICATION_PRIORITY = 2;
	
	/**
	 * System property for locale.
	 */
	public static final String LOCALE_SYSTEM_PROPERTY = "local";
	
	/**
	 * Currency code for usd.
	 */
	public static final String US_DOLLAR_CURRENCY_CODE = "usd";
	
}
