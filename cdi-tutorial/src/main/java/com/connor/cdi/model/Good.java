package com.connor.cdi.model;

import com.connor.cdi.util.ShippingUtil;

/**
 * This is a domain object representing a good (like an item)
 * @author Connors
 *
 */
public class Good {

	/**
	 * Price in us dollars
	 */
	private final double priceInUsDollars;
	
	/**
	 * damage done -- should never exceed price
	 */
	private double damageDoneInUsDollars;
	
	/**
	 * The weight, in pounds
	 */
	private final double weightInPounds;

	/**
	 * Constructor.
	 * @param priceInUsDollars
	 * @param weightInPounds
	 */
	public Good(double priceInUsDollars, double weightInPounds) {
		this.priceInUsDollars = priceInUsDollars;
		this.weightInPounds = weightInPounds;
		
		//always initialize with no damage
		this.damageDoneInUsDollars = 0;
	}
	
	/**
	 * Damages a good by the given percent (should be between 0 and 1 inclusive)
	 * @param damagePercent
	 */
	public void damageGood(double damagePercent) {
		//check is in range
		if(!new ShippingUtil().isValidPercent(damagePercent)) {
			throw new IllegalArgumentException("Enter valid damage percent");
		}
		
		//damage the good
		damageDoneInUsDollars = priceInUsDollars * damagePercent;
	}

	/**
	 * Getter
	 * @return
	 */
	public double getWeightInPounds() {
		return weightInPounds;
	}

	/**
	 * 
	 * @return
	 */
	public double getPriceInUsDollars() {
		return priceInUsDollars;
	}

	/**
	 * 
	 * @return
	 */
	public double getDamageDoneInUsDollars() {
		return damageDoneInUsDollars;
	}
}