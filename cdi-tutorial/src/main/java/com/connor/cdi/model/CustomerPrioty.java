package com.connor.cdi.model;

/**
 * Indicates customer preference on shipping.
 * @author Connors
 *
 */
public enum CustomerPrioty {

	/**
	 * Use if priority is goods getting their safely.
	 */
	SAFETY,
	
	/**
	 * Use if priority is getting the best deal.
	 */
	PRICE,
	
	/**
	 * Use if priority is having goods arrive the fastest.
	 */
	SPEED;
	
}
