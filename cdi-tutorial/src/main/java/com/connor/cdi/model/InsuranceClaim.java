package com.connor.cdi.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * Model class representing a simple insurance claim on a list of damaged goods.
 * @author Connors
 *
 */
public class InsuranceClaim implements Serializable{

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 6383759056826578394L;

	/**
	 * Unique id for claim
	 */
	private final String claimId;
	
	/**
	 * Constructor.
	 */
	public InsuranceClaim() {
		//set the claim id to a random uuid -- no chance of collision
		claimId = UUID.randomUUID().toString();
	}

	/**
	 * Getter.
	 * @return
	 */
	public String getClaimId() {
		return claimId;
	}
}
