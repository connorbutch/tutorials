package com.connor.cdi.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.connor.cdi.annotation.DataAnalytics;

/**
 * This is an interceptor class involving data analytics.
 * NOTE: interceptors are something I've never really seen much use for, if you have a good example
 * of them being used in industry, please let me know.
 * @author Connors
 *
 */
@Interceptor
@DataAnalytics
public class DataAnalyticsInterceptor {

	/**
	 * Hanldes the invoking of method that is marked with data analytics annotation.
	 * From here we can pull the parameters and name of method
	 * @param context
	 * @throws Exception 
	 */
	@AroundInvoke
	public Object runAnalytics(InvocationContext context) throws Exception {
		//our dummy implementation prints out the method name and the args passed
		String methodName = context.getMethod().getName();
		Object[] parameters =  context.getParameters();
		String parameterString = getParameterString(parameters);		
		
		//build the output and log to console
		String output = String.format("Running data analytics on method %s for args %s", methodName, parameterString);
		System.out.println(output);
		
		//call the originally invoked method -- letting exceptions continue up the stack trace
		return context.proceed();
	}
	
	/**
	 * Builc string representation of parameters
	 * @param parameters
	 * @return
	 */
	private String getParameterString(Object[] parameters) {		
		//NOTE: this could be a ternary if, but I prefer writing it this way sometimes for readability. . . .
		String parameterString;		
		if(parameters == null) {
			parameterString = "null";
		}else {
			parameterString = parameters.toString();
		}
		
		return parameterString;
	}
}
