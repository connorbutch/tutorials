package com.connor.cdi.producer;

import java.util.Locale;

import javax.enterprise.inject.Produces;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

import com.connor.cdi.model.ShippingConstants;

/**
 * This contains cdi producer methods for a java locale object.  Note that usually this just pulls from the default of the jvm,
 * but because we want flexibility, this pulls from system property.
 * @author Connors
 *
 */
public class LocaleProducer {

	/**
	 * Get a locale.  Will try to use system property, but will default to the local for the jvm instance.
	 * @return
	 */
	@Produces
	public Locale getLocaleFromSystemProperty() {
		//default to the one inherent to the jvm
		Locale toReturn = Locale.getDefault();

		//if system property is set and valid, set it to this . . . 
		String localeSystemProperty = System.getProperty(ShippingConstants.LOCALE_SYSTEM_PROPERTY);
		if(StringUtils.isNotBlank(localeSystemProperty)) {
			try {
				Locale fromSysProperty = LocaleUtils.toLocale(localeSystemProperty);
				if(fromSysProperty != null) {
					toReturn = fromSysProperty;
				}
			}catch(IllegalArgumentException e) {
				//silence -- go with default
			}
		}
		
		//return it
		return toReturn;
	}
}