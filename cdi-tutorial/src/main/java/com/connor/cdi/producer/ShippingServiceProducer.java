package com.connor.cdi.producer;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import com.connor.cdi.annotation.TopPriority;
import com.connor.cdi.factory.ShippingServiceFactory;
import com.connor.cdi.service.api.ShippingService;

/**
 * This is a class containing cdi producer methods.
 * @author Connors
 *
 */
public class ShippingServiceProducer {

	/**
	 * This is a producer method which uses a single qualifier.  This method must be public.
	 * NOTE: all arguments to a producer method are automatically injected via cdi.
	 * @param ip
	 * @param factory
	 * @return
	 */
	@Produces //this makes this a producer method
	@TopPriority //because this is non-binding, this method will get called regardless of the value in the annotation.  This let's us have a centralized palce to keep track.
	public ShippingService getShippingService(InjectionPoint ip, ShippingServiceFactory factory) {
		//what we return 
		ShippingService service = null;
		
		TopPriority tp = ip.getAnnotated().getAnnotation(TopPriority.class);
		//NOTE: no need to null check because top priority is a qualifier, so we know it exists
		switch(tp.priority()) {
		case PRICE:
			service = factory.getCheapestShippingOption(null); //TODO
			break;			
		case SAFETY:
			service = factory.getSafestShippingService();
			break;
		case SPEED:
			//TODO
			break;
		default:
			//log this . . . 
			break;
		}
		
		//return it
		return service;
	}	
	
	/**
	 * This is a disposer method for a shipping service.  A disposer method is always paired 1:1 with a producer method
	 * and allows for resource cleanup (similar to predestroy method).  Note that this MUST have same qualifiers/annotations
	 * as the associated producer method.
	 * @param service
	 */
	public void cleanupFactory(@Disposes @TopPriority ShippingService service) {
		//normally, you would perform cleanup operations here, such as closing resources
	}
}