package com.connor.cdi.util;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Utility class for the project.
 * @author Connors
 *
 */
public class ShippingUtil {

	/**
	 * Validates the percentage. I stay away from static methods for ease of testing.
	 * @param toValidate
	 * @return
	 */
	public boolean isValidPercent(double toValidate) {
		return toValidate >= 0 && toValidate <= 1;
	}	
	
	/**
	 * 
	 * @param locale
	 * @param currencyAmount
	 * @return
	 */
	public String getLocalCurrencyOutput(Locale locale, double currencyAmount) {
		//avoid null ptrs . . . 
		if(locale == null) {
			locale = Locale.getDefault();
		}
		
		//get the formatter for the locale
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
		
		//use the formatter to format the string and return
		return currencyFormatter.format(currencyAmount);		
	}
}