package com.connor.cdi.annotation;

import javax.enterprise.inject.Stereotype;

/**
 * This class shows how to "extend" an annotation (since annotations implicitly extend annotation
 * they cannot extend another class).  In this case, we use the stereotype annotation so that this
 * "inherits" all the qualities/annoations associated with the TopPriority annotation
 * @author Connors
 *
 */
@TopPriority
@Stereotype
public @interface TopPriorityExtension {

}
