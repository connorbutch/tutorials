package com.connor.cdi.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;

import com.connor.cdi.model.CustomerPrioty;

/**
 * This is an annotation used in cdi.
 * @author Connors
 *
 */
@Documented
@Qualifier //this means this is a qualifier
@Retention(RUNTIME)
@Target({ TYPE, FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
public @interface TopPriority {

	/**
	 * Set the customer's highest priority.  By declaring this as nonbinding, we can use a single
	 * producer method.  Defaults to price.
	 * @return
	 */
	@Nonbinding
	public CustomerPrioty priority() default CustomerPrioty.PRICE;	
}
