package com.connor.cdi.factory;

import java.io.Serializable;
import java.util.List;

import com.connor.cdi.model.Good;
import com.connor.cdi.service.api.ShippingService;

/**
 * Offers a programatic way of getting an instance of a shipping service
 * based on different attributes.  Follows the factory design pattern.
 * @author Connors
 *
 */
public interface ShippingServiceFactory extends Serializable {

	/**
	 * Used to get the cheapest option for shipping a list of goods.
	 * @param goodsToShip
	 * @return
	 */
	ShippingService getCheapestShippingOption(List<Good> goodsToShip);
	
	/**
	 * Return the safest shipping service.
	 * @return
	 */
	ShippingService getSafestShippingService();
}
