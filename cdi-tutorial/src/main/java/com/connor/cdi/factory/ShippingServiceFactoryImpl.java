package com.connor.cdi.factory;

import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.connor.cdi.model.Good;
import com.connor.cdi.service.api.ShippingService;

/**
 * An implementation of the shipping service.
 * @author Connors
 *
 */
public class ShippingServiceFactoryImpl implements ShippingServiceFactory{

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 2722586623562904772L;

	/**
	 * Think of this as a list of all concrete implementations of shipping service
	 * matching the qualifiers (i.e. every one without @Alternative)
	 */
	private final Instance<ShippingService> shippingServiceOptions;

	/**
	 * Cdi-enabled constructor.
	 * @param shippingServiceOptions
	 */
	@Inject
	public ShippingServiceFactoryImpl(Instance<ShippingService> shippingServiceOptions) {
		this.shippingServiceOptions = shippingServiceOptions;
	}

	//TODO might have to come back to this one
	@Override
	public ShippingService getCheapestShippingOption(List<Good> goodsToShip) {
		//what we return 
		ShippingService service = null;

		if(shippingServiceOptions != null) {
			for(ShippingService serviceFromLoop: shippingServiceOptions) {
				if(service == null) {
					service = serviceFromLoop;
					continue; //jump to next item in loop
				}


				//see if the current is cheaper than the previous cheapest
				//if(serviceFromLoop.get)
			}
		}

		//return it
		return service;
	}

	@Override
	public ShippingService getSafestShippingService() {
		//what we return 
		ShippingService service = null;

		if(shippingServiceOptions != null) {
			for(ShippingService serviceFromLoop: shippingServiceOptions) {
				//handle case of first item in collection
				if(service == null) {
					service = serviceFromLoop;
					continue; //jump to next item in loop
				}
				
				//if safer than previous, store it as the new safest
				if(serviceFromLoop.getLikelihoodOfDamagingAGood() < service.getLikelihoodOfDamagingAGood()) {
					service = serviceFromLoop;
				}
			}
		}
		
		//return it
		return service;
	}
}