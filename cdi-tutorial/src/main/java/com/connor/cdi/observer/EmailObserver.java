package com.connor.cdi.observer;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Priority;
import javax.enterprise.event.ObservesAsync;
import javax.enterprise.event.Reception;
import javax.inject.Inject;

import com.connor.cdi.cache.InsuranceClaimCache;
import com.connor.cdi.exception.ShippingRuntimeException;
import com.connor.cdi.model.Good;
import com.connor.cdi.model.InsuranceClaim;

/**
 * Observer class for damaged goods.  This gets called after an insurance claim is filed
 * @author Connors
 *
 */
public class EmailObserver implements Serializable {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 4984868260680732971L;

	/**
	 * Use to get claim numbers
	 */
	private final InsuranceClaimCache claimsCache;
	
	/**
	 * Cdi-enabled constructor.
	 * @param claimsCache
	 */
	@Inject
	public EmailObserver(InsuranceClaimCache claimsCache) {
		this.claimsCache = claimsCache;
	}

	/**
	 * Sends a customer an email about their damaged goods, including their claim number.
	 * @param damagedGoods
	 */
	public void sendCustomerEmail(@ObservesAsync(notifyObserver=Reception.ALWAYS) @Priority(2) List<Good> damagedGoods) {
		//get the claim id from the cache
		InsuranceClaim claim = claimsCache.getClaimForGoods(damagedGoods);
		
		//should not happen, but just in case . . .
		if(claim == null) {
			throw new ShippingRuntimeException("Could not find claim");
		}
		
		//get the customer email (mocked for now as it is not important)
		String email = getCustomerEmail(damagedGoods);
		
		//send the email with the claim number
		sendEmailForOrder(email, claim.getClaimId());
	}
	
	/**
	 * Get email for order.
	 * @param goods
	 * @return
	 */
	protected String getCustomerEmail(List<Good> goods) {
		return "a@a.com"; //mocked for now
	}
	
	/**
	 * Mocks sending a customer an email.
	 * @param customerEmail
	 * @param claimId
	 */
	protected void sendEmailForOrder(String customerEmail, String claimId) {
		//mock sending a customer an email
		String output = String.format("Sending email to %s for claim id %s", customerEmail, claimId);
		
		//just display for now . . . 
		System.out.println(output);
	}
	
}
