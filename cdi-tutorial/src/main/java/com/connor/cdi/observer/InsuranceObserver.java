package com.connor.cdi.observer;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Priority;
import javax.enterprise.event.ObservesAsync;
import javax.enterprise.event.Reception;
import javax.inject.Inject;

import com.connor.cdi.cache.InsuranceClaimCache;
import com.connor.cdi.model.Good;
import com.connor.cdi.model.InsuranceClaim;

/**
 * This class follows the (java ee) observer pattern.  It observes events involving a list of goods.
 * This should execute before the email observer.
 * @author Connors
 *
 */
public class InsuranceObserver implements Serializable{	
	
	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 4543337539139780721L;
	
	/**
	 * Cache of all claims.
	 */
	private final InsuranceClaimCache claimsCache;
	
	
	/**
	 * Cdi-enabled constructor.  Note that we used the singleton annotation on the cache, so we will always get the same instance.
	 * @param claimsCache
	 */
	@Inject
	public InsuranceObserver(InsuranceClaimCache claimsCache) {
		this.claimsCache = claimsCache;
	}

	/**
	 * This is an observer method that files a claim for a damaged list of goods.
	 * This is the first observer called (note the Priority(1)) annotation.
	 * NOTE: always is the default, but wanted to make it explicit.
	 * @param damagedGoods
	 */
	public void fileClaim(@ObservesAsync(notifyObserver=Reception.ALWAYS) @Priority(1) List<Good> damagedGoods) {
		//create a claim for a list of goods
		InsuranceClaim claim = new InsuranceClaim();
		
		//add to cache
		claimsCache.addToCache(damagedGoods, claim);
	}
}
