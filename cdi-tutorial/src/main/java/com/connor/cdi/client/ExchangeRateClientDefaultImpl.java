package com.connor.cdi.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.connor.cdi.model.ExchangeRateResponse;

/**
 * This is an implementation of exchange rate client that gets information via request to restful api.
 * For more information, please see documentation here: 
 * @author Connors
 *
 */
public class ExchangeRateClientDefaultImpl implements ExchangeRateClient {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 4285613944215378664L;
	
	/**
	 * THe number of retries before exception is thrown.
	 */
	private static final int NUMBER_OF_RETRIES = 5;
	
	/**
	 * Format string for get requets.
	 */
	private static final String GET_URL_FORMAT_STR = ""; //TODO

	@Override
	public double getExchangeRate(String toCurrency, String fromCurrency) {
		//what we return 
		double exchangeRate = 0.0;
		
		//setup to make call to webservice
		Client client = ClientBuilder.newClient();
		
		//build the url for the webservice
		String urlForRequest = String.format(GET_URL_FORMAT_STR, fromCurrency, toCurrency);
		
		//point to the url
		WebTarget webTarget 
		  = client.target(urlForRequest);
		
		//set the expected response type
		Invocation.Builder invocationBuilder 
		  = webTarget.request(MediaType.APPLICATION_JSON);
		
		//make the actual response -- try multiple times before throwing exception
		int numTries = 1;
		while(numTries <= NUMBER_OF_RETRIES) {
			try {
				ExchangeRateResponse response 
				  = invocationBuilder.get(ExchangeRateResponse.class);
				exchangeRate = response.getExchangeRate();
				break; //found it, don't make more requests
			}catch(Exception e) { //TODO break down into separate catch blocks
				//ignore any individual failure
			}			
		}
		
		//if exchange rate is still 0, this means we never made a successful request, so log . . . 
		if(exchangeRate == 0) {
			//TODO
		}
		
		//return what we found
		return exchangeRate;
	}
}