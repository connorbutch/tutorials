package com.connor.cdi.client;

import java.io.Serializable;

/**
 * This interface provides a way of getting exchange rates for currencies.
 * @author Connors
 *
 */
public interface ExchangeRateClient extends Serializable {

	/**
	 * Get the exchange rate between a currency pair.
	 * @param toCurrency
	 * @param fromCurrency
	 * @return the exchange rate between the currencies.
	 */
	double getExchangeRate(String toCurrency, String fromCurrency);	
}