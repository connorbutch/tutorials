package com.connor.cdi.decorator;

import java.util.Currency;
import java.util.List;
import java.util.Locale;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import com.connor.cdi.client.ExchangeRateClient;
import com.connor.cdi.model.Good;
import com.connor.cdi.model.ShippingConstants;
import com.connor.cdi.service.api.ShippingService;

/**
 * This is a decorator class that converts the currency to local currency before 
 * displaying it.  Note that this only decorates one method
 * @author Connors
 *
 */
@Decorator
public abstract class ShippingServiceDecorator implements ShippingService {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 5435729147641783123L;

	/**
	 * The delegate shipping service implementation.
	 */
	private final ShippingService delegateShippingService;
	
	/**
	 * Used to get currency exchange rates.
	 */
	private final ExchangeRateClient client;
	
	/**
	 * Used to get currency.
	 */
	private final Locale locale;
	
	/**
	 * Cdi-enabled constructor.
	 * @param delegateShippingService
	 * @param client
	 * @param locale
	 */
	@Inject
	public ShippingServiceDecorator(@Delegate ShippingService delegateShippingService, ExchangeRateClient client, Locale locale) {
		this.delegateShippingService = delegateShippingService;
		this.client = client;
		this.locale = locale;
	}

	@Override
	public double getShippingEstimate(List<Good> goodsToShip) {
		//get the estimate in usd
		double estimateInUSD = delegateShippingService.getShippingEstimate(goodsToShip);
	
		//the currency code
		String currency = Currency.getInstance(locale).getCurrencyCode();
		
		//convert the value to local currency
		double exchangeRate = client.getExchangeRate(currency, ShippingConstants.US_DOLLAR_CURRENCY_CODE);
		
		//return the converted value based on the exchange rate
		return exchangeRate * estimateInUSD;		
	}
}