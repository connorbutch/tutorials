package com.connor.cdi.cache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Singleton;

import com.connor.cdi.model.Good;
import com.connor.cdi.model.InsuranceClaim;

/**
 * This is a simple cache for an insurance claim.  Please note that is more for representation
 * it is not a solid cache implementation (for which infinispan should likely be used).
 * This uses the cdi singleton, which unlike ejb singleton, does not prevent multiple
 * threads from being in the same method at the same time.
 * @author Connors
 *
 */
@Singleton
public class InsuranceClaimCache {

	/**
	 * Maps a list of goods to its associated claim.
	 */
	private final Map<List<Good>, InsuranceClaim> claims;
	
	/**
	 * Constructor.
	 */
	public InsuranceClaimCache() {
		claims = new ConcurrentHashMap<>();
	}
	
	/**
	 * Adds to cache, replacing existing entry.
	 * @param goods
	 * @param associatedClaim
	 */
	public void addToCache(List<Good> goods, InsuranceClaim associatedClaim) {
		//leave synchronization up to underlying data structure.
		claims.put(goods, associatedClaim);
	}
	
	/**
	 * Gets a claim for a list of goods.  Will return null if none exists.
	 * @param goods
	 * @return
	 */
	public InsuranceClaim getClaimForGoods(List<Good> goods) {
		return claims.get(goods);
	}
}