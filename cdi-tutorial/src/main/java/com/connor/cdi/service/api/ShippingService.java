package com.connor.cdi.service.api;

import java.io.Serializable;
import java.util.List;

import com.connor.cdi.exception.DamagedGoodsException;
import com.connor.cdi.model.Good;

/**
 * Interface for a shipping service, which represents different
 * ways of shipping goods (such as a truck, train, etc).
 * @author Connors
 *
 */
public interface ShippingService extends Serializable{

	/**
	 * Ships a list of goods.  If any are damaged, it will throw a damaged goods exception
	 * containing the damaged goods.
	 * @param goodsToShip
	 * @throws DamagedGoodsException
	 */
	void ship(List<Good> goodsToShip) throws DamagedGoodsException;
	
	/**
	 * Returns the likelihood of damaging a good.  Note that all goods have the 
	 * same likelihood of being damaged (within a certain service implementation).
	 * @return
	 */
	double getLikelihoodOfDamagingAGood();
	
	/**
	 * Get an estimate of the cost to ship the list of goods.
	 * @param goodsToShip
	 * @return
	 */
	double getShippingEstimate(List<Good> goodsToShip);
}
