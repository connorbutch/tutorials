package com.connor.cdi.service.impl;

import com.connor.cdi.service.api.BaseShippingService;

/**
 * Semi implementation for the shipping service interface.
 * @author Connors
 *
 */
public class ShippingServiceSemiImpl extends BaseShippingService{

	
	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 2325337761694809279L;

	/**
	 * Constructor
	 */
	public ShippingServiceSemiImpl() {
		super(.8, .4);		
	}
}