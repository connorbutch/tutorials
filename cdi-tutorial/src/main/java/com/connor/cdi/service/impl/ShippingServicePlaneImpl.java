package com.connor.cdi.service.impl;

import javax.enterprise.inject.Alternative;

import com.connor.cdi.service.api.BaseShippingService;

/**
 * This is a plane based implementation of a shipping service.
 * @author Connors
 *
 */
//@Alternative //prefer the jet subclass
public class ShippingServicePlaneImpl extends BaseShippingService{

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = -2021336366234399986L;

	/**
	 * Constructor.
	 * @param likelihoodOfDamagingAGood
	 * @param pricePerPound
	 */
	public ShippingServicePlaneImpl(double likelihoodOfDamagingAGood, double pricePerPound) {
		super(likelihoodOfDamagingAGood, pricePerPound);
	}
	
	@Override
	public String toString() {
		return "plane";
	}
}