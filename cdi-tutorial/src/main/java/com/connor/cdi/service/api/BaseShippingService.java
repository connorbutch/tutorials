package com.connor.cdi.service.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.jboss.weld.exceptions.IllegalArgumentException;

import com.connor.cdi.annotation.DataAnalytics;
import com.connor.cdi.exception.DamagedGoodsException;
import com.connor.cdi.model.Good;

/**
 * Since the process is the same for shipping most goods, this is the base class
 * that can be extended (passing only the parameters) for different implementations.
 * All concrete implementations should look to extend this class and not realize 
 * the interface directly.
 * @author Connors
 *
 */
public abstract class BaseShippingService implements ShippingService{

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 5264836968878898586L;

	/**
	 * The likelihood of damaging a single good.  Same for all goods.
	 */
	protected final double likelihoodOfDamagingAGood;

	/**
	 * The price per pound to ship a good.
	 */
	protected final double pricePerPound;


	/**
	 * Constructor.
	 * @param likelihoodOfDamagingAGood
	 * @param pricePerPound
	 */
	protected BaseShippingService(double likelihoodOfDamagingAGood, double pricePerPound) {
		//check odds are between 0 and 1 inclusive
		if(likelihoodOfDamagingAGood < 0 || likelihoodOfDamagingAGood > 1) {
			throw new IllegalArgumentException("Likelihood of damaging a good must be between 0 and 1 inclusive");
		}
		
		//set fields
		this.likelihoodOfDamagingAGood = likelihoodOfDamagingAGood;
		this.pricePerPound = pricePerPound;
	}	
	
	@Override
	public void ship(List<Good> goodsToShip) throws DamagedGoodsException {
		//if we have items in the list, then process
		if(CollectionUtils.isNotEmpty(goodsToShip)) {
			//keep track of damaged goods
			List<Good> damagedGoods = new ArrayList<>();
			
			//loop through goods, and determine if one is damaged
			for(Good good: goodsToShip) {
				if(didDamageGood()) {
					damagedGoods.add(good);
				}
			}
			
			//if any are damaged, throw the exception
			if(CollectionUtils.isNotEmpty(damagedGoods)) {
				throw new DamagedGoodsException(damagedGoods);
			}
		}		
	}

	@Override
	public double getLikelihoodOfDamagingAGood() {
		return likelihoodOfDamagingAGood;
	}

	@DataAnalytics //declares this for interceptor to pick up
	@Override
	public double getShippingEstimate(List<Good> goodsToShip) {
		double estimate = 0;
		
		//only handle not empty lists
		if(CollectionUtils.isNotEmpty(goodsToShip)) {
			//get the sum of the weight of the goods
			//TODO switch this to lambda?
			double totalWeightInPounds = 0;
			for(Good good: goodsToShip) {
				totalWeightInPounds += good.getWeightInPounds();
			}
			
			//multiply the sum by the price per pound to get total and return it
			estimate = totalWeightInPounds * pricePerPound;
		}
		
		//return it
		return estimate;
	}	
	
	/**
	 * Uses the likelihood of damaging a good to determine if a good
	 * was damaged (uses random number generation).
	 * @return
	 */
	protected boolean didDamageGood() {
		//get a random double between 0 and 1
		double randomDouble = Math.random();
		
		//if it is less than or equal to likelihood of doing damage, return true, else return false
		return randomDouble <= likelihoodOfDamagingAGood;
	}
}