package com.connor.cdi.service.impl;

import javax.enterprise.inject.Specializes;

/**
 * This class is a specializing bean that extends the functionality of a plane.
 * @author Connors
 *
 */
//@Specializes can't figure this out for now . . . 
public class ShippingServiceJetImpl extends ShippingServicePlaneImpl{

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 192180256111788147L;

	/**
	 * Constructor.
	 */
	public ShippingServiceJetImpl() {
		super(.9, 3.8);
	}
	
	@Override
	public String toString() {
		return "Jet";
	}

}
