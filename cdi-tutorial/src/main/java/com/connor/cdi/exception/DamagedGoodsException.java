package com.connor.cdi.exception;

import java.util.ArrayList;
import java.util.List;

import com.connor.cdi.model.Good;

/**
 * This exception should be thrown when one or more good is damaged.
 * @author Connors
 *
 */
public class DamagedGoodsException extends Exception{

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 5082315953902507095L;

	/**
	 * The list of damaged goods
	 */
	private final List<Good> damagedGoods;
	
	/**
	 * 
	 * @param damagedGoods
	 */
	public DamagedGoodsException(List<Good> damagedGoods) {
		//not needed, but make explicit
		super();
		
		//I don't ever like having null lists, so set to empty
		if(damagedGoods == null) {
			damagedGoods = new ArrayList<>();
		}
		
		this.damagedGoods = damagedGoods;
	}

	/**
	 * Getter
	 * @return
	 */
	public List<Good> getDamagedGoods() {
		return damagedGoods;
	}
}
