package com.connor.cdi.exception;

/**
 * This is a generic runtime exception that can be used.
 * @author Connors
 *
 */
public class ShippingRuntimeException extends RuntimeException {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = -8382452211442363871L;

	/**
	 * Constructor.
	 * @param message
	 */
	public ShippingRuntimeException(String message) {
		super(message);
	}
	
	/**
	 * Constructor.
	 * @param message
	 * @param e
	 */
	public ShippingRuntimeException(String message, Exception e) {
		super(message, e);
	}
}