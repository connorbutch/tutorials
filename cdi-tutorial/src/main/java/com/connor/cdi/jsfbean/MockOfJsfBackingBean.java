package com.connor.cdi.jsfbean;

import java.util.List;

import javax.enterprise.event.Event;
import javax.inject.Inject;

import com.connor.cdi.annotation.TopPriority;
import com.connor.cdi.exception.DamagedGoodsException;
import com.connor.cdi.model.CustomerPrioty;
import com.connor.cdi.model.Good;
import com.connor.cdi.service.api.ShippingService;

/**
 * This class contains code that should be contained in a jsf backing bean
 * in a typical java web app.  This demonstrates multiple ways of injecting
 * the shipping service and firing events.
 * @author Connors
 *
 */
public class MockOfJsfBackingBean {

	/**
	 * Cheapest service.
	 */
	private final ShippingService cheapestService;
	
	/**
	 * Safest service.
	 */
	private final ShippingService safestService;
	
	/**
	 * Fire this when goods are damaged.
	 */
	private final Event<List<Good>> damageEvent;

	/**
	 * Cdi-enabled constructor.
	 * @param cheapestService
	 * @param safestService
	 * @param damageEvent
	 */
	@Inject
	public MockOfJsfBackingBean(@TopPriority(priority=CustomerPrioty.PRICE)ShippingService cheapestService, @TopPriority(priority=CustomerPrioty.SAFETY)ShippingService safestService, Event<List<Good>> damageEvent) {
		this.cheapestService = cheapestService;
		this.safestService = safestService;
		this.damageEvent = damageEvent;
	}
	
	/**
	 * Ships goods.
	 * @param goodsToShip
	 */
	public void shipGoodsCheap(List<Good> goodsToShip) {
		try {
			cheapestService.ship(goodsToShip);
		} 
		//checked exception, must be caught or declared in method signature
		catch (DamagedGoodsException e) {
			//fire the event to let our observers know
			damageEvent.fireAsync(e.getDamagedGoods());
		}
	}
	
	/**
	 * Ships goods.
	 * @param goodsToShip
	 */
	public void shipGoodsSafe(List<Good> goodsToShip) {
		try {
			safestService.ship(goodsToShip);
		} 
		//checked exception, must be caught or declared in method signature
		catch (DamagedGoodsException e) {
			//fire the event to let our observers know
			damageEvent.fireAsync(e.getDamagedGoods());
		}
	}	
}