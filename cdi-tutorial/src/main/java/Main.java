import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import com.connor.cdi.jsfbean.MockOfJsfBackingBean;

/**
 * This is the driver class for the java se application.
 * @author Connors
 *
 */
public class Main {

	/**
	 * Entry point.
	 * @param args
	 */
	public static void main(String[] args) {
		Weld weld = new Weld();
		
		//try with resources so we can avoid finally block 
		try(WeldContainer cdiContainer = weld.initialize()){
			/*
			 * NOTE: this is the only place we should ever use this way of getting  abean
			 *  This is the java se equivalent of 
			 * @Inject MockOfJsfBackingBean backingBean;
			 * 
			 */
			MockOfJsfBackingBean backingBean = cdiContainer.select(MockOfJsfBackingBean.class).get();
		}catch(RuntimeException e) {
			e.printStackTrace(); //TODO
		}
	}	
}