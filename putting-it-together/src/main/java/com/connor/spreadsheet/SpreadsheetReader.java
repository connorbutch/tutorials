package com.connor.spreadsheet;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.javatuples.Pair;

import com.connor.spreadsheet.conversion.api.AbstractBaseReflectionConverter;
import com.connor.spreadsheet.conversion.api.Converter;
import com.connor.spreadsheet.exception.NoTransformerException;
import com.connor.spreadsheet.exception.SpreadsheetException;
import com.connor.spreadsheet.factory.InputStreamReaderFactory;


/**
 * Reads in from a spreadsheet in csv format.
 * @author Connors
 *
 */
public class SpreadsheetReader {

	/**
	 * 
	 */
	private final InputStreamReaderFactory factory;
	
	
	/**
	 * Cdi-enabled constructor.
	 * @param factory
	 */
	@Inject
	public SpreadsheetReader(InputStreamReaderFactory factory) {
		this.factory = factory;
	}

	/**
	 * Reads in a csv from given path.
	 * Starting from inside out -- the pair is the header value and cell value for that header for a given row
	 * The inner list contains a list of header cells paired with values for same (inner list represents a row in spreadsheet)
	 * and outer list represents an entire spreadsheet (a list of rows)
	 * @param filePath
	 * @throws SpreadsheetException
	 * @return
	 */
	public List<List<Pair<String, String>>> read(String filePath){
		//represents an entire spreadsheet
		List<List<Pair<String, String>>> spreadsheet = new ArrayList<>();
		
		//represents a single row
		List<Pair<String, String>> row = new ArrayList<>();
		
		//represents a particular cell mapped to header value
		Pair<String, String> headerWithCellValue;
		
		//try with resources to avoid leaks
		try(CSVParser csvParser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(factory.getReader(filePath))){
			
			//grab the column headers, in order (leftmost at index 0)
			List<String> headers = csvParser.getHeaderNames();
			
			//trim the headers (sometimes appends extra spaces)
			headers = headers.parallelStream()
					.map(s -> s.trim())
					.collect(Collectors.toList());
			
			//NOTE: using the "withFirstRecordAsHeader" option in the builder skips the first row automatically
			for (CSVRecord record : csvParser) {			
				for(int columnIndex = 0; columnIndex < headers.size(); ++columnIndex) {
					//build the pair -- first value from header, second from record
					headerWithCellValue = Pair.with(headers.get(columnIndex), record.get(columnIndex));
					
					//add the pair to the list
					row.add(headerWithCellValue);
				}
				
				//at the end of each row, add to list
				spreadsheet.add(row);			    
			}
		} catch (FileNotFoundException e) {
			String errorMessage = String.format("File not found at %s", filePath);
			throw new SpreadsheetException(errorMessage, e);
		} catch (IOException e) {
			String errorMessage = String.format("Couldn't open/read from file at %s", filePath);
			throw new SpreadsheetException(errorMessage, e);			
		}		
		
		//return the spreadsheet representation
		return spreadsheet;
	}
	
	/**
	 * 
	 * @param filePath
	 * @param converter
	 * @throws {@link SpreadsheetException}
	 * @throws {@link NoTransformerException}
	 * @return
	 */
	public <T> List<T> read(String filePath, Converter<T> converter){
		List<T> list = new ArrayList<>();
		
		//read in as list of list of pairs
		List<List<Pair<String, String>>> pairs = read(filePath);
		
		//call converter to convert
		list = converter.convertAll(pairs);
		
		//return the value
		return list;
	}	
	
}
