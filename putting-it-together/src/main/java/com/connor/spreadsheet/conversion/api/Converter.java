package com.connor.spreadsheet.conversion.api;

import java.util.List;

import org.javatuples.Pair;

import com.connor.spreadsheet.exception.NoTransformerException;
import com.connor.spreadsheet.exception.SpreadsheetException;

public interface Converter<T> {

	/**
	 * Converts a given row in a spreadsheet to the object
	 * @param values first value is header title, second is value
	 * @throws NoTransformerException if a field that needs to be set does not have a transformer
	 * @throws SpreadsheetException if anything reflection related occurs
	 * @return
	 */
	public T convert(List<Pair<String, String>> values);
	
	/**
	 * Converts all rows in a spreadsheet to a list of objects
	 * 1 row in spreadsheet -> one object (same index as input)
	 * @param values first value is header title, second is value
	 * @throws {@link SpreadsheetException}
	 * @throws {@link NoTransformerException}
	 * @return
	 */
	public List<T> convertAll(List<List<Pair<String, String>>> values);	
}