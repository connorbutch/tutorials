package com.connor.spreadsheet.conversion.impl;

import javax.inject.Inject;

import com.connor.spreadsheet.conversion.api.AbstractBaseReflectionConverter;
import com.connor.spreadsheet.factory.TransformerFactory;
import com.connor.spreadsheet.model.Reservation;

/**
 * Converter implementation for reservation class.
 * @author Connors
 *
 */
public class ConverterReservationImpl extends AbstractBaseReflectionConverter<Reservation>{

	/**
	 * Cdi-enabled constructor
	 * @param factory
	 */
	@Inject
	public ConverterReservationImpl(TransformerFactory factory) {
		super(factory);
	}
	
	//NOTE: all work is done by parent class, all we have to do is declare this class
	//and make a cdi-enabled constructor:)
}
