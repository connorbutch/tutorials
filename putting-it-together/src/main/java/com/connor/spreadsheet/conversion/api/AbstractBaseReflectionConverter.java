package com.connor.spreadsheet.conversion.api;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.javatuples.Pair;

import com.connor.generic.one.api.Transformer;
import com.connor.spreadsheet.exception.NoTransformerException;
import com.connor.spreadsheet.exception.SpreadsheetException;
import com.connor.spreadsheet.factory.TransformerFactory;

/**
 * This class deals with converting given input (read from spreadsheet)
 * into the generic type based on the name of the field matching the 
 * name of the spreadsheet header.  In order to allow for cdi to get 
 * an instance (cdi is always type safe), classes must subclass this
 * but should usually not override methods.
 * 
 * Future improvements might be
 *      setting parent class fields
 *      recursively instantiating
 *      or matching on different regex for field names to columns (such as removing spaces)
 * @author Connors
 *
 * @param <T>
 */
public abstract class AbstractBaseReflectionConverter<T> implements Converter<T>{

	/**
	 * Used to keep reference to class type
	 */
	private final Class<T> clazz;
	
	/**
	 * Used to get the transformer for a type.
	 */
	protected final TransformerFactory factory;

	/**
	 * Constructor to be invoked from subclass
	 * @param factory
	 */
	@SuppressWarnings("unchecked")
	protected AbstractBaseReflectionConverter(TransformerFactory factory) {
		this.factory = factory;
		clazz = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public T convert(List<Pair<String, String>> values) {
		T t = null;

		//check we got a list
		if(CollectionUtils.isNotEmpty(values)) {
			//get a new instance via reflection (calls public no arg constructor)
			try {
				t = clazz.newInstance();
			} catch (InstantiationException e) {
				throw new SpreadsheetException("Error instantiating", e);
			} catch (IllegalAccessException e) {
				throw new SpreadsheetException(String.format("No public no arg constructor exists for class %s", t.getClass()), e);
			}

			//loop over the provided inputs
			for(Pair<String, String> pair: values) {

				//try to get a field with the same name
				Field field = null;
				try {
					field = t.getClass().getDeclaredField(pair.getValue0());					
				} catch (NoSuchFieldException e) {
					//silence -- we don't care if field doesn't exist
				} catch (SecurityException e) {
					throw new SpreadsheetException("Could not get field by name -- there is likely a security manager in place", e);
				}

				//this means we found a match
				if(field != null) {
					//get the mapper for the field 
					Transformer<?> transformer = factory.getTransformerForClass(field.getType()); //NOTE: this is very important we use get type here AND NOT GET CLASS

					//if mapper is null, that means no mapper is found for type, so throw exception
					if(transformer == null) {
						throw new NoTransformerException(field.getType());
					}

					//getting here means we have a mapper, so use it to map value
					Object value = transformer.transform(pair.getValue1());

					//attempt to make the field accessible -- ignoring any failures (as we will be unable to set field anyways)
					try {
						field.trySetAccessible();
					}catch(SecurityException e) {
						//silence -- handle when we cannot set field value
					}

					//if we get a null and value is a primitive, do not set -- this can lead to null pointer when unwrapping
					if(!(field.getType().isPrimitive() && value == null)) {
						//attempt to set the value on the field
						try {
							field.set(t, value);
						} catch (IllegalArgumentException e) {
							String errorMessage = String.format("Couldn't set value on field %s on class %s", field.getName(), t.getClass().getSimpleName());
							throw new SpreadsheetException(errorMessage, e);
						} catch (IllegalAccessException e) {
							String errorMessage = String.format("Couldn't set value on field %s on class %s", field.getName(), t.getClass().getSimpleName());
							throw new SpreadsheetException(errorMessage, e);
						}
					} 
				} 
			}  
		}

		//return the value
		return t;
	}

	@Override
	public List<T> convertAll(List<List<Pair<String, String>>> values){
		//default
		List<T> list = new ArrayList<>();

		//loop over and add to list
		if(CollectionUtils.isNotEmpty(values)) {
			for(List<Pair<String, String>> row: values) {
				list.add(convert(row));
			}
		}

		//return it -- either empty list, or having size
		return list;
	}		
}