package com.connor.spreadsheet.exception;

/**
 * Base exception for this project
 * @author Connors
 *
 */
public class SpreadsheetException extends RuntimeException{

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 4999677062188034915L;
	
	public SpreadsheetException(String message) {
		super(message);
	}
	
	public SpreadsheetException(String message, Throwable t) {
		super(message, t);
	}

}
