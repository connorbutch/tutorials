package com.connor.spreadsheet.exception;

/**
 * Throw this if there is no transformer for a given class
 * @author Connors
 *
 */
public class NoTransformerException extends RuntimeException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -5322999030962430689L;
	
	/**
	 * The class no transformer is present on classpath.
	 */
	private final Class<?> clazz;

	/**
	 * Constructor
	 * @param clazz
	 */
	public NoTransformerException(Class<?> clazz) {
		this.clazz = clazz;
	}
	
	/**
	 * Constructor
	 * @param message
	 * @param clazz
	 */
	public NoTransformerException(String message, Class<?> clazz) {
		super(message);
		this.clazz = clazz;
	}
	
	/**
	 * Constructor
	 * @param message
	 * @param exception
	 * @param clazz
	 */
	public NoTransformerException(String message, Throwable exception, Class<?> clazz) {
		super(message, exception);
		this.clazz = clazz;
	}

	/**
	 * Get the class associated with this
	 * @return
	 */
	public Class<?> getClazz() {
		return clazz;
	}	
}