package com.connor.spreadsheet.factory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import com.connor.spreadsheet.annotation.StreamReader;

/**
 * This class gives us a way to get an input stream from a factory.  It's primary purpose
 * is to allow for 100 % unit test coverage
 * @author Connors
 *
 */
public class InputStreamReaderFactory {

	/**
	 * Get a input stream reader for given path.  It is calling object's
	 * job to close the stream (either try with resources, or finally block)
	 * @param filePath
	 * @return
	 * @throws FileNotFoundException 
	 */
	public InputStreamReader getReader(String filePath) throws FileNotFoundException {
		return new InputStreamReader(new FileInputStream(filePath));
	}	

	@Produces
	@StreamReader(filePath = "") //non binding, so we would rather force client to provide value
	public InputStreamReader getReader(InjectionPoint ip) throws FileNotFoundException {
		//get the filepath
		String filePath = ip.getAnnotated().getAnnotation(StreamReader.class).filePath();
		return getReader(filePath);
	}
	
	/**
	 * Disposer for producer method which closes resources
	 * @param reader
	 */
	public void disposeStreamReader(@Disposes @StreamReader(filePath = "") InputStreamReader reader) {
		try{
			reader.close();
		}catch(IOException e) {
			//silence (for now)
		}
	}
}