package com.connor.spreadsheet.factory;

import java.util.Collections;
import java.util.Map;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.HashedMap;

import com.connor.generic.one.api.AbstractBaseTransformer;
import com.connor.generic.one.api.Transformer;
import com.connor.spreadsheet.annotation.Transform;

/**
 * This allows to get a transformer for a given class type.  This implements the 
 * factory pattern.
 * 
 * This also provides a way of getting a transformer via cdi.
 * @author Connors
 *
 */
@Singleton //only want one for whole project
public class TransformerFactory {

	/**
	 * Use this to get a transformer from it's associated class
	 */
	private final Map<Class<?>, Transformer<?>> transformers;
	
	/**
	 * Cdi-enabled constructor
	 * @param <T>
	 * @param transformers
	 */
	@Inject
	public TransformerFactory(Instance<Transformer<?>> transformers) {
		//create a temp map from the instance
		Map<Class<?>, Transformer<?>> tempTransformers = new HashedMap<>();
		for(Transformer<?> transformer: transformers) {
			tempTransformers.put(transformer.getMappedFieldType(), transformer);
		}
		
		//create an immutable map from our temp map
		this.transformers = Collections.unmodifiableMap(tempTransformers);	
	}
	
	/**
	 * Gets transformer for a class.  Will return null if no transformer exists for a given class.
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> Transformer<T> getTransformerForClass(Class<T> clazz){
		Transformer<T> transformer = null;
		
		//if map is not null, then loop over it
		if(clazz != null && MapUtils.isNotEmpty(transformers)) {
			for(Map.Entry<Class<?>, Transformer<?>> entry: transformers.entrySet()) {
				//if classes match, then set and break out of loop
				if(entry != null && (clazz.equals(entry.getKey()) || clazz.isPrimitive() && entry.getKey().equals(getWrapperForPrimtive(clazz)))) {
					transformer = (Transformer<T>) entry.getValue();
					break;
				}
			}
		}
		
		//return it -- will be null if none found
		return transformer;
	}
	
	/**
	 * In case we ever want to get transformer via cdi.
	 * @param <T>
	 * @param ip
	 * @return
	 */
	@Produces
	@Transform(transformType = TransformerFactory.class) //non-binding, but must be specified
	public <T> Transformer<T> getTransformerForClass(InjectionPoint ip){
		//we know it has the annotation and it is a type literal
		Transform transform = ip.getAnnotated().getAnnotation(Transform.class);
		Class<?> clazz = transform.transformType();
		return (Transformer<T>) getTransformerForClass(clazz);
	}	
	
	
	private Class<?> getWrapperForPrimtive(Class<?> clazz){
		//should always be a primitive, private method, shouldn't need to validate
		Class<?> classToReturn = null;
		
		//only 6 primitives, unlikely to ever change in java
		if(clazz == int.class) {
			classToReturn = Integer.class;
		}else if(clazz == long.class) {
			classToReturn = Long.class;
		}else if(clazz == boolean.class) {
			classToReturn = Boolean.class;
		}else if(clazz == double.class) {
			classToReturn = Double.class;
		}else if(clazz == float.class) {
			clazz = Float.class;
		}
		
		return classToReturn;
	}
}
