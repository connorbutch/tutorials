package com.connor.spreadsheet.model;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * This is a dummy class for testing reading in from spreadsheet.
 * @author Connors
 *
 */
public class Reservation implements Serializable {	

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -585010191066511995L;

	/**
	 * 
	 */
	private int id;
	
	/**
	 * 
	 */
	private boolean hasPaid;
	
	/**
	 * 
	 */
	private LocalDate checkin;
	
	/**
	 * 
	 */
	private LocalDate checkout;	

	/**
	 * 
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasPaid() {
		return hasPaid;
	}

	/**
	 * 
	 * @return
	 */
	public LocalDate getCheckin() {
		return checkin;
	}

	/**
	 * 
	 * @return
	 */
	public LocalDate getCheckout() {
		return checkout;
	}
	
	

	public void setId(int id) {
		this.id = id;
	}

	public void setHasPaid(boolean hasPaid) {
		this.hasPaid = hasPaid;
	}

	public void setCheckin(LocalDate checkin) {
		this.checkin = checkin;
	}

	public void setCheckout(LocalDate checkout) {
		this.checkout = checkout;
	}

	@Override
	public String toString() {
		return "Reservation [id=" + id + ", hasPaid=" + hasPaid + ", checkin=" + checkin + ", checkout=" + checkout
				+ "]";
	}	
	
	
}