package com.connor.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.connor.spreadsheet.SpreadsheetReader;
import com.connor.spreadsheet.conversion.api.Converter;
import com.connor.spreadsheet.conversion.impl.ConverterReservationImpl;
import com.connor.spreadsheet.model.Reservation;

@RunWith(MockitoJUnitRunner.class)
public class ReadReservationIT {

	@Test
	public void testReadReservation() {
		//fire up cdi container (weld)
		try(WeldContainer container = new Weld().initialize()){
			Converter<Reservation> converter = container.select(ConverterReservationImpl.class).get();
			SpreadsheetReader reader = container.select(SpreadsheetReader.class).get();
			List<Reservation> reservations = reader.read("C:\\College\\Coding_Projects\\In_Progress\\tutorials\\putting-it-together\\src\\test\\resources\\Reservation.csv", converter);
			assertTrue("There should be one reservation", reservations.size() == 1);
			checkReservation(reservations.get(0));
		}		
	}
	
	/**
	 * Used to check a reservation has values as expected
	 * @param reservation
	 */
	private void checkReservation(Reservation reservation) {
		//set our expected values
		Reservation expected = new Reservation();
		expected.setCheckin(LocalDate.of(2020, 10, 11));
		expected.setCheckout(LocalDate.of(2020, 10, 13));
		expected.setHasPaid(true);
		expected.setId(1);
		
		//assert all fields present are true by using apache lang equals builder
		assertTrue("The reservation should be what we expect", EqualsBuilder.reflectionEquals(reservation, expected));
	}
	
}
