package com.connor.ejb.local;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.connor.ejb.annotation.Service;
import com.connor.ejb.api.FundTransferService;
import com.connor.ejb.dao.AccountDao;
import com.connor.ejb.exception.AccountNotFoundException;
import com.connor.ejb.exception.InsufficientBalanceException;
import com.connor.ejb.exception.WithdrawNegativeNumberException;
import com.connor.ejb.model.Account;

/**
 * Local implementation of transfer service.  Please note that most of the important
 * configurations for the ejb are defined in the service annotation class.
 * @author Connors
 *
 */
@Service //NOTE: because this is a stereotype, we get all the annotations associated with it
public class FundTransferServiceLocalImpl implements FundTransferService{

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 3203753865801891381L;
	
	/**
	 * Logger instance
	 */
	private final Logger log;

	/**
	 * The dao for accessing accounts.
	 */
	private final AccountDao accountDao;

	
	/**
	 * Cdi-enabled constructor
	 * @param accountDao
	 * @param log
	 */
	@Inject
	public FundTransferServiceLocalImpl(AccountDao accountDao, Logger log) {
		this.accountDao = accountDao;
		this.log = log;
	}

	@Override
	public void transferFunds(long fromAccountId, long toAccountId, double amountToTranfer) {
		//validate we are not withdrawing negative amount, which leads to money going the wrong way
		try {
			validateAmount(amountToTranfer);
		}catch(WithdrawNegativeNumberException e) {
			log.error("Attempted to withdraw an invalid (negative) amount of funds {} from account with id {} to account with id {}", amountToTranfer, fromAccountId, toAccountId, e);
			throw e;
		}
		
		//validate from account exists
		try {
			validateAccountExists(fromAccountId);
		}catch(AccountNotFoundException e) {
			log.error("From account not found with id {}", fromAccountId, e);
			throw e;
		}
		
		//validate to account exists
		try {
			validateAccountExists(toAccountId);
		}catch(AccountNotFoundException e) {
			log.error("To account not found with id {}", toAccountId, e);
			throw e;
		}
		
		
		
		//withdraw money from account
		try {
			accountDao.withdraw(fromAccountId, amountToTranfer);
			log.info("Successfully withdrew {} dollars from account with id {}", amountToTranfer, fromAccountId);
		}
		/*
		 * NOTE: my normal exception handling practice for business logic being violated is to use specific unchecked (runtime) exceptions
		 * making sure to declare the exceptions that can be thrown in the "topmost" layer (normally service), but sometimes webservice.
		 * I take care to only log errors once, otherwise it can be confusing, and log as much info as possible (always including the
		 * exception itself)
		 */
		catch(AccountNotFoundException e) {			
			log.error("Could not find account with id {} to withdraw from", fromAccountId, e);
			throw e;
		}catch(InsufficientBalanceException e) {
			log.error("Could not withdraw {} dollars from account with id {}",amountToTranfer, fromAccountId, e);
			throw e;
		}

		//we were successful withdrawing, now deposit same amount to other account
		try {
			accountDao.deposit(toAccountId, amountToTranfer);
			log.info("Successfully deposited {} dollars to account with id {}", amountToTranfer, toAccountId);
		}catch(AccountNotFoundException e) {
			//follow same practice as above, only log at top layer to avoid confusion, then rethrow, causing transaction to be rolled back
			log.error("Could not find account with id {} to deposit funds into.  Transaction will be rolled back and {} dollars will be deposited back into account {}", toAccountId, amountToTranfer, fromAccountId, e);
			throw e;
		}		
	}
	
	/**
	 * Validates the amount to transfer (only checks is not negative)
	 * NOTE: while we would like to always house all of our business logic in service,
	 * we sometimes move it into dao for certain situations such as
	 * I attempt to withdraw $100, my current balance is 90, but i had just depostited 10
	 * in this case, we have to check in the dao as well (as changes may have occurred after
	 * service checks).  In this case, we let the dao throw those exceptions
	 * @throws WithdrawNegativeNumberException 
	 */
	private void validateAmount(double amountToTransfer) {
		//throw exception if less than 0, else, do nothing.
		if(amountToTransfer < 0) {
			throw new WithdrawNegativeNumberException(amountToTransfer);
		}
	}
	
	/**
	 * If the account does not exist, throws an exception
	 * @throws AccountNotFoundException e
	 * @param accountId
	 */
	private void validateAccountExists(long accountId) {
		//throw exception if account not found, else do nothing
		if(accountDao.getAccountById(accountId) == null) {
			throw new AccountNotFoundException(accountId);
		}
	}
}