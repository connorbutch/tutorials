package com.connor.ejb.exception;

/**
 * This should occur when a user attempts to withdraw a negative amount of funds.  
 * This is NOT used when a user overdrafts; in that case, use {@link InsufficientBalanceException}
 * @author Connors
 *
 */
public class WithdrawNegativeNumberException extends BusinessException {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = -4558039372630663069L;
	
	/**
	 * The (negative) amount attempted to be withdrawn
	 */
	private final double AmountToWithdraw;

	/**
	 * Constructor
	 * @param amountToWithdraw
	 */
	public WithdrawNegativeNumberException(double amountToWithdraw) {
		super(); //make explicit
		AmountToWithdraw = amountToWithdraw;
	}

	/**
	 * Getter
	 * @return
	 */
	public double getAmountToWithdraw() {
		return AmountToWithdraw;
	}
}
