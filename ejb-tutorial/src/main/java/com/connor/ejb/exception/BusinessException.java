package com.connor.ejb.exception;

import javax.ejb.ApplicationException;

/**
 * This class is the parent class of all business logic exceptions.  To be clear,
 * a business logic exception is not an exception that gets thrown from something
 * going wrong in a program, but rather a business rule being violated (such as 
 * trying to book a hotel room that is already booked, etc).
 * 
 * NOTE: this is an application exception, meaning it will get thrown to the client "as is".
 * The rollback=true tells the container to rollback the transaction if this is thrown.  Note, 
 * I highly recommend never using stateful ejb, but if you did, throwing runtime exceptions, 
 * even those marked as application exceptions, can lead to the container discarding the 
 * bean instance, which could be bad if it is used to hold state.
 * @author Connors
 *
 */
@ApplicationException(rollback=true) //NOTE: this forces us to rollback transaction
public class BusinessException extends RuntimeException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 4399138431727147068L;
	
	/**
	 * Default constructor.
	 */
	public BusinessException() {
		super(); //make explicit
	}
	
	/**
	 * Constructor.  Note that this should never take another exception as a 
	 * parameter, as this should be manually thrown inside an if statement.
	 * @param message
	 */
	public BusinessException(String message) {
		super(message);
	}
}
