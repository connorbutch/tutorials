package com.connor.ejb.exception;


/**
 * This exception should get thrown when we attempt to withdraw more funds from an account
 * than it has (i.e. overdrawing).  This will automatically rollback a transaction.
 * @author Connors
 *
 */
public class InsufficientBalanceException extends BusinessException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -5413312797467457442L;

	/**
	 * The balance in the account
	 */
	private final double accountBalance;
	
	/**
	 * The amount attempted to withdraw
	 */
	private final double attemptedAmountToWithdraw;

	/**
	 * Constructor
	 * @param accountBalance
	 * @param attemptedAmountToWithdraw
	 */
	public InsufficientBalanceException(double accountBalance, double attemptedAmountToWithdraw) {
		super(); //make explicit
		this.accountBalance = accountBalance;
		this.attemptedAmountToWithdraw = attemptedAmountToWithdraw;
	}

	/**
	 * Getter
	 * @return
	 */
	public double getAccountBalance() {
		return accountBalance;
	}

	/**
	 * Getter
	 * @return
	 */
	public double getAttemptedAmountToWithdraw() {
		return attemptedAmountToWithdraw;
	}	
}