package com.connor.ejb.exception;


/**
 * Throw this if an account is not found with a specified id.
 * 
 * NOTE: this extends BusinessException, which means it rolls back transactions automatically.
 * @author Connors
 *
 */
public class AccountNotFoundException extends BusinessException{

	
	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 9140016918677036820L;
	/**
	 * The account id.
	 */
	private final long accountId;
	
	/**
	 * Constructor.
	 * @param accountId
	 */
	public AccountNotFoundException(long accountId) {
		super();
		this.accountId = accountId;
	}

	/**
	 * Get the account id that did not exist.
	 * @return
	 */
	public long getAccountId() {
		return accountId;
	}
	
	
	
}
