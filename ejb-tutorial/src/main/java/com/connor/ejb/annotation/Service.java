package com.connor.ejb.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.io.Serializable;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.inject.Stereotype;


/**
 * This represents the "standard" options for a stateless, local ejb.
 * @author Connors
 *
 */
@Stereotype //this tells us that this annotation "contains" all other annotations.  Hence we can use this instead of @Stateless @Local @ . . . 
@Stateless //ejb should almost always be stateless!  This means they are not tied to a particular client
@TransactionManagement(TransactionManagementType.CONTAINER) //the default, but this means the cdi container handles transactions.  Should almost always use this.
@TransactionAttribute(TransactionAttributeType.MANDATORY) //this says all methods must be executed in the context of a transaction..  If none exists, one will be created
@Local //the default, this means resides within same jvm
//NOTE: before ejb 3, you could set transaction isolation level, however this is now set at jdbc connection level.
@Documented //document our annotation
@Retention(RUNTIME) 
@Target(TYPE) //only allow at class level or to mark other annotations
public @interface Service {

	//intentionally blank -- marker interface
	
}
