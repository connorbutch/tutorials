package com.connor.ejb.annotation;

import javax.ejb.Remote;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Stereotype;

@Service //bring in everything from service
@Stereotype //this tells us that this annotation "contains" all other annotations.  Hence we can use this instead of @Stateless . . .
@Remote //change to remote interface
@Alternative //prefer the local service unless we explicitly define in beans.xml (in case where we have both local and remote implementations)
public @interface RemoteService {
	//intentionally blank -- marker interface
}
