package com.connor.ejb.dao;

import java.io.Serializable;

import com.connor.ejb.exception.AccountNotFoundException;
import com.connor.ejb.exception.InsufficientBalanceException;
import com.connor.ejb.model.Account;

/**
 * This is an interface specifying a dao to interact with an account (as in a bank account).
 * Please NOTE: in a real project, this would NOT be in the ejb itself, it should be another
 * jar.  I have included it here so it is easily visible in the tutorials
 * @author Connors
 *
 */
public interface AccountDao extends Serializable {
	
	/**
	 * Gets an account by id.  If no account exists with the specified id, then returns null
	 * @param accountId
	 * @return
	 */
	Account getAccountById(long accountId);

	/**
	 * Deposit specified amount to the account.
	 * @param accountId the id of the account
	 * @param amountToDeposit the amount to deposit
	 * @throws AccountNotFoundException if no account exists with the specified id
	 */
	void deposit(long accountId, double amountToDeposit);
	
	/**
	 * Withdraw funds from specified account.
	 * @param accountId the id of the account
	 * @param amountToWithdraw the amount to withdraw
	 * @throws AccountNotFoundException if no account with the id exists
	 * @throws InsufficientBalanceException if there are not enough funds to withdraw
	 */
	void withdraw(long accountId, double amountToWithdraw);
}
