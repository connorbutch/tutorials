package com.connor.ejb.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.connor.ejb.model.Account;

/**
 * Extractor class for getting an account from a result set.
 * Normally, this would be in the dao layer (outside of ejb)
 * @author Connors
 *
 */
public class RowMapperAccountImpl implements RowMapper<Account>{

	/**
	 * Column name of account id
	 */
	private static final String ACCOUNT_ID = "ACCOUNT_SID";

	/**
	 * Column name of account balance.
	 */
	private static final String ACCOUNT_BALANCE = "ACCOUNT_BALANCE";

	@Override
	public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
		//declare here for use later
		long accountId;
		double balance;

		//grab the parameters from result set
		//NOTE: either of these two will throw an sql exception if they do not exist, we will let go through to calling class
		accountId = rs.getLong(ACCOUNT_ID);
		balance = rs.getDouble(ACCOUNT_BALANCE);

		//return the new account
		return new Account(accountId, balance);
	}
}