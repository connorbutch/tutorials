package com.connor.ejb.dao;

import com.connor.ejb.annotation.Datasource;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * This is a cdi producer class for a callable jdbc template based on the jndi name.
 * Normally, this would be in the dao layer (outside of ejb)
 * @author Connors
 *
 */
public class JdbcTemplateProducer {
	
	/**
	 * Used to interact with jndi
	 */
	private final Context context;

	
	/**
	 * Cdi-enabled constructor
	 * @param context
	 */
	@Inject
	public JdbcTemplateProducer(Context context) {
		this.context = context;
	}



	/**
	 * Producer method for a jdbc template that goes to the jndi.
	 * @param ip
	 * @return
	 */
	@Produces
	@Datasource(name = "") //nonbinding, but no default, so we must specify
	public JdbcTemplate getTemplate(InjectionPoint ip) {
		//grab the string to use to get from the jndi
		String name = ip.getAnnotated().getAnnotation(Datasource.class).name();
		
		try {
			DataSource ds = (DataSource) context.lookup(name);
			return new JdbcTemplate(ds);			
		}catch(NamingException e) {
			//TODO
			throw new RuntimeException(e);
		}
	}	
}