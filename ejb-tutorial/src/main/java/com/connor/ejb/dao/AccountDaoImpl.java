package com.connor.ejb.dao;


import com.connor.ejb.annotation.Datasource;
import com.connor.ejb.exception.AccountNotFoundException;
import com.connor.ejb.exception.InsufficientBalanceException;
import com.connor.ejb.model.Account;

import javax.inject.Inject;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * Implementation of account dao using spring data (jdbc template).
 * Normally, this would be in the dao layer (outside of ejb)
 * @author Connors
 *
 */
public class AccountDaoImpl implements AccountDao {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -3054744890397554105L;

	/**
	 * Sql ran to select an account by id.  Normally, I would externalize this to properties file, but left it
	 * here for visibility.
	 */
	private static final String SELECT_ACCOUNT_BY_ID_SQL = "SELECT * FROM ACCOUNT WHERE ACCOUNT_SID = ?";

	/**
	 * Note: this is just for readability.  In a real world, you would want to set to balance minus amount to prevent
	 * update since your last read
	 */
	private static final String UPDATE_ACCOUNT_BALANCE = "UPDATE ACCOUNT SET BALANCE = ? WHERE ACCOUNT_SID = ?";

	/**
	 * Used for database-related operations
	 */
	private final JdbcTemplate jdbcTemplate;

	/**
	 * Used to get account info from results of sql query
	 */
	private final RowMapper<Account> accountExtractor;

	/**
	 * Cdi-enabled constructor.
	 * @param jdbcTemplate
	 * @param accountExtractor
	 */
	@Inject
	public AccountDaoImpl(@Datasource(name="dummyDs")JdbcTemplate jdbcTemplate, RowMapper<Account> accountExtractor) {
		this.jdbcTemplate = jdbcTemplate;
		this.accountExtractor = accountExtractor;
	}

	@Override
	public Account getAccountById(long accountId) {
		try {
			return jdbcTemplate.queryForObject(SELECT_ACCOUNT_BY_ID_SQL, new Object[] {accountId}, accountExtractor);
		}catch(DataAccessException e) {
			throw e;
		}
	}
	
	@Override
	public void deposit(long accountId, double amountToDeposit) {
		//check they did not pass in negative amount to deposit
		if(amountToDeposit < 0) {
			String errorMessage = String.format("Please deposit a postive amount (attempted to deposit %f dollars", amountToDeposit);
			throw new IllegalArgumentException(errorMessage);
		}

		//get the account by id, throw exception if none exists
		Account account = getAccountById(accountId);
		if(account == null) {
			throw new AccountNotFoundException(accountId);
		}		

		//deposit the funds
		double newBalance = account.getAccountBalance() + amountToDeposit;
		updateAccountBalance(newBalance, accountId);
	}

	@Override
	public void withdraw(long accountId, double amountToWithdraw) {	
		//if pass in negative, throw illegal argument exception
		if(amountToWithdraw < 0) {
			String errorMessage = String.format("You must withdraw a positive amount of funds.  Attempted to withdraw %f dollars" , amountToWithdraw);
			throw new IllegalArgumentException(errorMessage);
		}


		//get the account by id, throw exception if none exists
		Account account = getAccountById(accountId);
		if(account == null) {
			throw new AccountNotFoundException(accountId);
		}

		//if the account has insufficient balance, throw exception
		if(account.getAccountBalance() < amountToWithdraw) {
			throw new InsufficientBalanceException(account.getAccountBalance(), amountToWithdraw);
		}

		//withdraw the funds
		double newBalance = account.getAccountBalance() - amountToWithdraw;
		updateAccountBalance(newBalance, accountId);
	}	



	/**
	 * Set the account balance to the parameter passed in.
	 * @param newBalance
	 * @param accountId
	 */
	private void updateAccountBalance(double newBalance, long accountId) {
		//NOTE: THIS IS JUST FOR A DEMO, YOU SHOULD MAKE THE OPERATION ATOMIC IN DB	
		try {
			jdbcTemplate.update(UPDATE_ACCOUNT_BALANCE, newBalance, accountId);
		}catch(DataAccessException e) {
			//TODO
			throw e;
		}
	}
}
