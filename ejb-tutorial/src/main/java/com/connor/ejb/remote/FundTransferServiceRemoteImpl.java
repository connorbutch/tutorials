package com.connor.ejb.remote;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.connor.ejb.annotation.RemoteService;
import com.connor.ejb.api.FundTransferService;
import com.connor.ejb.dao.AccountDao;
import com.connor.ejb.exception.AccountNotFoundException;
import com.connor.ejb.exception.InsufficientBalanceException;

/**
 * This illustrates implementing fund transfer service as a remote implementation.
 * NOTE: the local vs remote distinction is invisible to client using interface.
 * @author Connors
 *
 */
@RemoteService //this brings in all annotations on remote service
public class FundTransferServiceRemoteImpl implements FundTransferService {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 160781984664045664L;

	/**
	 * Logger instance
	 */
	private final Logger log;

	/**
	 * The dao for accessing accounts.
	 */
	private final AccountDao accountDao;
	
	/**
	 * Cdi-enabled constructor.
	 * @param log
	 * @param accountDao
	 */
	@Inject
	public FundTransferServiceRemoteImpl(Logger log, AccountDao accountDao) {
		this.log = log;
		this.accountDao = accountDao;
	}

	@Override
	public void transferFunds(long fromAccountId, long toAccountId, double amountToTranfer) {
		//NOTE: implementation here can be same as local except for one thing -- one is pass by ref, one is pass by value

		//withdraw money from account
		try {
			accountDao.withdraw(fromAccountId, amountToTranfer);
			log.info("Successfully withdrew {} dollars from account with id {}", amountToTranfer, fromAccountId);
		}
		/*
		 * NOTE: my normal exception handling practice for business logic being violated is to use specific unchecked (runtime) exceptions
		 * making sure to declare the exceptions that can be thrown in the "topmost" layer (normally service), but sometimes webservice.
		 * I take care to only log errors once, otherwise it can be confusing, and log as much info as possible (always including the
		 * exception itself)
		 */
		catch(AccountNotFoundException e) {			
			log.error("Could not find account with id {} to withdraw from", fromAccountId, e);
			throw e;
		}catch(InsufficientBalanceException e) {
			log.error("Could not withdraw {} dollars from account with id {}",amountToTranfer, fromAccountId, e);
			throw e;
		}

		//we were successful withdrawing, now deposit same amount to other account
		try {
			accountDao.deposit(toAccountId, amountToTranfer);
			log.info("Successfully deposited {} dollars to account with id {}", amountToTranfer, toAccountId);
		}catch(AccountNotFoundException e) {
			//follow same practice as above, only log at top layer to avoid confusion, then rethrow, causing transaction to be rolled back
			log.error("Could not find account with id {} to deposit funds into.  Transaction will be rolled back and {} dollars will be deposited back into account {}", toAccountId, amountToTranfer, fromAccountId, e);
			throw e;
		}
	}
}