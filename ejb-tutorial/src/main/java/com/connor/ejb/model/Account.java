package com.connor.ejb.model;

import java.io.Serializable;

import com.connor.ejb.exception.InsufficientBalanceException;

/**
 * This is a model class representing an account (like a bank account).
 * @author Connors
 *
 */
public class Account implements Serializable{	

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 2224827026003371703L;

	/**
	 * Account id -- no need for getter or setter
	 */
	private final long accountId;
	
	/**
	 * The account balance
	 */
	private double accountBalance;

	/**
	 * Constructor.
	 * @param accountId
	 * @param accountBalance
	 */
	public Account(long accountId, double accountBalance) {
		this.accountId = accountId;
		this.accountBalance = accountBalance;
	}

	/**
	 * Getter
	 * @return
	 */
	public double getAccountBalance() {
		return accountBalance;
	}
}