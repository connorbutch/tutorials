package com.connor.ejb.api;

import java.io.Serializable;

import com.connor.ejb.exception.AccountNotFoundException;
import com.connor.ejb.exception.InsufficientBalanceException;
import com.connor.ejb.model.Account;

/**
 * This provides the service contract for transferring funds between accounts.
 * Please note that this interface is independent of the underlying technology
 * (no knowledge it is implemented as an ejb)
 * @author Connors
 *
 */
public interface FundTransferService extends Serializable{

	/**
	 * Transfers funds from one account from another
	 * 
	 * NOTE: runtime exceptions are not declared with throws clauses (they are unchecked)
	 * but we do declare them here for javadoc.  This is follows the practice outlined in
	 * man libraries (such as spring data jdbc) so user can choose where to handle exception
	 * where to handle the exceptions 
	 * @param fromAccountId the id of the account to withdraw money from
	 * @param toAccountId the id of the account to deposit money to
	 * @param amountToTranfer the amount to transfer
	 * @throws AccountNotFoundException if no account exists for an id (or both)
	 * @throws InsufficientBalanceException if the from account does not have enough money
	 */
	void transferFunds(long fromAccountId, long toAccountId, double amountToTranfer);
	
}
